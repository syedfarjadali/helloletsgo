//
//  AppDelegate.h
//  TripSlip
//
//  Created by bsqptech3 on 9/11/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>
//appDelegate
//ali qamar
@property (strong, nonatomic) UIWindow *window;


@end

