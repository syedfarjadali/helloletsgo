//
//  ViewController.h
//  TripSlip
//
//  Created by bsqptech3 on 9/11/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    NSString *notificationToken;
}
@property (nonatomic, weak)IBOutlet UITextField *emailTxt;
@property (nonatomic, weak)IBOutlet UITextField *passwordTxt;

- (IBAction)signUp:(id)sender;
- (IBAction)forgotPassword:(id)sender;
@end

