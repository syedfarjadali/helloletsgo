//
//  AppDelegate.m
//  TripSlip
//
//  Created by bsqptech3 on 9/11/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Google/SignIn.h>

@import GoogleMaps;
@import Firebase;

@interface AppDelegate () <GIDSignInDelegate>

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[UITabBar appearance] setUnselectedItemTintColor:[UIColor colorWithRed:165.0/255.0 green:191.0/255.0 blue:204.0/255.0 alpha:1.0]]; // for unselected items that are gray
    [FIRApp configure];
    // Initialize the Google Mobile Ads SDK.
    [GADMobileAds configureWithApplicationID:@"ca-app-pub-3940256099942544~1458002511"];
    [GMSServices provideAPIKey:@"AIzaSyAkEDA816j4mI5NhK42Wd2AdTJluZgtOKM"];
    [[GIDSignIn sharedInstance]setClientID:@"540506000806-oaq52hdkb0c98uh7ojlhjiaof2329qab.apps.googleusercontent.com"];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    [GIDSignIn sharedInstance].delegate = self;
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
        if( !error ){
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
    }];
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    NSLog(@"url scheme: %@", [url scheme]);
    // Facebook Call back checking.
    if ([[url scheme] isEqualToString:@"fb156468141759411"])
    {
        BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                      openURL:url
                                                            sourceApplication:sourceApplication
                                                                   annotation:annotation
                        ];
        // Add any custom logic here.
        return handled;
    }
    else{
        
        return [[GIDSignIn sharedInstance]handleURL:url sourceApplication:sourceApplication annotation:annotation];

    }
    
}
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    NSLog(@"Userinfo %@",notification.request.content.userInfo);
    if ([notification.request.content.userInfo objectForKey:@"aps"]) {
        
        //[self.shared.notificationData addObject:[notification.request.content.userInfo objectForKey:@"aps"]];
        
    }
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"PushAlertNotification"
     object:self
     userInfo:@{@"alertString":[notification.request.content.userInfo objectForKey:@"aps"]}];
    completionHandler(UNNotificationPresentationOptionAlert);
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler{
    
    NSLog(@"Userinfo %@",response.notification.request.content.userInfo);
    
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"My token is: %@", deviceToken);
    
    NSString* token = [[[NSString stringWithFormat:@"%@",deviceToken] stringByReplacingOccurrencesOfString:@"<" withString:@""] copy];
    token = [[token stringByReplacingOccurrencesOfString:@">" withString:@""] copy];
    token =[[token stringByReplacingOccurrencesOfString:@" " withString:@"" ] copy];
    NSUserDefaults * settings  = [NSUserDefaults standardUserDefaults];
    [settings setObject:token forKey:@"deviceToken"];
    [settings synchronize];
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    
    
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];

}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [FBSDKAppEvents activateApp];

}

#pragma mark GOOGLE
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    NSURL *profilePic;
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
    //NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *fullName = user.profile.name;
    BOOL hasImage = user.profile.hasImage;
    if (hasImage) {
        profilePic = [user.profile imageURLWithDimension:100];
    }
    else{
        profilePic = [NSURL URLWithString:@""];
    }
    NSString *email = user.profile.email;
    NSDictionary *dic;
    if (userId.length == 0) {
        
        dic = nil;
    }
    else{
        dic = @{@"name":fullName,@"email":email,@"gender":@"",@"id":userId,@"url":[NSString stringWithFormat:@"%@",profilePic],@"username":@""};
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"googleSignIn" object:nil userInfo:dic];
    
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
    [[GIDSignIn sharedInstance] signOut];
    
}

@end
