//
//  Fonts.h
//  Occasions
//
//  Created by BSQPTech on 24/06/2016.
//  Copyright © 2016 App Shah. All rights reserved.
//

#ifndef Fonts_h
#define Fonts_h

#define ColorAppButtons                 [UIColor colorWithRed:235.0/255.0 green:235.0/255.0 blue:235.0/255.0 alpha:1]

#define HelveticaNeueBold               @"HelveticaNeue-Bold.ttf"
#define HelveticaNeueLight              @"HelveticaNeue-Light.ttf"
#define HelveticaNeueRegular            @"HelveticaNeue-Regular.ttf"


#endif /* Fonts_h */
