//
//  Objects.h
//  Occasions
//
//  Created by BSQPTech on 24/06/2016.
//  Copyright © 2016 App Shah. All rights reserved.
//

#ifndef Objects_h
#define Objects_h

#define sharedHelper                    [SharedHelper alloc]
#define ImageDownSizeFactor             0.1

#define ServiceId                       @"id"
#define ServiceStatus                   @"status"
#define ServiceMessage                  @"message"
#define ServiceData                     @"data"
#define ServiceQuestion                 @"question"
#define ServiceCaption                  @"caption"
#define ServiceQuestionId               @"questionId"

#endif /* Objects_h */
