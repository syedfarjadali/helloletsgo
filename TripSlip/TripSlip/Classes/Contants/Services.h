//
//  Services.h
//  Occasions
//
//  Created by BSQPTech on 24/06/2016.
//  Copyright © 2016 App Shah. All rights reserved.
//

#ifndef Services_h
#define Services_h

#define RESPONSE_STATUS                 @"status"
#define RESPONSE_SUCCESS                @"success"
#define RESPONSE_MESSAGE                @"message"

#pragma mark - User Info
#define USER_ID                    @"user_id"
#define USER_INTEREST              @"user_interest"
#define USER_INTEREST_ID           @"user_interest_id"
#define USER_FIRST_NAME            @"user_firstName"
#define USER_NAME                  @"user_name"
#define USER_EMAIL                 @"user_email"
#define USER_PASSWORD              @"user_password"
#define USER_CONFIRM_PASSWORD      @"user_confirmPassword"
#define USER_IMAGE                 @"user_image"
#define USER_ADDRESS               @"user_address"
#define USER_SOCIAL                @"user_social"
#define USER_LATITUDE              @"user_lat"
#define USER_LONGITUDE             @"user_long"
#define USER_BLOCKED               @"user_is_blocked"
#define USER_ABOUT                 @"user_about"
#define USER_SOCIAL_TYPE           @"user_social_type"





#pragma mark - Business Info
#define BUSINESS_NAME                       @"business_name"
#define BUSINESS_USER_ID                    @"business_id"
#define BUSINESS_FULL_NAME                  @"business_firstName"
#define BUSINESS_LAST_NAME                  @"business_lastName"
#define BUSINESS_PASSWORD                   @"business_password"
#define BUSINESS_CONFIRM_PASSWORD           @"business_confirmPassword"
#define BUSINESS_EMAIL                      @"business_email"
#define BUSINESS_IMAGE                      @"business_Image"
#define BUSINESS_ADDRESS                    @"business_Address"
#define BUSINESS_CATEGORY                   @"business_Category"
#define BUSINESS_CATEGORYID                 @"business_CategoryId"
#define BUSINESS_PHONE                      @"business_Phone"
#define BUSINESS_WEB                        @"business_Web"
#define BUSINESS_ISSUB                      @"business_Sub"
#define BUSINESS_ISFIRST                    @"business_First"



#define TYPE                                @"type"


#endif /* Services_h */
