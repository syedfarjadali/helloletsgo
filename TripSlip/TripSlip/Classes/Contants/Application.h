//
//  Application.h
//  Occasions
//
//  Created by BSQPTech on 24/06/2016.
//  Copyright © 2016 App Shah. All rights reserved.
//

#ifndef Application_h
#define Application_h

#define DATABASE_NAME                   @"Ocassions.sqlite"



//#define GoogleApiKey    @"AIzaSyAggs7SL3fSEVMAr6sIdQcQrvSjl3a4Kww"

#define GoogleApiKey    @"AIzaSyDsEC5N3mcKzgAXYhaioA15vWv456UKjT0"

#define InstagramAppURL                 @"itms-apps://itunes.apple.com/app/id389801252"

#define AppStore_URL                    @"http://www.apple.com/itunes/"
#define GoogleStore_URL                 @"https://play.google.com/store"

#define appdelegate ((AppDelegate *)    [[UIApplication sharedApplication] delegate])

#define IS_OS_8_OR_LATER                ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kLEFT_MENU_WIDTH                280

#define kScreenWidth                    [[UIScreen mainScreen]bounds].size.width
#define kScreenHeight                   [[UIScreen mainScreen]bounds].size.height
#define kScreenWidthIPadController      728

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#endif /* Application_h */
