//
//  Constants.h
//  Occasions
//
//  Created by BSQPTech on 24/06/2016.
//  Copyright © 2016 App Shah. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#import <GoogleMaps/GoogleMaps.h>

#pragma mark - ALert Constants
#define MSG_ALERT_TITLE_APP_NAME        @"HelloLetsGo"
#define MSG_ALERT_TITLE_ERROR           @"Error"
#define MSG_ALERT_TITLE_NOTIFICATION    @"Notification"
#define MSG_ALERT_BUSINESSNAME          @"Please, enter business name"
#define MSG_ALERT_BUSINESS_CATEGORY     @"Please, enter business category"
#define MSG_ALERT_BUSINESS_LOCATION     @"Please, enter business Location"
#define MSG_ALERT_BUSINESS_CONTACTNO    @"Please, enter business contact"
#define MSG_ALERT_BUSINESS_STARTTIME    @"Please, enter business starttime"
#define MSG_ALERT_BUSINESS_ENDTIME      @"Please, enter business endtime"
#define MSG_ALERT_BUSINESS_DESCRIPTION  @"Please, enter business description"
#define MSG_ALERT_BUSINESS_WEBSITE      @"Please, enter business website"
#define MSG_ALERT_ADDRESS               @"Please, select your location"
#define MSG_ALERT_FIRST_NAME            @"Please, enter first name"
#define MSG_ALERT_LAST_NAME             @"Please, enter last name"
#define MSG_ALERT_FULL_NAME             @"Please, enter full name"
#define MSG_ALERT_USERNAME              @"Please, enter username"
#define MSG_ALERT_PASSORD               @"Please, enter your password"
#define MSG_ALERT_IMAGE                 @"Please, select your image"
#define MSG_ALERT_CONFIRM_PASSORD       @"Please, confirm your password"
#define MSG_ALERT_PASSORDS_NOT_MATCHED  @"Password & confirm password could not match."
#define MSG_ALERT_PHONE                 @"Please, enter your contact number"
#define MSG_ALERT_VALIDATE_EMAIL        @"Please, enter valid email address"
#define MSG_ALERT_EMAIL                 @"Please, enter email address"
#define MSG_ALERT_COMMENTS              @"Please, enter your comments or suggestions"
#define MSG_ALERT_BUSINESS              @"Please, enter your business name"
#define MSG_ALERT_BUSINESS_LIST         @"Please, select your business category"
#define MSG_ALERT_OWNERSHIP             @"Please, upload your business ownership"
#define MSG_ALERT_FEEDBACK              @"We appreciate your input, and will follow up with you regarding your questions, concerns or comments as soon as we are able."
#define SHARING_CONTENT                 @"Find the best spot in town with verified video reviews"
#define APP_URL                         @"http://www.globalreviewcenter.com"

#endif /* Constants_h */
