//
//  Messages.h
//  Occasions
//
//  Created by BSQPTech on 24/06/2016.
//  Copyright © 2016 App Shah. All rights reserved.
//

#ifndef Messages_h
#define Messages_h

#define MSG_ITEM_NOTAVAILABLE           @"This feature is currently not available."
#define MSG_NO_INTERNET                 @"Please connect to the internet in-order to continue."
#define MSG_NO_DATA                     @"No results found."
#define MSG_CHECK_LATER                 @"Check back later for updates."
#define MSG_TRY_AGAIN                   @"Please try again later."
#define MSG_ALERT_OKAY                  @"OK"
#define MSG_ALERT_CANCEL                @"Cancel"
#define MSG_EMPTYFIELD                  @"Empty Field"
#define MSG_PICK_SOURCE                 @"Choose a Source"
#define MSG_CHOOSE_LIBRARY              @"Photo Library"
#define MSG_CHOOSE_CAMERA               @"Camera"
#define TITLE_INSTAGRAM_APP             @"Download Instagram App"
#define MSG_INSTAGRAM_APP               @"Please download instagram app to proceed further."
#define ERROR_MSG                       @"Something went wrong !"

#endif /* Messages_h */
