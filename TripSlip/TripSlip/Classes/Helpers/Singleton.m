//
//  Singleton.m
//  oneHourProject
//
//  Created by BCi New Media on 6/13/13.
//
//

#import "Singleton.h"
static Singleton *shared = NULL;

@implementation Singleton

- (id)init
{
    if ( self = [super init] )
    {

    }
    return self;
}



+ (Singleton *)sharedSingleton
{
    if ( !shared || shared == NULL )
    {
        shared = [[Singleton alloc] init];
    }
        return shared;
}
@end
