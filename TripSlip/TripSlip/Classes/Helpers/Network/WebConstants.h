//
//  Constants.h
//  BluePlan
//
//  Created by E-Nexus on 21/10/2015.
//  Copyright © 2015 Enexus Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

#define kWebServiceBaseURL @"http://olxseventyseven.com/helloletsgo/public/api/" //LIVE

#define kWebServiceUserLogin            @"login-user"
#define kWebServiceUserRegister         @"create-user"
#define KWebServiceGetAllInterest       @"all-intrests"
#define KWebServiceUserInterest         @"user-interests"
#define KWebServiceForgotPAssword       @"forget-password"
#define KWebServiceEditProfile          @"update-user"
#define KWebServiceLogout               @"logout-user"
#define KWebServiceCreateEvent          @"create-event"
#define KWebServiceGetAllEvents         @"get-events-feeds"
#define KWebServiceUserAllEvents        @"user-events"
#define KWebServiceGetAllFriends        @"my-friends"
#define KWebServiceCreatePost           @"create-post"
#define KWebServiceUserComment          @"user-comment"
#define KWebServiceGetAllComments       @"get-comments"

@end
