//
//  WebClient.m
//
//
//  Created by Developer on 14/01/2014.
//  Copyright (c) 2014 V7iTech. All rights reserved.
//

#import "WebClient.h"
#import "WebConstants.h"
//#import "ProgressHUD.h"
#import "AFHTTPRequestOperationManager.h"

@implementation UploadFile


@synthesize name,fileName,type,fileData;

@end

@implementation WebClient

+ (instancetype)sharedClient {
    static WebClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[WebClient alloc] initWithBaseURL:[NSURL URLWithString:kWebServiceBaseURL]];
        [_sharedClient setSecurityPolicy:[AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey]];
    });
    
    return _sharedClient;
}

-(void) showErrorMessage:(NSString *)message{
    
    NSString *title = NSLocalizedString(@"Error", @"Error");
    if ([message isEqualToString:@"no record found"]||[message isEqualToString:@"no records found"]||[message isEqualToString:@"no data found"]) {
        
    }
    else{
        
        [[[UIAlertView alloc] initWithTitle:title
                                    message:message
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles: nil] show];

    }
    
}


-(NSMutableArray *) parseArray:(NSArray *)array
                   ofClassType:(Class)type
                     clientKey:(NSString *)clientKey
                     serverKey:(NSString *)serverKey {
    
    NSMutableArray *objects=[NSMutableArray array];
    
    /*for (NSDictionary *objectDictionary in array) {
        
        BaseEntity *baseEntity=[BaseEntity createOrUpdateEntity:type
                                                           data:objectDictionary
                                                      serverKey:serverKey
                                                      clientKey:clientKey];
        [baseEntity initWithDictionary:objectDictionary];
        
        [objects addObject:baseEntity];
        
    }
     */
    return objects;
}
-(void)Response:(AFHTTPRequestOperation *)operation
               responseObject:(id) responseObject
             showErrorMessage:(BOOL)isShowErrorMessage
                      success:(void (^)(id responseObject))success
                      failure:(void (^)(NSError *error))failure{
    
    if(responseObject&&[responseObject isKindOfClass:[NSDictionary class]]) {
        
        NSDictionary *responseData = (NSDictionary*)responseObject;
        NSDictionary *dataDictionary = [responseData objectForKey:@"schedule"];
        success(dataDictionary);
        
    }
    
}


-(void) handleSuccessResponse:(AFHTTPRequestOperation *)operation
               responseObject:(id) responseObject
             showErrorMessage:(BOOL)isShowErrorMessage
                      success:(void (^)(id responseObject))success
                      failure:(void (^)(NSError *error))failure {
     
     if(responseObject&&[responseObject isKindOfClass:[NSDictionary class]]) {
        
        NSDictionary *responseData = (NSDictionary*)responseObject;
        
        if([[responseData objectForKey:@"status"]boolValue]==0){
            
            NSString *errorMessage = [responseData objectForKey:@"message"];
            if([errorMessage isKindOfClass:[NSArray class]])
                errorMessage=[(NSArray *)errorMessage firstObject];
            
            NSError *error=[NSError errorWithDomain:@"Server" code:0 userInfo:@{NSLocalizedDescriptionKey:errorMessage}];
            failure(error);
            
            if (error) {
                //showing error message
                [self showErrorMessage:error.localizedDescription];
            }
        }
        else {
        
            if([[responseData objectForKey:@"status"]boolValue]==1) {
                success(responseObject);
                
            }
            else {
                success(responseObject);
            }

        }
    }
    else {
        success(responseObject);
    }
    
}

- (void)postOthersPath:(NSString *)URLString
            parameters:(NSDictionary *)parameters
           showLoading:(NSString *)message
               success:(void (^)(id responseObject))success
               failure:(void (^)(NSError *error))failure\
{
    if(message) {
        //[ProgressHUD show:message];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    
    [manager POST:[NSString stringWithFormat:@"http://cuse.com/services/schd.aspx?format=json&starting=%@",URLString]//[[NSURL URLWithString:URLString relativeToURL:self.baseURL] absoluteString]
       parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              //[ProgressHUD dismiss];
              [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
              
              [self Response:operation
                           responseObject:responseObject
                         showErrorMessage:message!=nil
                                  success:success failure:failure];
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //[ProgressHUD dismiss];
              [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
              
              
              if (error && message) {
                  
                  [self showErrorMessage:[error localizedDescription]];
                  
                  if (failure) {
                      NSMutableDictionary *messageDictionary=[NSMutableDictionary dictionaryWithDictionary:error.userInfo];
                      
                      NSError *localError=[NSError errorWithDomain:error.domain
                                                              code:error.code
                                                          userInfo:messageDictionary];
                      failure(localError);
                  }
              }
          }];

    
}

- (void)deletePath:(NSString *)URLString
        parameters:(NSDictionary *)parameters
       showLoading:(NSString *)message
           success:(void (^)(id responseObject))success
           failure:(void (^)(NSError *error))failure{
    
    if(message) {
        //[ProgressHUD show:message];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndex:400];
    //manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndex:200];
    NSString *formatJson = [NSString stringWithFormat:@"%@/format/json",[[NSURL URLWithString:URLString relativeToURL:self.baseURL]absoluteString]];
    [manager DELETE:formatJson
       parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              //[ProgressHUD dismiss];
              [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
              
              [self handleSuccessResponse:operation
                           responseObject:responseObject
                         showErrorMessage:message!=nil
                                  success:success failure:failure];
              
              //success([responseObject objectForKey:@"data"]);
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //[ProgressHUD dismiss];
              [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
              if (error && message) {
                  
                  [self showErrorMessage:[error localizedDescription]];
                  
                  if (failure) {
                      NSMutableDictionary *messageDictionary=[NSMutableDictionary dictionaryWithDictionary:error.userInfo];
                      
                      NSError *localError=[NSError errorWithDomain:error.domain
                                                              code:error.code
                                                          userInfo:messageDictionary];
                      failure(localError);
                  }
              }
          }];

    
}

- (void)postPath:(NSString *)URLString
      parameters:(NSDictionary *)parameters
     showLoading:(NSString *)message
         success:(void (^)(id responseObject))success
         failure:(void (^)(NSError *error))failure
{

    if(message) {
        //[ProgressHUD show:message];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    }
  
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndex:400];
    manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndex:200];
    NSString *formatJson = [NSString stringWithFormat:@"%@",[[NSURL URLWithString:URLString relativeToURL:self.baseURL]absoluteString]];
    [manager POST:formatJson
       parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
             // [ProgressHUD dismiss];
              [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
              
              [self handleSuccessResponse:operation
                           responseObject:responseObject
                         showErrorMessage:message!=nil
                                  success:success failure:failure];
              
              //success([responseObject objectForKey:@"data"]);
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //[ProgressHUD dismiss];
              [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
             /* if (error && message) {
                  
                  [self showErrorMessage:[error localizedDescription]];
                  
                  if (failure) {
                      NSMutableDictionary *messageDictionary=[NSMutableDictionary dictionaryWithDictionary:error.userInfo];
                      
                      NSError *localError=[NSError errorWithDomain:error.domain
                                                              code:error.code
                                                          userInfo:messageDictionary];
                      failure(localError);
                  }
              }
              */
              //[ProgressHUD dismiss];
              NSLog(@"error code %ld",[operation.response statusCode]);
              failure(error);
              [self showErrorMessage:[error localizedDescription]];
              /*if (error && message) {
               //NSInteger statusCode = operation.response.statusCode;
               [self showErrorMessage:[error localizedDescription]];
               
               
               if (failure) {
               NSMutableDictionary *messageDictionary=[NSMutableDictionary dictionaryWithDictionary:error.userInfo];
               
               NSError *localError=[NSError errorWithDomain:error.domain
               code:error.code
               userInfo:messageDictionary];
               failure(localError);
               }
               
               }
               */

          }];
}

- (void)getPath:(NSString *)URLString
     parameters:(NSDictionary *)parameters
    showLoading:(NSString *)message
        success:(void (^)(id responseObject))success
        failure:(void (^)(NSError *error))failure
{
  
    if(message) {
        //[ProgressHUD show:message];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    }
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    //manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndex:400];
    NSString *formatJson = [NSString stringWithFormat:@"%@",[[NSURL URLWithString:URLString relativeToURL:self.baseURL]absoluteString]];
    NSLog(@"The url send to server is :%@",formatJson);
    [manager GET:formatJson
       parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              //[ProgressHUD dismiss];
              [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
              [self handleSuccessResponse:operation
                           responseObject:responseObject
                         showErrorMessage:message!=nil
                                  success:success failure:failure];
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              
              //[ProgressHUD dismiss];
              [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
              NSLog(@"error code %ld",[operation.response statusCode]);
              failure(error);
              [self showErrorMessage:[error localizedDescription]];
              /*if (error && message) {
                  //NSInteger statusCode = operation.response.statusCode;
                    [self showErrorMessage:[error localizedDescription]];
                  
                  
                  if (failure) {
                      NSMutableDictionary *messageDictionary=[NSMutableDictionary dictionaryWithDictionary:error.userInfo];
                      
                      NSError *localError=[NSError errorWithDomain:error.domain
                                                              code:error.code
                                                          userInfo:messageDictionary];
                      failure(localError);
                  }
               
              }
               */
          }];
}

- (void)postPathMultipart:(NSString *)URLString
               parameters:(NSDictionary *)parameters
              showLoading:(NSString *)message
                    files:(NSArray *)filesArray
                  success:(void (^)(id responseObject))success
                  failure:(void (^)(NSError *error))failure
{
    if(message) {
        //[ProgressHUD show:message];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    }

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    //manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndex:400];
    //manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndex:200];
    NSString *formatJson = [NSString stringWithFormat:@"%@",[[NSURL URLWithString:URLString relativeToURL:self.baseURL]absoluteString]];
    [manager
     POST:formatJson
     parameters:parameters
     constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
         
         for (UploadFile *file in filesArray ) {
             
             if (file.fileData) {
                 [formData appendPartWithFileData:file.fileData
                                             name:file.name
                                         fileName:file.fileName
                                         mimeType:file.type];
             }
         }
     } success:^(AFHTTPRequestOperation *operation, id responseObject) {
         //[ProgressHUD dismiss];
         [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
         NSLog(@"%@",responseObject);
         
         [self handleSuccessResponse:operation
                      responseObject:responseObject
                    showErrorMessage:message!=nil
                             success:success failure:failure];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         //[ProgressHUD dismiss];
         [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
         NSLog(@"error code %ld",[operation.response statusCode]);
         failure(error);
         [self showErrorMessage:[error localizedDescription]];
         /*if (error && message) {
          //NSInteger statusCode = operation.response.statusCode;
          [self showErrorMessage:[error localizedDescription]];
          
          
          if (failure) {
          NSMutableDictionary *messageDictionary=[NSMutableDictionary dictionaryWithDictionary:error.userInfo];
          
          NSError *localError=[NSError errorWithDomain:error.domain
          code:error.code
          userInfo:messageDictionary];
          failure(localError);
          }
          
          }
          */
         
     }];
}

- (void)putMultiPath:(NSString *)URLString
          parameters:(NSDictionary *)parameters
         showLoading:(NSString *)message
             success:(void (^)(id responseObject))success
             failure:(void (^)(NSError *error))failure{
    
    if(message) {
        //[ProgressHUD show:message];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndex:400];
    manager.responseSerializer.acceptableStatusCodes = [NSIndexSet indexSetWithIndex:200];
    NSString *formatJson = [NSString stringWithFormat:@"%@/format/json",[[NSURL URLWithString:URLString relativeToURL:self.baseURL]absoluteString]];
    [manager PUT:formatJson
       parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
              //[ProgressHUD dismiss];
              [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
              
              [self handleSuccessResponse:operation
                           responseObject:responseObject
                         showErrorMessage:message!=nil
                                  success:success failure:failure];
              
              //success([responseObject objectForKey:@"data"]);
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              //[ProgressHUD dismiss];
              [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
              /*if (error && message) {
                  
                  [self showErrorMessage:[error localizedDescription]];
                  
                  if (failure) {
                      NSMutableDictionary *messageDictionary=[NSMutableDictionary dictionaryWithDictionary:error.userInfo];
                      
                      NSError *localError=[NSError errorWithDomain:error.domain
                                                              code:error.code
                                                          userInfo:messageDictionary];
                      failure(localError);
                  }
              }
               */
              //[ProgressHUD dismiss];
              NSLog(@"error code %ld",[operation.response statusCode]);
              failure(error);
              [self showErrorMessage:[error localizedDescription]];
              /*if (error && message) {
               //NSInteger statusCode = operation.response.statusCode;
               [self showErrorMessage:[error localizedDescription]];
               
               
               if (failure) {
               NSMutableDictionary *messageDictionary=[NSMutableDictionary dictionaryWithDictionary:error.userInfo];
               
               NSError *localError=[NSError errorWithDomain:error.domain
               code:error.code
               userInfo:messageDictionary];
               failure(localError);
               }
               
               }
               */

          }];

}

- (void)postMultipleImages:(NSString *)URLString
               parameters:(NSDictionary *)parameters
              showLoading:(NSString *)message
                    files:(NSMutableArray *)filesArray
                  success:(void (^)(id responseObject))success
                  failure:(void (^)(NSError *error))failure{
    
    if(message) {
        //[ProgressHUD show:message];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
    }
    
    NSMutableURLRequest *request;
    AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
    //set images
    for (int i = 0; i<[filesArray count]; i++)
    {
        request =
        [serializer multipartFormRequestWithMethod:@"POST" URLString:[[NSURL URLWithString:URLString relativeToURL:self.baseURL] absoluteString]
                                        parameters:parameters
                         constructingBodyWithBlock:^(id <AFMultipartFormData> formData)
         {
             
             [formData appendPartWithFileData:UIImageJPEGRepresentation([filesArray objectAtIndex:i], 0.1)
                                         name:@"subImg"
                                     fileName:[NSString stringWithFormat:@"EventImage0%d.jpg",i+1]
                                     mimeType:@"image/jpeg"];
             
         }];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        AFHTTPRequestOperation *operation =
        [manager HTTPRequestOperationWithRequest:request
                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                             //[ProgressHUD dismiss];
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             
                                             
                                             [self handleSuccessResponse:operation
                                                          responseObject:responseObject
                                                        showErrorMessage:message!=nil
                                                                 success:success failure:failure];
                                             

                                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                             //[ProgressHUD dismiss];
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             
                                             if (error) {
                                                 if (failure) {
                                                     failure(error);
                                                     [self showErrorMessage:error.localizedDescription];
                                                 }
                                             }

                                             
                                         }];
        
        // 4. Set the progress block of the operation.
        /*[operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
         long long totalBytesWritten,
         long long totalBytesExpectedToWrite)
         {
         NSLog(@"Wrote %lld/%lld", totalBytesWritten, totalBytesExpectedToWrite);
         float uploadPercentge = (float)totalBytesWritten / (float)totalBytesExpectedToWrite;
         float uploadActualPercentage = uploadPercentge * 100;
         NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
         NSLog(@"Multipartdata upload in progress: %@",[NSString stringWithFormat:@"%.2f %%",uploadActualPercentage]);
         NSLog(@"test progress = %f",uploadActualPercentage);
         
         if (uploadActualPercentage >= 100)
         {
         
         NSLog(@"main progress = %f",uploadPercentge);
         
         }
         
         //[self performSelector:@selector(next) withObject:nil afterDelay:0.5];
         }];
         */
        
        // 5. Begin!
        [operation start];
        
    }
}

@end
