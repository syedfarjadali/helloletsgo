//
//  WebClient.h
//  
//
//  Created by Developer on 14/01/2014.
//  Copyright (c) 2014 V7iTech. All rights reserved.
//

#import "AFHTTPSessionManager.h"


#define WEB_MANAGER [WebClient sharedClient]


@interface  UploadFile : NSObject

@property(nonatomic,strong) NSString *name,*fileName,*type;
@property(nonatomic,strong) NSData *fileData;


@end    


@interface WebClient : AFHTTPSessionManager
{

}

+ (instancetype)sharedClient;

- (void)postOthersPath:(NSString *)URLString
      parameters:(NSDictionary *)parameters
     showLoading:(NSString *)message
         success:(void (^)(id responseObject))success
         failure:(void (^)(NSError *error))failure;

- (void)postPath:(NSString *)URLString
      parameters:(NSDictionary *)parameters
     showLoading:(NSString *)message
         success:(void (^)(id responseObject))success
         failure:(void (^)(NSError *error))failure;

- (void)deletePath:(NSString *)URLString
      parameters:(NSDictionary *)parameters
     showLoading:(NSString *)message
         success:(void (^)(id responseObject))success
         failure:(void (^)(NSError *error))failure;

- (void)getPath:(NSString *)URLString
     parameters:(NSDictionary *)parameters
    showLoading:(NSString *)message
        success:(void (^)(id responseObject))success
        failure:(void (^)(NSError *error))failure;

- (void)postPathMultipart:(NSString *)URLString
               parameters:(NSDictionary *)parameters
               showLoading:(NSString *)message
                    files:(NSArray *)filesArray
                  success:(void (^)(id responseObject))success
                  failure:(void (^)(NSError *error))failure;

- (void)postMultipleImages:(NSString *)URLString
                parameters:(NSDictionary *)parameters
                    showLoading:(NSString *)message
                        files:(NSMutableArray *)filesArray
                        success:(void (^)(id responseObject))success
                        failure:(void (^)(NSError *error))failure;

- (void)putMultiPath:(NSString *)URLString
      parameters:(NSDictionary *)parameters
     showLoading:(NSString *)message
         success:(void (^)(id responseObject))success
         failure:(void (^)(NSError *error))failure;

@end
