//
//  Singleton.h
//  oneHourProject
//
//  Created by BCi New Media on 6/13/13.
//
//

#import <Foundation/Foundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface Singleton : NSObject

@property (readwrite) BOOL checkInterest;
@property (nonatomic, strong) FBSDKAccessToken *currentAccessToken;
@property (nonatomic, strong) FBSDKLoginManager *loginManager;
@property (nonatomic, retain) NSString *tokenFb;
+ (Singleton *)sharedSingleton;
@end
