//
//  HelloLetsGoGlobals.m
//  TripSlip
//
//  Created by bsqptech3 on 11/2/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "HelloLetsGoGlobals.h"

@implementation HelloLetsGoGlobals

+(BOOL)isNetworkReachable{
    if ([AFNetworkReachabilityManager sharedManager].reachable == YES) {
        return YES;
    }else
        return NO;
}

+ (void) showAlertWithTitle:(NSString *)title AndMessage:(NSString *)message OnController:(UIViewController*)controller
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Ok"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    //Handel your yes please button action here
                                    
                                    
                                }];
    
    
    [alert addAction:yesButton];
    
    [controller presentViewController:alert animated:YES completion:nil];
}
/*+ (void)showGlobalProgressHUDWithTitle:(NSString *)title currentView:(UIView *)currentView
{
    [self dismissGlobalHUD];
    progressHUD = [[MBProgressHUD alloc] initWithView:currentView];
    [currentView addSubview:progressHUD];
    progressHUD.labelText = title;
    [AppDelegate sharedObjectManager].window.userInteractionEnabled=NO;
    progressHUD.dimBackground = NO;
    [progressHUD show:YES];
}

+ (void)dismissGlobalHUD {
    if (progressHUD) {
        [progressHUD hide:YES];
        [progressHUD removeFromSuperview];
        progressHUD = nil;
        [AppDelegate sharedObjectManager].window.userInteractionEnabled=YES;
    }
}
 */

@end
