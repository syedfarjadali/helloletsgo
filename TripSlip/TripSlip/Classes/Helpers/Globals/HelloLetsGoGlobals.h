//
//  HelloLetsGoGlobals.h
//  TripSlip
//
//  Created by bsqptech3 on 11/2/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworkReachabilityManager.h"

@interface HelloLetsGoGlobals : NSObject

+(BOOL)isNetworkReachable;
+ (void) showAlertWithTitle:(NSString *)title AndMessage:(NSString *)message OnController:(UIViewController*)controller;
//+ (void)showGlobalProgressHUDWithTitle:(NSString *)title currentView:(UIView *)currentView;
//+ (void)dismissGlobalHUD;
@end
