//
//  SearchVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/24/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchVC : UIViewController
{
    UISearchBar *searchBar;
    int controlSegmented;
    UIView *circleView;
    UIView *circleViewReq;
    UILabel *circleViewReqLabel;
    UILabel *notificationLabel;
    UIButton *notifitionIcon;
}
@property (nonatomic, weak)IBOutlet UITableView *searchTable;;
@property (nonatomic, weak)IBOutlet UISegmentedControl *segmented;
- (void)didChangeSegmentControl:(UISegmentedControl *)control;

@end
