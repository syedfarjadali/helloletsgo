//
//  SearchVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/24/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "SearchVC.h"
#import "NewsFeedCell.h"
#import "FriendListCell.h"
#import "EventsCell.h"
#import "MapVC.h"
#import "CreateEventVC.h"

@interface SearchVC () <UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>

@end

@implementation SearchVC

- (void)viewDidLoad {
//    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0.0, 0.0, 200.0, 44.0)];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:searchBar];
//    searchBar.placeholder = @"Search";
//    searchBar.delegate = self;
    [self setNavigationBarButtons];
    [self setNAvigationBar];
    self.segmented.selectedSegmentIndex = 0;
    
    [self.segmented addTarget:self
                       action:@selector(didChangeSegmentControl:)
             forControlEvents:UIControlEventValueChanged];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setNAvigationBar{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigation_bar"]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.layer.shadowColor = [[UIColor whiteColor] CGColor]; self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f); self.navigationController.navigationBar.layer.shadowRadius = 4.0f;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.7f;
}
- (void)setNavigationBarButtons{
    //
    circleView = [[UIView alloc] initWithFrame:CGRectMake(15,9,18,18)];
    circleView.layer.cornerRadius = 9;
    circleView.backgroundColor = [UIColor colorWithRed:4.0/255.0 green:21.0/255.0 blue:85.0/255.0 alpha:1.0];
    
    circleViewReq = [[UIView alloc] initWithFrame:CGRectMake(15,9,18,18)];
    circleViewReq.layer.cornerRadius = 9;
    circleViewReq.backgroundColor = [UIColor colorWithRed:4.0/255.0 green:21.0/255.0 blue:85.0/255.0 alpha:1.0];
    //
    notificationLabel = [[UILabel alloc] init];
    [notificationLabel setFrame:CGRectMake(0, 0, 18, 18)];
    [notificationLabel setTextAlignment:NSTextAlignmentCenter];
    notificationLabel.backgroundColor=[UIColor clearColor];
    notificationLabel.textColor=[UIColor whiteColor];
    [notificationLabel setFont:[UIFont fontWithName:@"Helvetica" size:8]];
    notificationLabel.userInteractionEnabled=NO;
    notificationLabel.text= @"10";
    //
    circleViewReqLabel = [[UILabel alloc] init];
    [circleViewReqLabel setFrame:CGRectMake(0, 0, 18, 18)];
    [circleViewReqLabel setTextAlignment:NSTextAlignmentCenter];
    circleViewReqLabel.backgroundColor=[UIColor clearColor];
    circleViewReqLabel.textColor=[UIColor whiteColor];
    [circleViewReqLabel setFont:[UIFont fontWithName:@"Helvetica" size:8]];
    circleViewReqLabel.userInteractionEnabled=NO;
    circleViewReqLabel.text= @"10";
    //
    UIButton *leftIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [leftIcon setImage:[UIImage imageNamed:@"friend_request"] forState:UIControlStateNormal];
    [leftIcon addTarget:self action:@selector(friendReq) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *eventCreateIcon = [[UIButton alloc] initWithFrame:CGRectMake(42, 0, 24, 24)];
    [eventCreateIcon setImage:[UIImage imageNamed:@"eventCreateIcon"] forState:UIControlStateNormal];
    [eventCreateIcon addTarget:self action:@selector(createEvent) forControlEvents:UIControlEventTouchUpInside];
    //
    notifitionIcon =  [UIButton buttonWithType:UIButtonTypeCustom];
    [notifitionIcon setImage:[UIImage imageNamed:@"notification"] forState:UIControlStateNormal];
    [notifitionIcon addTarget:self action:@selector(notifications) forControlEvents:UIControlEventTouchUpInside];
    [notifitionIcon setFrame:CGRectMake(0, 0, 24, 24)];
    //
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 24)];
    [rightBarButtonItems addSubview:notifitionIcon];
    [rightBarButtonItems addSubview:eventCreateIcon];
    [notifitionIcon addSubview:circleView];
    [circleView addSubview:notificationLabel];
    [leftIcon addSubview:circleViewReq];
    [circleViewReq addSubview:circleViewReqLabel];
    
    //
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftIcon];

}

- (void)friendReq{
    
    UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendReqNAv"];
    [self presentViewController:vc
                       animated:YES
                     completion:^{
                         
                     }];
}
- (void)notifications{
    
    UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationNav"];
    [self presentViewController:vc
                       animated:YES
                     completion:^{
                         
                     }];
}

- (void)createEvent{
    CreateEventVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"CreateEventVC"];
    [self.navigationController pushViewController:obj animated:YES];
}
- (void)didChangeSegmentControl:(UISegmentedControl *)control {
    
    if (control.selectedSegmentIndex == 0) {
        controlSegmented = 0;
        [self.searchTable reloadData];
    }
    
    if (control.selectedSegmentIndex == 1) {
        controlSegmented = 1;
        [self.searchTable reloadData];

    }
    
    if (control.selectedSegmentIndex == 2) {
        controlSegmented = 2;
        [self.searchTable reloadData];

    }
    
    if (control.selectedSegmentIndex == 3) {
        controlSegmented = 3;
        [self.searchTable reloadData];

    }
    
}
#pragma mark - UISearchBar Delegates
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    self.navigationController.navigationBar.hidden = TRUE;
    CGRect r = self.view.frame;
    r.origin.y = -44;
    r.size.height += 44;
    self.view.frame = r;
    
    [searchBar setShowsCancelButton:YES animated:YES];
}


-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:NO animated:YES];
}

#pragma mark - Protocol conformance
#pragma mark - UITableViewDelegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (controlSegmented == 0) {
        return 69.0f;
    }
    if (controlSegmented == 1) {
        return 55.0f;

    }
    if (controlSegmented == 2) {
        return 340.0f;

    }
    if (controlSegmented == 3) {
        return 55.0f;

    }
    return 0.0f;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"FriendListCell";
    static NSString *simpleTableNewsFeedIdentifier = @"NewsFeedCell";
    static NSString *simpleTableEventsIdentifier = @"EventsCell";
    
    if (controlSegmented == 0) {
        EventsCell *cell = (EventsCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableEventsIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EventsCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.backgroundColor = [UIColor clearColor];
        [cell.locationBtn setHidden:NO];
        [cell.locationBtn addTarget:self action:@selector(locationClicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.locationBtn.tag = indexPath.row;
        
        return cell;
    }
    if (controlSegmented == 1) {
        FriendListCell *cell = (FriendListCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FriendListCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    if (controlSegmented == 2) {
        NewsFeedCell *cell = (NewsFeedCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableNewsFeedIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NewsFeedCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    if (controlSegmented == 3) {
        FriendListCell *cell = (FriendListCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FriendListCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    data = [self.userDataArr objectAtIndex:indexPath.row];
    //    UserBusinessDetailViewController *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"UserBusinessDetail"];
    //    obj.title = data.businessName;
    //    obj.pageDetailData = self.userDataArr;
    //    obj.pageId = data.idBusiness;
    //    obj.tag = (int)indexPath.row;
    //    [self.navigationController pushViewController:obj animated:YES];
}

- (void)locationClicked:(UIButton *)sender{
    MapVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"MapVC"];
    [self.navigationController pushViewController:obj animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
