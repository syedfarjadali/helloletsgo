//
//  ConfirmPaymentVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/24/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmPaymentVC : UIViewController

@property (nonatomic, weak)IBOutlet UIButton *confirmPayBtn;
@end
