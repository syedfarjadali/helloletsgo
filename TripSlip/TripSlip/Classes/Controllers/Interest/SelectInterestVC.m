//
//  SelectInterestVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/16/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "SelectInterestVC.h"
#import "SelectInterestCell.h"

@interface SelectInterestVC () <UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) FFLoadingView *loadingView;

@end

@implementation SelectInterestVC
@synthesize interestArray=_interestArray;
- (void)viewDidLoad {
    //alloc dataArray
    self.interestArray = [[NSMutableArray alloc]init];
    self.selectedArray = [[NSMutableArray alloc]init];
    self.selectedNames = [[NSMutableArray alloc]init];
    [self getAllInterest];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    obj = [Singleton sharedSingleton];
    if (obj.checkInterest == YES) {
        self.navigationItem.leftBarButtonItem.title = @"Cancel";
        NSString *interestUser = [[NSUserDefaults standardUserDefaults]objectForKey:USER_INTEREST];
        NSString *interestUserIds = [[NSUserDefaults standardUserDefaults]objectForKey:USER_INTEREST_ID];
        if (interestUser.length == 0||[interestUser isEqualToString:@""]) {
            
        }
        else{
            self.selectedArray = [[interestUserIds componentsSeparatedByString:@","] mutableCopy];
            self.selectedNames = [[interestUser componentsSeparatedByString:@","] mutableCopy];
            //NSLog(@"%@",self.selectedNames);
            //NSLog(@"%@",self.selectedArray);
        }
    }
    else{
        self.navigationItem.leftBarButtonItem.title = @"Skip";

    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Custom Accessors
- (void)addLoader{
    self.loadingView = [[FFLoadingView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-25, [UIScreen mainScreen].bounds.size.height/2-25, 50, 50)];
    self.loadingView.lineWidth = 2;
    self.loadingView.strokeColor = [UIColor colorWithRed:87.0/255.0 green:116.0/255.0 blue:136.0/255.0 alpha:1.0];
    self.loadingView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.loadingView];
    [self.loadingView startLoading];
    [self.view setUserInteractionEnabled:NO];
}

- (void)removeLoader{
    [self.loadingView endLoading];
    [self performSelector:@selector(remove) withObject:nil afterDelay:1.0];
}

- (void)remove{
    [self.view setUserInteractionEnabled:YES];
    [self.loadingView removeFromSuperview];
}
- (void)getAllInterest{
    [self addLoader];
    [WEB_MANAGER getPath:KWebServiceGetAllInterest
              parameters:nil
             showLoading:nil
                 success:^(id responseObject) {
                     NSLog(@":%@", responseObject);
                     [self removeLoader];
                     [self.interestArray removeAllObjects];
                     if ([[responseObject valueForKey:RESPONSE_STATUS] boolValue] == 1) {
                         NSDictionary *data = [responseObject valueForKey:@"data"];
                         for (NSDictionary *userDictionary in data)
                         {
                             [self.interestArray addObject:userDictionary];
                             
                             
                         }
                         dispatch_async(dispatch_get_main_queue(), ^{
                             [self removeLoader];
                             [self.interestTable reloadData];
                         });
                         
                     }
                     
                 }
                 failure:^(NSError *error) {
                                          
                 }];
    
}

- (void)saveInterest:(NSString*)interest{
    [self addLoader];
    NSDictionary *params = @{@"category_id":interest,
                             @"user_id":[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]
                             };
    
    [WEB_MANAGER postPath:KWebServiceUserInterest
               parameters:params
              showLoading:nil
                  success:^(id responseObject) {
                      NSLog(@":%@", responseObject);
                      [self removeLoader];
                      if ([[responseObject objectForKey:RESPONSE_STATUS]boolValue] == 1){
                          NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
                          [userDefaults setObject:seperatedName  forKey:USER_INTEREST];
                          [userDefaults setObject:interest  forKey:USER_INTEREST_ID];
                          [userDefaults synchronize];
                          if (obj.checkInterest == YES) {
                              [self dismissViewControllerAnimated:YES completion:nil];
                          }
                          else{
                              UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarNavRoot"];
                              [self presentViewController:vc
                                                 animated:YES
                                               completion:^{
                                                   
                                               }];
                          }
                      }
                  }
                  failure:^(NSError *error) {
                      [self removeLoader];
                  }];
    
}
#pragma mark - @IBActions
- (IBAction)Skip:(UIBarButtonItem*)sender{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@""  forKey:USER_INTEREST];
    [userDefaults setObject:@""  forKey:USER_INTEREST_ID];
    [userDefaults synchronize];
    if ([sender.title isEqualToString:@"Cancel"]) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else{
        UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarNavRoot"];
        [self presentViewController:vc
                           animated:YES
                         completion:^{
                             
                         }];
    }
}
- (IBAction)Done:(id)sender{
    if (obj.checkInterest == YES) {
        if (self.selectedArray.count == 0) {
            [HelloLetsGoGlobals showAlertWithTitle:@"HelloLetsGo" AndMessage:@"Please select interest first or choose other options" OnController:self];
        }
        else{
            seperated = [self.selectedArray componentsJoinedByString:@","];
            seperatedName = [self.selectedNames componentsJoinedByString:@","];
            [self saveInterest:seperated];
        }
    }
    else{
        if (self.selectedArray.count == 0) {
            [HelloLetsGoGlobals showAlertWithTitle:@"HelloLetsGo" AndMessage:@"Please select interest first or choose other options" OnController:self];
        }
        else{
            seperated = [self.selectedArray componentsJoinedByString:@","];
            seperatedName = [self.selectedNames componentsJoinedByString:@","];
            [self saveInterest:seperated];
        }
    }
}
- (IBAction)ViewAll:(id)sender{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@""  forKey:USER_INTEREST];
    [userDefaults setObject:@""  forKey:USER_INTEREST_ID];
    [userDefaults synchronize];
    UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarNavRoot"];
    [self presentViewController:vc
                       animated:YES
                     completion:^{
                         
                     }];
}
#pragma mark - Protocol conformance
#pragma mark - UITableViewDelegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.interestArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 58.0f;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SelectInterestCell";
    SelectInterestCell *cell = (SelectInterestCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SelectInterestCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    NSString *allInterest = [[self.interestArray objectAtIndex:indexPath.row] valueForKey:@"category_name"];//category_image
    NSString *allInterestImages = [[self.interestArray objectAtIndex:indexPath.row] valueForKey:@"category_image"];//category_image
    NSURL *url = [NSURL URLWithString:allInterestImages];
    [cell.interestImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
    cell.interestText.text = allInterest;
    [cell.checkBtn addTarget:self action:@selector(check:) forControlEvents:UIControlEventTouchUpInside];
    cell.checkBtn.tag = indexPath.row;
    if ([self.selectedNames containsObject:allInterest]) {
        [cell.checkBtn setSelected:YES];
    }
    else{
        [cell.checkBtn setSelected:NO];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)check:(UIButton*)sender{
    if ([sender isSelected]) {
        [sender setSelected: NO];
        NSString *unselected = [[self.interestArray objectAtIndex:sender.tag] valueForKey:@"id"];
        NSString *unselectedName = [[self.interestArray objectAtIndex:sender.tag] valueForKey:@"category_name"];
        [self.selectedArray removeObject:unselected];
        [self.selectedNames removeObject:unselectedName];
    } else {
        [sender setSelected: YES];
        NSString *selected = [[self.interestArray objectAtIndex:sender.tag] valueForKey:@"id"];
        NSString *selectedName = [[self.interestArray objectAtIndex:sender.tag] valueForKey:@"category_name"];
        [self.selectedArray addObject:selected];
        [self.selectedNames addObject:selectedName];

    }
}
- (void)dealloc{
    self.interestArray = nil;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
