//
//  SelectInterestVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/16/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Singleton.h"

@interface SelectInterestVC : UIViewController
{
    NSArray *interestArray;
    Singleton *obj;
    NSString *seperatedName;
    NSString *seperated;
    
}
@property (nonatomic, weak)IBOutlet UITableView *interestTable;
@property (nonatomic, strong)NSMutableArray *interestArray;
@property (nonatomic, strong)NSMutableArray *selectedArray;
@property (nonatomic, strong)NSMutableArray *selectedNames;
- (IBAction)Skip:(id)sender;
- (IBAction)Done:(id)sender;
- (IBAction)ViewAll:(id)sender;
@end
