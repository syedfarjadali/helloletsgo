//
//  LocateEventsVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/19/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "LocateEventsVC.h"
#import "EventsCell.h"
#import "LocateSpotsVC.h"
@interface LocateEventsVC () <UITableViewDelegate,UITableViewDataSource>

@end

@implementation LocateEventsVC

- (void)viewDidLoad {
    [self leftButton];
    [self setNavigationBarButtons];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)leftButton{
    UIButton *leftIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [leftIcon setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftIcon addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftIcon];
}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setNavigationBarButtons{
    //
    notifitionIcon =  [UIButton buttonWithType:UIButtonTypeCustom];
    [notifitionIcon setImage:[UIImage imageNamed:@"filterEvents"] forState:UIControlStateNormal];
    [notifitionIcon addTarget:self action:@selector(filterEvents) forControlEvents:UIControlEventTouchUpInside];
    [notifitionIcon setFrame:CGRectMake(8, 0, 24, 24)];
    //
    UIButton *homeBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [homeBtn setImage:[UIImage imageNamed:@"mapEventsIcon"] forState:UIControlStateNormal];
    [homeBtn addTarget:self action:@selector(filterMap) forControlEvents:UIControlEventTouchUpInside];
    [homeBtn setFrame:CGRectMake(44, 0, 24, 24)];
    //
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 24)];
    [rightBarButtonItems addSubview:notifitionIcon];
    [rightBarButtonItems addSubview:homeBtn];
    //
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
}

- (void)filterEvents{
    
}

- (void)filterMap{
    LocateSpotsVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"LocateSpotsVC"];
    [self.navigationController pushViewController:obj animated:YES];
}


#pragma mark - Protocol conformance
#pragma mark - UITableViewDelegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 69.0f;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"EventsCell";
    EventsCell *cell = (EventsCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EventsCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    [cell.locationBtn setHidden:YES];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
