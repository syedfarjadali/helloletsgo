//
//  ProfileVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/18/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MJPopupViewController.h"

@interface ProfileVC : UIViewController

@property (nonatomic, weak)IBOutlet UITableView *profileTable;
@property (nonatomic, strong)NSMutableArray *profileArray;

@end
