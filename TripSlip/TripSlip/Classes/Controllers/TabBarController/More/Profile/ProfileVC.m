//
//  ProfileVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/18/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "ProfileVC.h"
#import "NewsFeedCell.h"
#import "ProfileCell.h"
#import "EditProfileVC.h"
#import "AccountSettingsVC.h"
#import "EventsCell.h"
#import "AboutProfileVC.h"

@interface ProfileVC () <UITableViewDelegate,UITableViewDataSource,AboutDelegates>

@end
@implementation ProfileVC

- (void)viewDidLoad {
    self.profileArray = [[NSMutableArray alloc]init];
    [self leftButton];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [self.profileTable reloadData];
}
- (void)leftButton{
    UIButton *leftIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [leftIcon setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftIcon addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftIcon];
}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Protocol conformance
#pragma mark - UITableViewDelegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2; //one male and other female
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 150.0;
    }
    return 69.0f;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"EventsCell";
    static NSString *simpleProfileIdentifier = @"ProfileCell";
    if (indexPath.section == 0) {
        
        ProfileCell *cell = (ProfileCell *)[tableView dequeueReusableCellWithIdentifier:simpleProfileIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProfileCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.backgroundColor = [UIColor clearColor];
        cell.nameLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:USER_FIRST_NAME];
        NSURL *url = [NSURL URLWithString:[[NSUserDefaults standardUserDefaults]objectForKey:USER_IMAGE]];
        [cell.profileImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"New Folder"]];
        [cell.optionBtn addTarget:self action:@selector(optionBTn:) forControlEvents:UIControlEventTouchUpInside];
        cell.optionBtn.tag = indexPath.row;
        [cell.aboutBtn addTarget:self action:@selector(aboutBtn:) forControlEvents:UIControlEventTouchUpInside];
        cell.aboutBtn.tag = indexPath.row;
        [cell.eventBtn addTarget:self action:@selector(eventBtn:) forControlEvents:UIControlEventTouchUpInside];
        cell.eventBtn.tag = indexPath.row;
        [cell.eventBtn setSelected:YES];
        return cell;
    }
    else{
        
        EventsCell *cell = (EventsCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EventsCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
}

- (void)aboutBtn:(UIButton*)sender{
    if ([sender isSelected]) {
        [sender setSelected:NO];
    }
    else{
        [sender setSelected:YES];
        NSIndexPath *myIP = [NSIndexPath indexPathForRow:sender.tag inSection:0];
        //Type cast it to CustomCell
        ProfileCell *cell = (ProfileCell*)[self.profileTable cellForRowAtIndexPath:myIP];
        [cell.eventBtn setSelected:NO];
        AboutProfileVC *myDestination = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutProfileVC"];
        myDestination.view.frame = CGRectMake(0, 0, 270.0f, 280.0f);
        myDestination.delegate = self;
        [self presentPopupViewController:myDestination animationType:MJPopupViewAnimationSlideBottomTop];
    }
}

- (void)eventBtn:(UIButton*)sender{
    if ([sender isSelected]) {
        [sender setSelected:NO];
    }
    else{
        [sender setSelected:YES];
        NSIndexPath *myIP = [NSIndexPath indexPathForRow:sender.tag inSection:0];
        //Type cast it to CustomCell
        ProfileCell *cell = (ProfileCell*)[self.profileTable cellForRowAtIndexPath:myIP];
        [cell.aboutBtn setSelected:NO];
    }
}
- (void)optionBTn:(UIButton*)sender{
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Profile"
                                                                  message:nil
                                                           preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* editProfile = [UIAlertAction actionWithTitle:@"Edit Profile"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
    {
        /** What we write here???????? **/
        EditProfileVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileVC"];
        [self.navigationController pushViewController:obj animated:YES];
        // call method whatever u need
    }];
    
    UIAlertAction* settings = [UIAlertAction actionWithTitle:@"Settings"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
    {
        /** What we write here???????? **/
        AccountSettingsVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"AccountSettingsVC"];
        [self.navigationController pushViewController:obj animated:YES];        // call method whatever u need
    }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                       style:UIAlertActionStyleCancel
                                                     handler:^(UIAlertAction * action)
                               {
                                   /** What we write here???????? **/
                                   // call method whatever u need
                               }];
    
    [alert addAction:editProfile];
    [alert addAction:settings];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark - PopUp Delegates

- (void)closePopupUser{
    NSIndexPath *myIP = [NSIndexPath indexPathForRow:0 inSection:0];
    //Type cast it to CustomCell
    ProfileCell *cell = (ProfileCell*)[self.profileTable cellForRowAtIndexPath:myIP];
    [cell.aboutBtn setSelected:NO];
    [cell.eventBtn setSelected:YES];
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
