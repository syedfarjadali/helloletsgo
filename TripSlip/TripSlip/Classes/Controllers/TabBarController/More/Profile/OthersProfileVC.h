//
//  OthersProfileVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/23/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InterestCollectionViewCell.h"
#import "KTAwesomeCell.h"
@interface OthersProfileVC : UIViewController
{
    int byDefault;
    NSArray *array;
}
@property (nonatomic, weak)IBOutlet UITableView *othersTable;
@property (strong) KTAwesomeCell *sizingCell;
@end
