//
//  OthersProfileVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/23/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "OthersProfileVC.h"
#import "OthersProfileCell.h"
#import "NewsFeedCell.h"
#import "AboutTableViewCell.h"
#import "InterestCollectionViewCell.h"
#import "KTCenterFlowLayout.h"
#import "KTAwesomeCell.h"
#import "InterestVC.h"
#import "EventsCell.h"
#import "MapVC.h"

@interface OthersProfileVC () <UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate>

@end

@implementation OthersProfileVC

- (void)viewDidLoad {
    [self leftButton];
    self.othersTable.separatorColor = [UIColor clearColor];
    array = @[@"Birthday Events", @"Dinner Parties", @"Road Trips", @"Opening Ceremonies",@"Business Conference"];//,@"Business Conference",@"City Tour",@"Food Festivals"];
    byDefault = 0;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)leftButton{
    UIButton *leftIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [leftIcon setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftIcon addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftIcon];
}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Protocol conformance
#pragma mark - UITableViewDelegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2; //one male and other female
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    else{
        if (byDefault == 0) {
            return 20;
        }
        else{
            return 1;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 150.0;
    }
    else{
        if (byDefault == 0) {
            return 69.0f;

        }
        else{
            return 420.0f;
        }
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"EventsCell";
    static NSString *simpleProfileIdentifier = @"OthersProfileCell";
    static NSString *simpleAboutIdentifier = @"AboutCell";
    //
    if (indexPath.section == 0) {
        
        OthersProfileCell *cell = (OthersProfileCell *)[tableView dequeueReusableCellWithIdentifier:simpleProfileIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"OthersProfileCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.backgroundColor = [UIColor clearColor];
        [cell.optBtn addTarget:self action:@selector(optionBTn:) forControlEvents:UIControlEventTouchUpInside];
        cell.optBtn.tag = indexPath.row;
        cell.followBtn.layer.cornerRadius = 4.0;
        cell.followBtn.layer.masksToBounds = YES;
        [cell.aboutBtn addTarget:self action:@selector(about:) forControlEvents:UIControlEventTouchUpInside];
        cell.aboutBtn.tag = indexPath.row;
        [cell.eventBtn addTarget:self action:@selector(events:) forControlEvents:UIControlEventTouchUpInside];
        cell.eventBtn.tag = indexPath.row;
        if (byDefault == 0) {
            [cell.eventBtn setSelected:YES];
            [cell.aboutBtn setSelected:NO];
        }
        else{
            [cell.aboutBtn setSelected:YES];
            [cell.eventBtn setSelected:NO];
        }
        return cell;
    }
    else{
        if (byDefault == 0) {
            EventsCell *cell = (EventsCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EventsCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            [cell.locationBtn setHidden:NO];
            [cell.locationBtn addTarget:self action:@selector(locationClicked:) forControlEvents:UIControlEventTouchUpInside];
            cell.locationBtn.tag = indexPath.row;
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }
        else{
            AboutTableViewCell *cell = (AboutTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleAboutIdentifier];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AboutTableViewCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            cell.backgroundColor = [UIColor clearColor];
            ANTagsView *tagsView = [[ANTagsView alloc] initWithTags:array frame:CGRectMake(12, 260, cell.frame.size.width - 10, 10)];
            [tagsView setTagCornerRadius:12];
            [tagsView setTagBackgroundColor:[UIColor lightGrayColor]];
            [tagsView setTagTextColor:[UIColor whiteColor]];
            [tagsView setBackgroundColor:[UIColor whiteColor]];
            [tagsView setFrameWidth:300];
            [cell addSubview:tagsView];
            //
            [cell.showAll addTarget:self action:@selector(showAll:) forControlEvents:UIControlEventTouchUpInside];
            cell.showAll.tag = indexPath.row;
            return cell;
        }
       
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (void)showAll:(UIButton*)sender{
    InterestVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"InterestVC"];
    [self.navigationController pushViewController:obj animated:YES];
}


- (void)optionBTn:(UIButton*)sender{
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Profile"
                                                                  message:nil
                                                           preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* sendMessage = [UIAlertAction actionWithTitle:@"Send Message"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                   
                                  }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                          style:UIAlertActionStyleCancel
                                                        handler:^(UIAlertAction * action)
                                  {
                                      
                                  }];
    
   /* UIAlertAction* settings = [UIAlertAction actionWithTitle:@"Share Profile"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                           
                               }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Report Profile"
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * action)
                             {
                                 // call method whatever u need
                             }];
*/
    
    [alert addAction:sendMessage];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)locationClicked:(UIButton *)sender{
    MapVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"MapVC"];
    [self.navigationController pushViewController:obj animated:YES];
}
- (void)about:(UIButton *)sender{
    byDefault = 1;
    //[sender setSelected: YES];
    [self.othersTable reloadData];
}
- (void)events:(UIButton *)sender{
    byDefault = 0;
    //[sender setSelected: YES];
    [self.othersTable reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
