//
//  LocateSpotsVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/19/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "LocateSpotsVC.h"
#import "infoWindow.h"
@interface LocateSpotsVC ()

@end

@implementation LocateSpotsVC

- (void)viewDidLoad {
    [self leftButton];
    //Google Maps
    self.latitude = @"34.024212";
    self.longitude = @"-118.496475";
    self.mapView.myLocationEnabled = YES;
    self.mapView.mapType = kGMSTypeNormal;
    self.mapView.accessibilityElementsHidden = NO;
    self.mapView.settings.scrollGestures = YES;
    self.mapView.settings.zoomGestures = YES;
    self.mapView.settings.compassButton = YES;
    self.mapView.settings.myLocationButton = YES;
    self.mapView.delegate = self;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
     GMSMarker *saveMarker = [[GMSMarker alloc] init];
     saveMarker.position = CLLocationCoordinate2DMake([self.latitude floatValue], [self.longitude floatValue]);
     //saveMarker.icon = [UIImage imageNamed:@"mapPin.png"];
     //saveMarker.title = @"";
     //saveMarker.snippet = [sharedObj.locationArr objectAtIndex:0];
     saveMarker.map = self.mapView;
     
     self.mapView.camera = [GMSCameraPosition cameraWithLatitude:[self.latitude floatValue] longitude:[self.longitude floatValue] zoom:15.0 bearing:18.0 viewingAngle:65.0];
     [self.mapView animateToViewingAngle:45];
}
- (void)leftButton{
    UIButton *leftIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [leftIcon setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftIcon addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftIcon];
}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    infoWindow *view =  [[[NSBundle mainBundle] loadNibNamed:@"infoWindow" owner:self options:nil] objectAtIndex:0];
    //view.name.text = @"Place Name";
    //view.description.text = @"Place description";
    //view.phone.text = @"123 456 789";
    //view.placeImage.image = [UIImage imageNamed:@"customPlaceImage"];
    //view.placeImage.transform = CGAffineTransformMakeRotation(-.08);
    return view;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
