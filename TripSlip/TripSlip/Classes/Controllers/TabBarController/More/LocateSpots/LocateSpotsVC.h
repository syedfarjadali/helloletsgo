//
//  LocateSpotsVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/19/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocateSpotsVC : UIViewController<GMSMapViewDelegate>

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (nonatomic,retain)NSString *latitude;
@property (nonatomic,retain)NSString *longitude;

@end
