//
//  FriendListVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/18/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendListVC : UIViewController

@property (nonatomic, weak)IBOutlet UIButton *searchBtn;
- (IBAction)searchNewFriends:(id)sender;
@end
