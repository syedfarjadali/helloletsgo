//
//  PrivacyVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/20/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "PrivacyVC.h"

@interface PrivacyVC () <UITableViewDelegate,UITableViewDataSource>

@end

@implementation PrivacyVC

- (void)viewDidLoad {
    [self leftButton];
    arraySettingItems = @[@"Private Profile",@"Allow Tagging"];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated{
      [self.privacyTable setFrame:CGRectMake(self.privacyTable.frame.origin.x, self.privacyTable.frame.origin.y, self.privacyTable.frame.size.width,(40.0f*([arraySettingItems count])))];
}
- (void)leftButton{
    UIButton *leftIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [leftIcon setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftIcon addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftIcon];
}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*-(void)setHeightOfTableView
{
    float height=60.0f*[arraySettingItems count];
    [self.privacyTable setFrame:CGRectMake(self.privacyTable.frame.origin.x, self.privacyTable.frame.origin.y, self.privacyTable.frame.size.width,(60.0f*([arraySettingItems count])))];
}*/

#pragma mark - Protocol conformance
#pragma mark - UITableViewDelegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arraySettingItems.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"Cell"];
    }
    cell.textLabel.text = [arraySettingItems objectAtIndex:indexPath.row];
    if (indexPath.row == 0) {
        UISwitch *onoff = [[UISwitch alloc] initWithFrame: CGRectZero];
        [onoff addTarget: self action: @selector(flip:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = onoff;
        /*BOOL myBool = [[NSUserDefaults standardUserDefaults] boolForKey:@"notifications"];
         if (myBool == YES) {
         
         [onoff setOn:YES animated:YES];
         
         }
         else{
         
         [onoff setOn:NO animated:YES];
         
         }
         */
        [onoff setOnTintColor:[UIColor colorWithRed:21.0/255.0 green:99.0/255.0 blue:163.0/255.0 alpha:1.0]];
        [cell addSubview: onoff];
    }
    if (indexPath.row == 1) {
        UISwitch *onoff = [[UISwitch alloc] initWithFrame: CGRectZero];
        [onoff addTarget: self action: @selector(flip:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = onoff;
        /*BOOL myBool = [[NSUserDefaults standardUserDefaults] boolForKey:@"notifications"];
         if (myBool == YES) {
         
         [onoff setOn:YES animated:YES];
         
         }
         else{
         
         [onoff setOn:NO animated:YES];
         
         }
         */
        [onoff setOnTintColor:[UIColor colorWithRed:21.0/255.0 green:99.0/255.0 blue:163.0/255.0 alpha:1.0]];
        [cell addSubview: onoff];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    //cell.textLabel.font = [UIFont fontWithName:@"SFUIText-Light" size:40.0];
    cell.textLabel.font = [UIFont systemFontOfSize:18.0];
    cell.textLabel.textColor = [UIColor blackColor]; //redColor greenColor etc.
    
}

- (void)flip:(UISwitch*)sender{
    //NSDictionary *param;
    if ([sender isOn]) {
        
        //status = YES;
    }
    else{
        
        //status = NO;
        
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
