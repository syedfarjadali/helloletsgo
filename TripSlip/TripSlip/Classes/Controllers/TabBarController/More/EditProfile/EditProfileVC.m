//
//  EditProfileVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/19/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "EditProfileVC.h"

@interface EditProfileVC () <UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (strong, nonatomic) FFLoadingView *loadingView;

@end

@implementation EditProfileVC

- (void)viewDidLoad {
    [self leftButton];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tap];
    //
    self.updateProfile.layer.cornerRadius = 4.0;
    self.updateProfile.layer.masksToBounds = YES;
    [self setProfileData];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated{
    
}
- (void)viewWillDisappear:(BOOL)animated{
    [self setProfileData];
}
- (void)leftButton{
    UIButton *leftIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [leftIcon setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftIcon addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftIcon];
}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setProfileData{
    
    self.emailTxt.text = [[NSUserDefaults standardUserDefaults]objectForKey:USER_EMAIL];
    self.userNameTxt.text = [[NSUserDefaults standardUserDefaults]objectForKey:USER_FIRST_NAME];
    self.passwordTxt.text = [[NSUserDefaults standardUserDefaults]objectForKey:USER_PASSWORD];
    NSString *userAbout = [[NSUserDefaults standardUserDefaults]objectForKey:USER_ABOUT];
    if (userAbout == nil || userAbout == (id)[NSNull null] || [userAbout isEqualToString:@""]) {
    }
    else{
        self.aboutBox.text = userAbout;
    }
    NSString *userSocial = [[NSUserDefaults standardUserDefaults]objectForKey:USER_SOCIAL_TYPE];
    if ([userSocial isEqualToString:@"facebook"]) {
        [self.fbBtn setImage:[UIImage imageNamed:@"facebookProfile"] forState:UIControlStateNormal];
    }
    else if ([userSocial isEqualToString:@"google"]){
        [self.fbBtn setImage:[UIImage imageNamed:@"google_plusProfile"] forState:UIControlStateNormal];

    }
    else{
        [self.fbBtn setImage:[UIImage imageNamed:@"emailProfileIcon"] forState:UIControlStateNormal];
    }
    dispatch_queue_t dispatchQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_async(dispatchQueue, ^(void)
                   {
                       //[activity startAnimating];
                       //[self loadImages];
                       dispatch_sync(dispatch_get_main_queue(), ^{
                           NSURL *url = [NSURL URLWithString:[[NSUserDefaults standardUserDefaults]objectForKey:USER_IMAGE]];
                           UIImage *chosenImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
                           profileData = UIImageJPEGRepresentation(chosenImage, 0.5);
                           if (profileData) {
                               isImage = YES;
                           }
                           [self.profileImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"New Folder"]];
                       });
                   });
}
- (void)handleTap:(UITapGestureRecognizer *)recognizer
{
    // your code goes here...
    [self.view endEditing:YES];// this will do the trick
    
}
#pragma mark - IBActions
- (IBAction)camera:(id)sender{
    [self openCamera];
}
- (IBAction)photoLibrary:(id)sender{
    [self openPhotoLibrary];
}
- (IBAction)connecAccounts:(id)sender{
    
}
- (IBAction)updateProfile:(id)sender{
    if ([self.userNameTxt.text isEqualToString:[[NSUserDefaults standardUserDefaults]objectForKey:USER_NAME]]&&[self.emailTxt.text isEqualToString:[[NSUserDefaults standardUserDefaults]objectForKey:USER_EMAIL]]&&[self.passwordTxt.text isEqualToString:[[NSUserDefaults standardUserDefaults]objectForKey:USER_PASSWORD]]&&isImage == NO) {
        
    }
    else{
        [self.view endEditing:YES];
        [self addLoader];
        NSDictionary *params = @{@"user_id":[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],
                                 @"user_full_name":self.userNameTxt.text,
                                 @"user_email":self.emailTxt.text,
                                 @"user_password":self.passwordTxt.text,
                                 @"user_about":self.aboutBox.text
                                 };
        
        UploadFile *file    = [[UploadFile alloc] init];
        file.fileData       = profileData;
        file.name           = @"user_image";
        file.fileName       = @"user_image.jpg";
        file.type           = @"image/jpg";
        
        [WEB_MANAGER postPathMultipart:KWebServiceEditProfile
                            parameters:params
                           showLoading:nil
                                 files:@[file]
                               success:^(id responseObject) {
                                   NSLog(@"response:%@", responseObject);
                                   [self removeLoader];
                                   if ([[responseObject objectForKey:RESPONSE_STATUS]boolValue] == 1){
                                       NSDictionary *dictUser = [responseObject objectForKey:@"data"];
                                       NSLog(@"%@",dictUser);
                                       NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
                                       [userDefaults setObject:[dictUser objectForKey:@"id"]  forKey:USER_ID];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_email"]  forKey:USER_EMAIL];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_image"]  forKey:USER_IMAGE];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_full_name"]  forKey:USER_FIRST_NAME];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_name"]  forKey:USER_NAME];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_is_social_media"]  forKey:USER_SOCIAL];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_lat"]  forKey:USER_LATITUDE];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_long"]  forKey:USER_LONGITUDE];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_is_blocked"]  forKey:USER_BLOCKED];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_about"]  forKey:USER_ABOUT];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_social_type"]  forKey:USER_SOCIAL_TYPE];
                                       [userDefaults synchronize];
                                       [self setProfileData];
                                    
                                   }
                                   
                               }
                               failure:^(NSError *error) {
                                   [self removeLoader];
                               }];
    }
}

#pragma mark - Custom Accessors
- (void)addLoader{
    self.loadingView = [[FFLoadingView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-25, [UIScreen mainScreen].bounds.size.height/2-25, 50, 50)];
    self.loadingView.lineWidth = 2;
    self.loadingView.strokeColor = [UIColor colorWithRed:87.0/255.0 green:116.0/255.0 blue:136.0/255.0 alpha:1.0];
    self.loadingView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.loadingView];
    [self.loadingView startLoading];
    [self.view setUserInteractionEnabled:NO];
}

- (void)removeLoader{
    [self.loadingView finishSuccess:nil];
    [self performSelector:@selector(remove) withObject:nil afterDelay:1.0];
}

- (void)remove{
    [self.view setUserInteractionEnabled:YES];
    [self.loadingView removeFromSuperview];
}
-(void)openPhotoLibrary
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
    
}
-(void)openCamera{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:NULL];
}
#pragma mark - UIImagePickerDelegates
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    isImage = YES;
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.profileImageView.image = chosenImage;
    profileData = UIImageJPEGRepresentation(chosenImage, 0.5);
    [picker dismissViewControllerAnimated:NO completion:nil];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
