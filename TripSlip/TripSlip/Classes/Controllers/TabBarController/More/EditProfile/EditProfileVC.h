//
//  EditProfileVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/19/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProfileVC : UIViewController
{
    NSData *profileData;
    BOOL isImage;
    NSString *imageUrl;

}
@property (nonatomic, weak)IBOutlet UIImageView *profileImageView;
@property (nonatomic, weak)IBOutlet UIButton *updateProfile;
@property (nonatomic, weak)IBOutlet UIButton *fbBtn;
@property (nonatomic, weak)IBOutlet UIButton *googleBtn;
@property (nonatomic, weak)IBOutlet UITextView *aboutBox;
@property (nonatomic, weak)IBOutlet UITextField *emailTxt;
@property (nonatomic, weak)IBOutlet UITextField *userNameTxt;
@property (nonatomic, weak)IBOutlet UITextField *passwordTxt;

- (IBAction)camera:(id)sender;
- (IBAction)photoLibrary:(id)sender;
- (IBAction)updateProfile:(id)sender;
- (IBAction)connecAccounts:(id)sender;
@end
