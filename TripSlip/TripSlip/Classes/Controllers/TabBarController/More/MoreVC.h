//
//  MoreVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/16/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreVC : UIViewController
{
    NSArray *titles;
    NSArray *images;
    UIView *circleView;
    UIView *circleViewReq;
    UILabel *circleViewReqLabel;
    UILabel *notificationLabel;
    UIButton *notifitionIcon;
    NSString *userId;
    Singleton *sharedInstance;

}
@property (nonatomic, weak)IBOutlet UITableView *moreTable;
@end
