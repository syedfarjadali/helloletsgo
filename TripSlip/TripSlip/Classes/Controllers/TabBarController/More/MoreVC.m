//
//  MoreVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/16/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "MoreVC.h"
#import "MoreCell.h"
#import "LogoutCell.h"
#import "ProfileVC.h"
#import "FriendListVC.h"
#import "AboutUsVC.h"
#import "AccountSettingsVC.h"
#import "LocateEventsVC.h"
#import "LocateSpotsVC.h"
#import "SearchVC.h"
#import "LocateFriendsVC.h"
#import "Singleton.h"
#import <Google/SignIn.h>

@interface MoreVC () <UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) FFLoadingView *loadingView;

@end

@implementation MoreVC

- (void)viewDidLoad {
    //titles = @[@"", @"", @"",@""];
    //images = @[@"", @"", @"",@""];
    self.moreTable.separatorColor = [UIColor clearColor];
    [self setNAvigationBar];
    [self setNavigationBarButtons];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setNavigationBarButtons{
    //
    circleView = [[UIView alloc] initWithFrame:CGRectMake(15,9,18,18)];
    circleView.layer.cornerRadius = 9;
    circleView.backgroundColor = [UIColor colorWithRed:4.0/255.0 green:21.0/255.0 blue:85.0/255.0 alpha:1.0];
    
    circleViewReq = [[UIView alloc] initWithFrame:CGRectMake(15,9,18,18)];
    circleViewReq.layer.cornerRadius = 9;
    circleViewReq.backgroundColor = [UIColor colorWithRed:4.0/255.0 green:21.0/255.0 blue:85.0/255.0 alpha:1.0];
    //
    notificationLabel = [[UILabel alloc] init];
    [notificationLabel setFrame:CGRectMake(0, 0, 18, 18)];
    [notificationLabel setTextAlignment:NSTextAlignmentCenter];
    notificationLabel.backgroundColor=[UIColor clearColor];
    notificationLabel.textColor=[UIColor whiteColor];
    [notificationLabel setFont:[UIFont fontWithName:@"Helvetica" size:8]];
    notificationLabel.userInteractionEnabled=NO;
    notificationLabel.text= @"10";
    //
    circleViewReqLabel = [[UILabel alloc] init];
    [circleViewReqLabel setFrame:CGRectMake(0, 0, 18, 18)];
    [circleViewReqLabel setTextAlignment:NSTextAlignmentCenter];
    circleViewReqLabel.backgroundColor=[UIColor clearColor];
    circleViewReqLabel.textColor=[UIColor whiteColor];
    [circleViewReqLabel setFont:[UIFont fontWithName:@"Helvetica" size:8]];
    circleViewReqLabel.userInteractionEnabled=NO;
    circleViewReqLabel.text= @"10";
    //
    UIButton *leftIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [leftIcon setImage:[UIImage imageNamed:@"friend_request"] forState:UIControlStateNormal];
    [leftIcon addTarget:self action:@selector(friendReq) forControlEvents:UIControlEventTouchUpInside];
    //
    notifitionIcon =  [UIButton buttonWithType:UIButtonTypeCustom];
    [notifitionIcon setImage:[UIImage imageNamed:@"notification"] forState:UIControlStateNormal];
    [notifitionIcon addTarget:self action:@selector(notifications) forControlEvents:UIControlEventTouchUpInside];
    [notifitionIcon setFrame:CGRectMake(38, 0, 24, 24)];
    //
    UIButton *homeBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [homeBtn setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
    [homeBtn addTarget:self action:@selector(search) forControlEvents:UIControlEventTouchUpInside];
    [homeBtn setFrame:CGRectMake(44, 0, 24, 24)];
    //
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 24)];
    [rightBarButtonItems addSubview:notifitionIcon];
    //[rightBarButtonItems addSubview:homeBtn];
    [notifitionIcon addSubview:circleView];
    [circleView addSubview:notificationLabel];
    [leftIcon addSubview:circleViewReq];
    [circleViewReq addSubview:circleViewReqLabel];
    
    //
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftIcon];
}
- (void)setNAvigationBar{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigation_bar"]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.layer.shadowColor = [[UIColor whiteColor] CGColor]; self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f); self.navigationController.navigationBar.layer.shadowRadius = 4.0f;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.7f;
}

- (void)addLoader{
    self.loadingView = [[FFLoadingView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-25, [UIScreen mainScreen].bounds.size.height/2-25, 50, 50)];
    self.loadingView.lineWidth = 2;
    self.loadingView.strokeColor = [UIColor colorWithRed:87.0/255.0 green:116.0/255.0 blue:136.0/255.0 alpha:1.0];
    self.loadingView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.loadingView];
    [self.loadingView startLoading];
    [self.view setUserInteractionEnabled:NO];
}

- (void)removeLoader{
    [self.loadingView endLoading];
    [self performSelector:@selector(remove) withObject:nil afterDelay:1.0];
}

- (void)remove{
    [self.view setUserInteractionEnabled:YES];
    [self.loadingView removeFromSuperview];
}
- (void)friendReq{
    
    UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendReqNAv"];
    [self presentViewController:vc
                       animated:YES
                     completion:^{
                         
                     }];
}
- (void)notifications{
    
    UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationNav"];
    [self presentViewController:vc
                       animated:YES
                     completion:^{
                         
                     }];
}
- (void)search{
    SearchVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchVC"];
    [self.navigationController pushViewController:obj animated:YES];
}

#pragma mark - Protocol conformance
#pragma mark - UITableViewDelegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 18)];
    [label setFont:[UIFont boldSystemFontOfSize:12]];
    label.textColor = [UIColor lightGrayColor];
    NSString *string;
    if (section == 0) {
        string =@"More";//[list objectAtIndex:section];
    }
    if (section == 1) {
        string =@"Explore";//[list objectAtIndex:section];
    }
    if (section == 2) {
        string =@"About";//[list objectAtIndex:section];
    }
    if (section == 3) {
        string =@"Settings";//[list objectAtIndex:section];
    }
    if (section == 4) {
    }
    /* Section header is in 0th index... */
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor whiteColor]]; //your background color...
    return view;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5; //one male and other female
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    if (section == 1) {
        return 4;
    }
    if (section == 2) {
        return 1;
    }
    if (section == 3) {
        return 3;
    }
    if (section == 4) {
        return 1;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 4) {
        return 76.0f;
    }
    return 44.0f;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 4) {
        static NSString *simpleTableIdentifier = @"LogoutCell";
        LogoutCell *cell = (LogoutCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LogoutCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.backgroundColor = [UIColor clearColor];
        [cell.logoutBtn addTarget:self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else{
        
        static NSString *simpleTableIdentifier = @"MoreCell";
        MoreCell *cell = (MoreCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MoreCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.backgroundColor = [UIColor clearColor];
        if (indexPath.section == 0) {
            cell.titleText.text = @"Profile";
            cell.titleImageView.image = [UIImage imageNamed:@"profile_icon"];
        }
        if (indexPath.section == 1) {
            if (indexPath.row == 0) {
                cell.titleText.text = @"Friend List";
                cell.titleImageView.image = [UIImage imageNamed:@"friend_list"];

                
            }
            if (indexPath.row == 1) {
                cell.titleText.text = @"Locate Events";
                cell.titleImageView.image = [UIImage imageNamed:@"locate_event"];

                
            }
            if (indexPath.row == 2) {
                cell.titleText.text = @"Locate NearBy Spots";
                cell.titleImageView.image = [UIImage imageNamed:@"locate_nearby_event"];

                
            }
            if (indexPath.row == 3) {
                cell.titleText.text = @"Locate Friends";
                cell.titleImageView.image = [UIImage imageNamed:@"locate_nearby_event"];
                
                
            }
        }
        if (indexPath.section == 2) {
            cell.titleText.text = @"About Us";
            cell.titleImageView.image = [UIImage imageNamed:@"about_us"];

        }
        if (indexPath.section == 3) {
            if (indexPath.row == 0) {
                cell.titleText.text = @"Add Paypal Account";
                cell.titleImageView.image = [UIImage imageNamed:@"add_paypal"];

            }
            if (indexPath.row == 1) {
                cell.titleText.text = @"Account Settings";
                cell.titleImageView.image = [UIImage imageNamed:@"account_setting"];
                
            }
            if (indexPath.row == 2) {
                cell.titleText.text = @"Notifications";
                cell.titleImageView.image = [UIImage imageNamed:@"icon_notification"];
                
            }
        }
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        ProfileVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
        [self.navigationController pushViewController:obj animated:YES];
    }
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            FriendListVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendListVC"];
            [self.navigationController pushViewController:obj animated:YES];
        }
        if (indexPath.row == 3) {
            LocateFriendsVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"LocateFriendsVC"];
            [self.navigationController pushViewController:obj animated:YES];
        }
    }
    if (indexPath.section == 2) {
        AboutUsVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUsVC"];
        [self.navigationController pushViewController:obj animated:YES];
    }
    if (indexPath.section == 3) {
        if (indexPath.row == 1) {
            AccountSettingsVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"AccountSettingsVC"];
            [self.navigationController pushViewController:obj animated:YES];
        }
        if (indexPath.row == 2) {
            UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationNav"];
            [self presentViewController:vc
                               animated:YES
                             completion:^{
                                 
                             }];
        }
    }
    if (indexPath.section == 1) {
        if (indexPath.row == 1) {
            LocateEventsVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"LocateEventsVC"];
            [self.navigationController pushViewController:obj animated:YES];
        }
    }
    if (indexPath.section == 1) {
        if (indexPath.row == 2) {
            LocateSpotsVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"LocateSpotsVC"];
            [self.navigationController pushViewController:obj animated:YES];
        }
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
//cell button action
- (void)logout:(UIButton*)sender{
    sharedInstance = [Singleton sharedSingleton];
    sharedInstance.checkInterest = NO;
    [self clearCacheUser:@"user"];
}

- (void)clearCacheUser:(NSString*)user{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    userId = [userDefaults objectForKey:USER_ID];
    [userDefaults removeObjectForKey:USER_ID];
    [userDefaults removeObjectForKey:USER_EMAIL];
    [userDefaults removeObjectForKey:USER_IMAGE];
    [userDefaults removeObjectForKey:USER_FIRST_NAME];
    [userDefaults removeObjectForKey:USER_NAME];
    [userDefaults removeObjectForKey:USER_SOCIAL];
    [userDefaults removeObjectForKey:USER_LATITUDE];
    [userDefaults removeObjectForKey:USER_LONGITUDE];
    [userDefaults removeObjectForKey:USER_BLOCKED];
    [userDefaults removeObjectForKey:USER_PASSWORD];
    [userDefaults removeObjectForKey:USER_INTEREST];
    [userDefaults removeObjectForKey:USER_INTEREST_ID];
    [self logoutSocialNetworks];
}
- (void)logoutSocialNetworks{
    [sharedInstance.loginManager logOut];
    sharedInstance.currentAccessToken = nil;
    sharedInstance.tokenFb = @"";
    [[GIDSignIn sharedInstance] signOut];
    [self logoutFromApp:userId];
    
}

- (void)logoutFromApp:(id)sender{
    [self addLoader];
    NSDictionary *params = @{@"user_id":sender
                             };
    [WEB_MANAGER postPath:KWebServiceLogout
              parameters:params
             showLoading:nil
                 success:^(id responseObject) {
                     NSLog(@":%@", responseObject);
                     [self removeLoader];
                     if ([[responseObject objectForKey:RESPONSE_STATUS]boolValue] == 1){
                         [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"userLogin"];
                         UINavigationController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"NavLogin"];
                         [self presentViewController:vc
                                            animated:YES
                                          completion:^{
                                          }];
                     }
                 }
                 failure:^(NSError *error) {
                     [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"userLogin"];
                     [self removeLoader];
                     UINavigationController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"NavLogin"];
                     [self presentViewController:vc
                                        animated:YES
                                      completion:^{
                                      }];
                     
                 }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
