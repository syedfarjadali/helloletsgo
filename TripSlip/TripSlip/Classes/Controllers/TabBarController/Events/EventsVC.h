//
//  EventsVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/16/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventsModelData.h"
//EventsCell
@interface EventsVC : UIViewController
{
    int controlSegmented;
    UIView *circleView;
    UIView *circleViewReq;
    UILabel *circleViewReqLabel;
    UILabel *notificationLabel;
    UIButton *notifitionIcon;
    EventsModelData *eventData;

}
@property (nonatomic, strong)NSMutableArray *eventsArray;
@property (nonatomic, weak)IBOutlet UITableView *eventTable;
@property (nonatomic, weak)IBOutlet UISegmentedControl *segmented;

- (void)didChangeSegmentControl:(UISegmentedControl *)control;
@end
