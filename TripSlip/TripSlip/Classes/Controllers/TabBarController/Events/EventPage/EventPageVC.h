//
//  EventPageVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/25/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"

@interface EventPageVC : UIViewController
{
    int controlSegmented;
}
@property (nonatomic, weak)IBOutlet UIButton *interestedBtn;
@property (nonatomic, weak)IBOutlet UIButton *countInBtn;
@property (nonatomic, weak)IBOutlet UIButton *counterOfferBtn;
@property (nonatomic, weak)IBOutlet UISegmentedControl *segmented;
@property (nonatomic, weak)IBOutlet UITableView *eventTable;
@property (nonatomic, weak)IBOutlet UICollectionView *eventCollectionView;
@property (nonatomic, weak)IBOutlet UITextView *addressLabel;

- (void)didChangeSegmentControl:(UISegmentedControl *)control;
@end
