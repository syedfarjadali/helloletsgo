//
//  EventPageVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/25/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "EventPageVC.h"
#import "NewsFeedCell.h"
#import "EventCounterOfferVC.h"
#import "SendPaymentVC.h"
#import "ConfirmPaymentVC.h"
#import "EventDetailVC.h"
#import "RateEventsVC.h"
#import "ReportFormVC.h"
#import "FriendListCell.h"
#import "EventDetailCell.h"
#import "EventGalleryCell.h"
#import "GalleryCollectionCell.h"

@interface EventPageVC () <UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource>

@end

@implementation EventPageVC

- (void)viewDidLoad {
    [self leftButton];
    self.interestedBtn.layer.cornerRadius = 4.0;
    self.countInBtn.layer.cornerRadius = 4.0;
    self.counterOfferBtn.layer.cornerRadius = 4.0;
    self.interestedBtn.layer.masksToBounds = YES;
    self.countInBtn.layer.masksToBounds = YES;
    self.counterOfferBtn.layer.masksToBounds = YES;
    //
    self.segmented.selectedSegmentIndex = 0;
    [self.eventTable setHidden:NO];
    [self.eventCollectionView setHidden:YES];
    [self.segmented addTarget:self
                       action:@selector(didChangeSegmentControl:)
             forControlEvents:UIControlEventValueChanged];
    [self.eventCollectionView registerNib:[UINib nibWithNibName:@"GalleryCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"MyCell"];
    self.eventCollectionView.backgroundColor = [UIColor clearColor];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    self.addressLabel.dataDetectorTypes = UIDataDetectorTypeAll;
    self.addressLabel.text = @"1600 Amphitheatre Parkway Mountain View, CA 94043 United States";
}
- (void)leftButton{
    UIButton *leftIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [leftIcon setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftIcon addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftIcon];
}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didChangeSegmentControl:(UISegmentedControl *)control {
    
    if (control.selectedSegmentIndex == 0) {
        controlSegmented = 0;
        [self.eventCollectionView setHidden:YES];
        [self.eventTable setHidden:NO];
        [self.eventTable reloadData];
    }
    else{
        controlSegmented = 1;
        [self.eventTable setHidden:YES];
        [self.eventCollectionView setHidden:NO];
        [self.eventCollectionView reloadData];
        //EventDetailVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailVC"];
        //[self.navigationController pushViewController:obj animated:YES];
    }
    
    
}

- (IBAction)interestClicked:(id)sender{
    
}

- (IBAction)countInClicked:(id)sender{
    ConfirmPaymentVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ConfirmPaymentVC"];
    [self.navigationController pushViewController:obj animated:YES];
}

- (IBAction)counterClicked:(id)sender{
    EventCounterOfferVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"EventCounterOfferVC"];
    [self.navigationController pushViewController:obj animated:YES];
}

#pragma mark - Protocol conformance
#pragma mark - UITableViewDelegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    /*if (controlSegmented == 0) {
        return 2;

    }
    else{
        return 1;

    }
     */
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    /*if (controlSegmented == 1) {
        return 1;
    }
    else{
        if (section == 0) {
            return 1;
        }
        else{
            return 20;
        }
    }
     */
    if (section == 0) {
        return 1;
    }
    else{
        return 20;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*if (controlSegmented == 1) {
        return 550.0f;

    }
    else{
        if (indexPath.section == 0) {
            return 579.0f;

        }
        else{
            return 55.0f;

        }
    }
     */
    if (indexPath.section == 0) {
        return 579.0f;
        
    }
    else{
        return 55.0f;
        
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        EventDetailCell *cell = (EventDetailCell *)[tableView dequeueReusableCellWithIdentifier:@"EventDetailCell"];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EventDetailCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    else{
        FriendListCell *cell = (FriendListCell *)[tableView dequeueReusableCellWithIdentifier:@"FriendListCell"];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FriendListCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.backgroundColor = [UIColor clearColor];
        cell.unfriendBtn.layer.cornerRadius = 4.0;
        cell.unfriendBtn.layer.masksToBounds = YES;
        [cell.unfriendBtn setTitle:@"Chat" forState:UIControlStateNormal];
        return cell;
    }
    /*static NSString *simpleTableIdentifier = @"EventGalleryCell";
    if (controlSegmented == 1) {
        EventGalleryCell *cell = (EventGalleryCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EventGalleryCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.backgroundColor = [UIColor clearColor];
        [cell.collectionGallery registerNib:[UINib nibWithNibName:@"GalleryCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"MyCell"];
        cell.collectionGallery.backgroundColor = [UIColor clearColor];
        cell.collectionGallery.delegate = self;
        cell.collectionGallery.dataSource = self;
        return cell;
    }
    else{
        if (indexPath.section == 0) {
            EventDetailCell *cell = (EventDetailCell *)[tableView dequeueReusableCellWithIdentifier:@"EventDetailCell"];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EventDetailCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            cell.backgroundColor = [UIColor clearColor];
            return cell;
        }
        else{
            FriendListCell *cell = (FriendListCell *)[tableView dequeueReusableCellWithIdentifier:@"FriendListCell"];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FriendListCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            cell.backgroundColor = [UIColor clearColor];
            cell.unfriendBtn.layer.cornerRadius = 4.0;
            cell.unfriendBtn.layer.masksToBounds = YES;
            [cell.unfriendBtn setTitle:@"Chat" forState:UIControlStateNormal];
            return cell;
        }
    }
 
    return nil;
     */
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark : Collection View Datasource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 20;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(105, 105);
}

- (GalleryCollectionCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    GalleryCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyCell" forIndexPath:indexPath];
    //Add your cell Values here
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%d",(int)indexPath.row);
}
- (IBAction)option:(id)sender{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"HelloLetsGo"
                                                                  message:nil
                                                           preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* createPost = [UIAlertAction actionWithTitle:@"Rate Event"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     RateEventsVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"RateEventsVC"];
                                     [self.navigationController pushViewController:obj animated:YES];
                                     
                                 }];
    
    UIAlertAction* createEvent = [UIAlertAction actionWithTitle:@"Send Message"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                     
                                  }];
    
    
    UIAlertAction* uploadImage = [UIAlertAction actionWithTitle:@"Report Event"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      ReportFormVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"ReportFormVC"];
                                      [self.navigationController pushViewController:obj animated:YES];
                                  }];
    
    
    UIAlertAction* uploadVideo = [UIAlertAction actionWithTitle:@"Share Event"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                     
                                  }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * action)
                             {
                                 
                             }];
    
    
    [alert addAction:createPost];
    [alert addAction:createEvent];
    [alert addAction:uploadImage];
    [alert addAction:uploadVideo];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
