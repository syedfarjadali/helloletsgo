//
//  EventCounterOfferVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/25/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "EventCounterOfferVC.h"
#import "CounterOfferSentVC.h"

@interface EventCounterOfferVC ()

@end

@implementation EventCounterOfferVC

- (void)viewDidLoad {
    [self leftButton];
    //
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tap];
    //
    self.counterOfferBtn.layer.cornerRadius = 4.0;
    self.counterOfferBtn.layer.masksToBounds = YES;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)leftButton{
    UIButton *leftIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [leftIcon setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftIcon addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftIcon];
}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleTap:(UITapGestureRecognizer *)recognizer
{
    // your code goes here...
    [self.view endEditing:YES];// this will do the trick
    
}
-(IBAction)counterOfferClicked:(id)sender{
    CounterOfferSentVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"CounterOfferSentVC"];
    obj.counterText = @"1";
    [self.navigationController pushViewController:obj animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
