//
//  EventsVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/16/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "EventsVC.h"
#import "EventsCell.h"
#import "EventsAttendedCell.h"
#import "SearchVC.h"
#import "EventPageVC.h"
#import "CreateEventVC.h"
#import "MapVC.h"

@interface EventsVC () <UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) FFLoadingView *loadingView;

@end

@implementation EventsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.eventsArray = [[NSMutableArray alloc]init];
    eventData = [[EventsModelData alloc]init];
    [self setNavigationBarButtons];
    [self setNAvigationBar];
    self.segmented.selectedSegmentIndex = 0;
    [self.segmented addTarget:self
                              action:@selector(didChangeSegmentControl:)
                    forControlEvents:UIControlEventValueChanged];
    [self getEvents:@"scheduled"];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setNAvigationBar{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigation_bar"]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.layer.shadowColor = [[UIColor whiteColor] CGColor]; self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f); self.navigationController.navigationBar.layer.shadowRadius = 4.0f;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.7f;
}

- (void)setNavigationBarButtons{
    //
    circleView = [[UIView alloc] initWithFrame:CGRectMake(15,9,18,18)];
    circleView.layer.cornerRadius = 9;
    circleView.backgroundColor = [UIColor colorWithRed:4.0/255.0 green:21.0/255.0 blue:85.0/255.0 alpha:1.0];
    //
    circleViewReq = [[UIView alloc] initWithFrame:CGRectMake(15,9,18,18)];
    circleViewReq.layer.cornerRadius = 9;
    circleViewReq.backgroundColor = [UIColor colorWithRed:4.0/255.0 green:21.0/255.0 blue:85.0/255.0 alpha:1.0];
    //
    notificationLabel = [[UILabel alloc] init];
    [notificationLabel setFrame:CGRectMake(0, 0, 18, 18)];
    [notificationLabel setTextAlignment:NSTextAlignmentCenter];
    notificationLabel.backgroundColor=[UIColor clearColor];
    notificationLabel.textColor=[UIColor whiteColor];
    [notificationLabel setFont:[UIFont fontWithName:@"Helvetica" size:8]];
    notificationLabel.userInteractionEnabled=NO;
    notificationLabel.text= @"10";
    //
    circleViewReqLabel = [[UILabel alloc] init];
    [circleViewReqLabel setFrame:CGRectMake(0, 0, 18, 18)];
    [circleViewReqLabel setTextAlignment:NSTextAlignmentCenter];
    circleViewReqLabel.backgroundColor=[UIColor clearColor];
    circleViewReqLabel.textColor=[UIColor whiteColor];
    [circleViewReqLabel setFont:[UIFont fontWithName:@"Helvetica" size:8]];
    circleViewReqLabel.userInteractionEnabled=NO;
    circleViewReqLabel.text= @"10";
    //
    UIButton *leftIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [leftIcon setImage:[UIImage imageNamed:@"friend_request"] forState:UIControlStateNormal];
    [leftIcon addTarget:self action:@selector(friendReq) forControlEvents:UIControlEventTouchUpInside];
    //
    notifitionIcon =  [UIButton buttonWithType:UIButtonTypeCustom];
    [notifitionIcon setImage:[UIImage imageNamed:@"notification"] forState:UIControlStateNormal];
    [notifitionIcon addTarget:self action:@selector(notifications) forControlEvents:UIControlEventTouchUpInside];
    [notifitionIcon setFrame:CGRectMake(2, 0, 24, 24)];
    //
    UIButton *homeBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [homeBtn setImage:[UIImage imageNamed:@"eventCreateIcon"] forState:UIControlStateNormal];
    [homeBtn addTarget:self action:@selector(CreateEvent) forControlEvents:UIControlEventTouchUpInside];
    [homeBtn setFrame:CGRectMake(44, 0, 24, 24)];
    //
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 24)];
    [rightBarButtonItems addSubview:notifitionIcon];
    [rightBarButtonItems addSubview:homeBtn];
    [notifitionIcon addSubview:circleView];
    [circleView addSubview:notificationLabel];
    [leftIcon addSubview:circleViewReq];
    [circleViewReq addSubview:circleViewReqLabel];
    //
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftIcon];
}
#pragma mark - Custom Accessors
- (void)addLoader{
    self.loadingView = [[FFLoadingView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-25, [UIScreen mainScreen].bounds.size.height/2-25, 50, 50)];
    self.loadingView.lineWidth = 2;
    self.loadingView.strokeColor = [UIColor colorWithRed:87.0/255.0 green:116.0/255.0 blue:136.0/255.0 alpha:1.0];
    self.loadingView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.loadingView];
    [self.loadingView startLoading];
    [self.view setUserInteractionEnabled:NO];
}

- (void)removeLoader{
    [self.loadingView endLoading];
    [self performSelector:@selector(remove) withObject:nil afterDelay:1.0];
}

- (void)remove{
    [self.view setUserInteractionEnabled:YES];
    [self.loadingView removeFromSuperview];
}
- (void)getEvents:(NSString*)eventByName{
    [self addLoader];
    //event service call
    NSDictionary *params = @{@"user_id":[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],
                             @"user_event_type":eventByName
                             };
    [EventsModelData getAllWithParams:params
                             success:^(id responseObject) {
                                 NSLog(@"%@",responseObject);
                                 [self removeLoader];
                                 [self.eventsArray removeAllObjects];
                                 [self.eventsArray addObjectsFromArray:responseObject];
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     [self.eventTable reloadData];
                                 });
                             }
                             failure:^(NSError *error) {
                                 [self removeLoader];
                                 [self.eventsArray removeAllObjects];
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     [self.eventTable reloadData];
                                 });
                             }];
}

- (void)friendReq{
    
    UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendReqNAv"];
    [self presentViewController:vc
                       animated:YES
                     completion:^{
                         
                     }];
}
- (void)notifications{
    //
    UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationNav"];
    [self presentViewController:vc
                       animated:YES
                     completion:^{
                         
                     }];
}
- (void)CreateEvent{
    CreateEventVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"CreateEventVC"];
    [self.navigationController pushViewController:obj animated:YES];
}

- (void)didChangeSegmentControl:(UISegmentedControl *)control {
    
    if (control.selectedSegmentIndex == 0) {
        
        controlSegmented = 0;
        [self getEvents:@"scheduled"];
        
    }
    else if (control.selectedSegmentIndex == 1)
    {
        controlSegmented = 1;
        [self getEvents:@"attended"];
        
    }
    else{
        controlSegmented = 2;
        [self getEvents:@"created"];
    }
    
    
}
#pragma mark - Protocol conformance
#pragma mark - UITableViewDelegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.eventsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 69.0f;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"EventsCell";
    if (controlSegmented == 1) {
        eventData = [self.eventsArray objectAtIndex:indexPath.row];
        EventsAttendedCell *cell = (EventsAttendedCell *)[tableView dequeueReusableCellWithIdentifier:@"EventsAttendedCell"];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EventsAttendedCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.backgroundColor = [UIColor clearColor];
        cell.ratingView.emptySelectedImage = [UIImage imageNamed:@"StarFull"];
        cell.ratingView.fullSelectedImage = [UIImage imageNamed:@"StarFull"];
        cell.ratingView.contentMode = UIViewContentModeScaleAspectFill;
        cell.ratingView.maxRating = 5;
        cell.ratingView.minRating = 0;
        cell.ratingView.editable = NO;
        cell.ratingView.halfRatings = NO;
        cell.ratingView.floatRatings = NO;
        cell.ratingView.rating = 5.0;
        //
        /*NSString *rating = data.rating;
        if (rating == nil || rating == (id)[NSNull null]) {
            
            cell.ratingView.rating = 0.0;
        }
        else{
            
            cell.ratingView.rating = [rating floatValue];
            
        }
         */
        return cell;
    }
    else{
        eventData = [self.eventsArray objectAtIndex:indexPath.row];
        EventsCell *cell = (EventsCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EventsCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.backgroundColor = [UIColor clearColor];
        [cell.locationBtn addTarget:self action:@selector(locationMap:) forControlEvents:UIControlEventTouchUpInside];
        cell.locationBtn.tag = indexPath.row;
        cell.eventName.text =eventData.eventName;
        NSURL *eventImageUrl = [NSURL URLWithString:eventData.eventImage];
        [cell.eventImage sd_setImageWithURL:eventImageUrl placeholderImage:[UIImage imageNamed:@""]];
        cell.eventDate.text = [NSString stringWithFormat:@"%@ at %@",eventData.eventStartDate,eventData.eventTime];
        cell.eventLocation.text =eventData.eventAddress;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventPageVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"EventPageVC"];
    [self.navigationController pushViewController:obj animated:YES];
}

- (void)locationMap:(UIButton*)sender{
    MapVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"MapVC"];
    obj.className = @"EventList";
    [self.navigationController pushViewController:obj animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
