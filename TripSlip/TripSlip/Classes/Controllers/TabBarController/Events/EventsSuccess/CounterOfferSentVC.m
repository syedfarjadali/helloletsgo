//
//  CounterOfferSentVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/24/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "CounterOfferSentVC.h"

@interface CounterOfferSentVC ()

@end

@implementation CounterOfferSentVC

- (void)viewDidLoad {
    if ([self.counterText isEqualToString:@""]) {
        self.counterOfferText.text = @"Your Payment Send Successfully";
    }
    else{
        self.counterOfferText.text = @"Your Counter Offer Send Successfully";
    }
    self.counterBtn.layer.cornerRadius = 4.0;
    self.counterBtn.layer.masksToBounds = YES;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBackToRoot:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
