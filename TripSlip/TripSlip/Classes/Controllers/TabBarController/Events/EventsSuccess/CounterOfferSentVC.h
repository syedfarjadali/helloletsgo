//
//  CounterOfferSentVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/24/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CounterOfferSentVC : UIViewController

@property (nonatomic, weak)IBOutlet UIButton *counterBtn;
@property (nonatomic, weak)IBOutlet UILabel *counterOfferText;
@property (nonatomic, retain)NSString *counterText;
@end
