//
//  RateEventsVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/24/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPFloatRatingView.h"
@interface RateEventsVC : UIViewController
{
    
}
@property (nonatomic, weak)IBOutlet UITextView *descriptionBox;
@property (nonatomic, weak)IBOutlet UIButton *doneRateBtn;
@property (nonatomic, weak)IBOutlet TPFloatRatingView *ratingView;
@end
