//
//  RateEventsVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/24/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "RateEventsVC.h"

@interface RateEventsVC ()

@end

@implementation RateEventsVC

- (void)viewDidLoad {
    [self leftButton];
    //
    self.descriptionBox.layer.cornerRadius = 4.0;
    self.descriptionBox.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.descriptionBox.layer.borderWidth = 1.0f;
    self.descriptionBox.layer.masksToBounds = YES;
    //
    self.doneRateBtn.layer.cornerRadius = 4.0;
    self.doneRateBtn.layer.masksToBounds = YES;
    //
    self.ratingView.emptySelectedImage = [UIImage imageNamed:@"StarFull"];
    self.ratingView.fullSelectedImage = [UIImage imageNamed:@"StarFull"];
    self.ratingView.contentMode = UIViewContentModeScaleAspectFill;
    self.ratingView.maxRating = 5;
    self.ratingView.minRating = 0;
    self.ratingView.editable = YES;
    self.ratingView.halfRatings = NO;
    self.ratingView.floatRatings = NO;
    self.ratingView.rating = 5.0;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)leftButton{
    UIButton *leftIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [leftIcon setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftIcon addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftIcon];
}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
