//
//  CreateEventVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/23/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventFeedModel.h"

@interface CreateEventVC : UIViewController
{
    NSArray *eventArray;
    NSMutableDictionary *fieldsDic;
    BOOL isImage;
    NSData *profileImageData;
    NSString *eventCategoryId;
}
@property (nonatomic, strong)NSMutableArray *interestArray;
@property (nonatomic, weak)IBOutlet UITextField *eventNameTxt;
@property (nonatomic, weak)IBOutlet UITextField *startDateTxt;
@property (nonatomic, weak)IBOutlet UITextField *endDateTxt;
@property (nonatomic, weak)IBOutlet UITextField *timeTxt;
@property (nonatomic, weak)IBOutlet UITextField *eventTypeTxt;
@property (nonatomic, weak)IBOutlet UITextView *descriptionBox;
@property (nonatomic, weak)IBOutlet UITextField *guestTxt;
@property (nonatomic, weak)IBOutlet UITextField *locationTxt;
@property (nonatomic, weak)IBOutlet UILabel *selectedFriends;
@property (nonatomic, weak)IBOutlet UIButton *createEventBtn;
@property (nonatomic, weak)IBOutlet UIButton *inviteBtn;
@property (nonatomic, weak)IBOutlet UIButton *publicEventBtn;
@property (nonatomic, weak)IBOutlet UIButton *privateEventBtn;
@property (nonatomic, weak)IBOutlet UIButton *locateEventBtn;
@property (nonatomic, weak)IBOutlet UIButton *uploadImageBtn;
@property (nonatomic, weak)IBOutlet UIButton *publicBtn;
@property (nonatomic, weak)IBOutlet UIButton *privateBtn;
@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) UIDatePicker *dateEndPicker;
@property (nonatomic, strong) UIDatePicker *timePicker;
@property (nonatomic, strong) UIPickerView *eventsPickerView;
@property (nonatomic, weak)IBOutlet UIScrollView *scrollView;
@end
