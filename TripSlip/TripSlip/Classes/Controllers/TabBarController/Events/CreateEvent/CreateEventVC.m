//
//  CreateEventVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/23/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "CreateEventVC.h"
#import "InviteFriendsVC.h"
#import "EventCreatedSuccessVC.h"
#import "ABCGooglePlacesSearchViewController.h"
#import "EventContribVC.h"

@interface CreateEventVC () <UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,ABCGooglePlacesSearchViewControllerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,MultiPickerDelegates>
@property (strong, nonatomic) FFLoadingView *loadingView;


@end

@implementation CreateEventVC

- (void)viewDidLoad {
    [self leftButton];
    [self setFieldsValues];
    [self getAllInterest];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tap];
    //
    //eventArray = @[@"Birthday", @"Anniversary", @"Bar mitzvah",@"Party",@"Road trips"];
    self.interestArray = [[NSMutableArray alloc]init];
    //
    self.descriptionBox.layer.cornerRadius = 4.0;
    self.descriptionBox.layer.borderColor = [UIColor colorWithRed:116.0f/255.0f green:121.0f/255.0f blue:145.0f/255.0f alpha:1.0].CGColor;
    self.descriptionBox.layer.borderWidth = 1.0f;
    self.descriptionBox.layer.masksToBounds = YES;
    //
    self.createEventBtn.layer.cornerRadius = 4.0;
    self.inviteBtn.layer.cornerRadius = 4.0;
    //
    self.createEventBtn.layer.masksToBounds = YES;
    self.inviteBtn.layer.masksToBounds = YES;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)leftButton{
    UIButton *leftIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [leftIcon setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftIcon addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftIcon];
}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    //[self registerForKeyboardNotifications];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
#pragma mark - Custom Accessors
- (void)addLoader{
    self.loadingView = [[FFLoadingView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-25, [UIScreen mainScreen].bounds.size.height/2-25, 50, 50)];
    self.loadingView.lineWidth = 2;
    self.loadingView.strokeColor = [UIColor colorWithRed:87.0/255.0 green:116.0/255.0 blue:136.0/255.0 alpha:1.0];
    self.loadingView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.loadingView];
    [self.loadingView startLoading];
    [self.view setUserInteractionEnabled:NO];
}

- (void)removeLoader{
    [self.loadingView endLoading];
    [self performSelector:@selector(remove) withObject:nil afterDelay:1.0];
}

- (void)remove{
    [self.view setUserInteractionEnabled:YES];
    [self.loadingView removeFromSuperview];
}
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.descriptionBox.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, self.descriptionBox.frame.origin.y-kbSize.height);
        [self.scrollView setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//get Interest List
- (void)getAllInterest{
    [self addLoader];
    [WEB_MANAGER getPath:KWebServiceGetAllInterest
              parameters:nil
             showLoading:nil
                 success:^(id responseObject) {
                     NSLog(@":%@", responseObject);
                     [self removeLoader];
                     [self.interestArray removeAllObjects];
                     if ([[responseObject valueForKey:RESPONSE_STATUS] boolValue] == 1) {
                         NSDictionary *data = [responseObject valueForKey:@"data"];
                         for (NSDictionary *userDictionary in data)
                         {
                             [self.interestArray addObject:userDictionary];
                             
                             
                         }
                         dispatch_async(dispatch_get_main_queue(), ^{
                             //[self showEventsPicker];
                             //self.eventTypeTxt.inputView = self.eventsPickerView;
                             //self.eventTypeTxt.text  = [[self.interestArray objectAtIndex:0]valueForKey:@"category_name"];
                         });
                         
                     }
                     
                 }
                 failure:^(NSError *error) {
                     [self removeLoader];
                 }];
    
}
#pragma mark - @IBActions
- (IBAction)publicClicked:(UIButton*)sender{
    if ([sender isSelected]) {
        [sender setSelected: NO];
    } else {
        [sender setSelected: YES];
        [fieldsDic setValue:@"0" forKey:@"event_is_private"];
        [self.privateEventBtn setSelected:NO];
    }
}
- (IBAction)privateClicked:(UIButton*)sender{
    if ([sender isSelected]) {
        [sender setSelected: NO];
    } else {
        [sender setSelected: YES];
        [fieldsDic setValue:@"1" forKey:@"event_is_private"];
        [self.publicEventBtn setSelected:NO];
    }
}

- (IBAction)inviteBtn:(id)sender{
    InviteFriendsVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"InviteFriendsVC"];
    obj.delegate = self;
    [self.navigationController pushViewController:obj animated:YES];
}

- (IBAction)createEventClicked:(id)sender{
    if ([self validateFields]) {
        EventContribVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"EventContribVC"];
        obj.imageData = profileImageData;
        obj.fieldsContributionDic = fieldsDic;
        [self.navigationController pushViewController:obj animated:YES];
    }
}

- (IBAction)uploadPictureClicked:(UIButton*)sender{
    if ([sender isSelected]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:MSG_ALERT_TITLE_APP_NAME message:@"Picture already uploaded, Do you want to change it again?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            [self openCameraLibrary];
            // Ok action example
        }];
        UIAlertAction *otherAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
            // Other action
        }];
        [alert addAction:okAction];
        [alert addAction:otherAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        [self openCameraLibrary];
    }
   
}

- (void)openCameraLibrary{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@""
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* cameraButton = [UIAlertAction
                                   actionWithTitle:@"Take Photo"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                       [self openCamera];
                                       
                                   }];
    UIAlertAction* libButton = [UIAlertAction
                                actionWithTitle:@"Open Library"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [self openPhotoLibrary];
                                    
                                }];
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction * action)
                                   {
                                       NSLog(@"cancel");
                                   }];
    
    
    [alert addAction:cameraButton];
    [alert addAction:libButton];
    [alert addAction:cancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark - @Custom Accessors
- (void)setFieldsValues{
    //set by default values
    fieldsDic = [[NSMutableDictionary alloc]init];
    [fieldsDic setValue:[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID] forKey:@"user_id"];
    [fieldsDic setValue:@"" forKey:@"event_name"];
    [fieldsDic setValue:@"" forKey:@"category_id"];
    [fieldsDic setValue:@"" forKey:@"event_start_date"];
    [fieldsDic setValue:@"" forKey:@"event_end_date"];
    [fieldsDic setValue:@"8:00 am" forKey:@"event_time"];
    [fieldsDic setValue:@"" forKey:@"event_expected_number_of_visitors"];
    [fieldsDic setValue:@"" forKey:@"event_location"];
    [fieldsDic setValue:@"" forKey:@"event_description"];
    [fieldsDic setValue:@"0" forKey:@"event_is_private"];
    [fieldsDic setValue:@"" forKey:@"event_gas"];
    [fieldsDic setValue:@"" forKey:@"event_parking"];
    [fieldsDic setValue:@"" forKey:@"event_hotel"];
    [fieldsDic setValue:@"" forKey:@"event_ticket"];
    [fieldsDic setValue:@"" forKey:@"event_food"];
    [fieldsDic setValue:@"" forKey:@"event_other"];
    [fieldsDic setValue:@"" forKey:@"event_additional"];
    [fieldsDic setValue:@"" forKey:@"event_friends"];
    [self.publicBtn setSelected:YES];
}
-(BOOL) validateFields{
    if (self.eventNameTxt.text.length == 0) {
        
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:@"Please, Enter your event name" OnController:self];
        return NO;
    }
    if (self.eventTypeTxt.text.length == 0) {
        
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:@"Please, Select your event category" OnController:self];
        return NO;
    }
    else  if (self.startDateTxt.text.length == 0)
    {
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:@"Please, Select your event start date" OnController:self];
        return NO;
    }
    else  if (self.endDateTxt.text.length == 0)
    {
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:@"Please, Select your event end date" OnController:self];
        return NO;
    }
    else  if (self.timeTxt.text.length == 0)
    {
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:@"Please, Select your event time" OnController:self];
        return NO;
    }
    else  if (self.guestTxt.text.length == 0)
    {
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:@"Please, Enter your event visitors" OnController:self];
        return NO;
    }
    else  if (self.locationTxt.text.length == 0)
    {
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:@"Please, Select your event location" OnController:self];
        return NO;
    }
    else  if (self.descriptionBox.text.length == 0 || [self.descriptionBox.text isEqualToString:@"Event Description"])
    {
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:@"Please, Enter your event description" OnController:self];
        return NO;
    }
    else  if(isImage == NO){
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:@"Please, Select your event image" OnController:self];
        return NO;
    }
    else  if([[fieldsDic objectForKey:@"event_is_private"]isEqualToString:@"1"]){
        if (self.selectedFriends.text.length == 0) {
            [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:@"Please, Select your friends for private event" OnController:self];
            return NO;
        }
    }
    return YES;
}
#pragma mark - UITextFields Delegates
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    CGPoint scrollPoint = CGPointMake(0, textField.frame.origin.y);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
    //
    if (textField == self.eventTypeTxt) {
        if (self.interestArray.count) {
            [self showEventsPicker];
            self.eventTypeTxt.inputView = self.eventsPickerView;
            self.eventTypeTxt.text  = [[self.interestArray objectAtIndex:0]valueForKey:@"category_name"];
        }
        else{
            
            [self getAllInterest];
            
        }
    }
    if (textField == self.startDateTxt) {
        [self showDatePicker];
        self.startDateTxt.inputView = self.datePicker;
        self.startDateTxt.text = [self formatDate:self.datePicker.date];
    }
    if (textField == self.endDateTxt) {
        [self showEndDatePicker];
        self.endDateTxt.inputView = self.dateEndPicker;
        self.endDateTxt.text = [self formatDate:self.dateEndPicker.date];
    }
    if (textField == self.timeTxt) {
        [self showTimePicker];
        self.timeTxt.inputView = self.timePicker;
        //self.timeTxt.text = [self formatTime:self.timePicker.date];
    }
    if (textField == self.locationTxt) {
        [textField resignFirstResponder];
        ABCGooglePlacesSearchViewController *searchViewController = [[ABCGooglePlacesSearchViewController alloc] init];
        [searchViewController setDelegate:self];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:searchViewController];
        [self presentViewController:navigationController animated:YES completion:nil];
    }
   
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    if (textField == self.eventNameTxt) {
        [fieldsDic setValue:textField.text forKey:@"event_name"];
    }
    if (textField == self.startDateTxt) {
        [fieldsDic setValue:textField.text forKey:@"event_start_date"];
    }
    if (textField == self.endDateTxt) {
        [fieldsDic setValue:textField.text forKey:@"event_end_date"];
    }
    if (textField == self.timeTxt) {
        [fieldsDic setValue:textField.text forKey:@"event_time"];
    }
    if (textField == self.guestTxt) {
        [fieldsDic setValue:textField.text forKey:@"event_expected_number_of_visitors"];
    }
}
-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = textField.frame.origin.y/2; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
#pragma mark - Custom Method
- (void)showEventsPicker{
    self.eventsPickerView = [[UIPickerView alloc] init];
    self.eventsPickerView.showsSelectionIndicator = YES;
    self.eventsPickerView.hidden = NO;
    self.eventsPickerView.delegate = self;
}
- (void)showTimePicker{
    // Create a date picker for the date field.
    self.timePicker = [[UIDatePicker alloc]init];
    self.timePicker.datePickerMode = UIDatePickerModeTime;
    self.timePicker.minimumDate = [NSDate dateWithTimeIntervalSinceNow:-31536000];
    [self.timePicker setDate:[NSDate date]];
    [self.timePicker addTarget:self action:@selector(updateTimeField:) forControlEvents:UIControlEventValueChanged];
}

- (void)showDatePicker{
    // Create a date picker for the date field.
    self.datePicker = [[UIDatePicker alloc]init];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.datePicker.minimumDate = [NSDate dateWithTimeIntervalSinceNow:-31536000];
    [self.datePicker setDate:[NSDate date]];
    [self.datePicker addTarget:self action:@selector(updateDateField:) forControlEvents:UIControlEventValueChanged];
    
}

- (void)showEndDatePicker{
    // Create a date picker for the date field.
    self.dateEndPicker = [[UIDatePicker alloc]init];
    self.dateEndPicker.datePickerMode = UIDatePickerModeDate;
    self.dateEndPicker.minimumDate = [NSDate dateWithTimeIntervalSinceNow:-31536000];
    [self.dateEndPicker setDate:[NSDate date]];
    [self.dateEndPicker addTarget:self action:@selector(updateEndDateField:) forControlEvents:UIControlEventValueChanged];
    
}
- (void)updateDateField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)self.startDateTxt.inputView;
    self.startDateTxt.text = [self formatDate:picker.date];
}
- (void)updateEndDateField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)self.endDateTxt.inputView;
    self.endDateTxt.text = [self formatDate:picker.date];
}

- (void)updateTimeField:(id)sender{
    UIDatePicker *picker = (UIDatePicker*)self.timePicker.inputView;
    self.timeTxt.text = [self formatTime:picker.date];
}
- (NSString *)formatDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}
- (NSString *)formatTime:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}

#pragma mark - Protocol conformance
#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView; {
    return 1;
}
//Rows in each Column

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.interestArray.count;
}

// these methods return either a plain NSString, a NSAttributedString, or a view (e.g UILabel) to display the row for the component.
-(NSString*) pickerView:(UIPickerView*)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *allInterest = [[self.interestArray objectAtIndex:row] valueForKey:@"category_name"];
    return allInterest;
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
{
    NSString *allInterest = [[self.interestArray objectAtIndex:row] valueForKey:@"category_name"];
    self.eventTypeTxt.text = allInterest;
    NSString *catId = [[self.interestArray objectAtIndex:row] valueForKey:@"id"];
    [fieldsDic setValue:catId forKey:@"category_id"];

}


#pragma mark - Protocol conformance
#pragma mark - ABCGooglePlacesSearchViewControllerDelegate

-(void)searchViewController:(ABCGooglePlacesSearchViewController *)controller didReturnPlace:(ABCGooglePlace *)place {
     NSLog(@"%@",place.formatted_address);
    //NSString *coordinatesString = [NSString stringWithFormat:@"(%f,%f)",place.location.coordinate.latitude, place.location.coordinate.longitude];
    //[self postCheckinStatusToServer:place.formatted_address];
    //[self.coordinatesLabel setText:coordinatesString];
    self.locationTxt.text = place.formatted_address;
    if (self.locationTxt.text.length != 0) {
        [self.locateEventBtn setSelected:YES];
        [fieldsDic setValue:self.locationTxt.text forKey:@"event_location"];
    }
    
}
#pragma mark - Protocol conformance
#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    CGPoint scrollPoint = CGPointMake(0, textView.frame.origin.y);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
    if (textView == self.descriptionBox) {
        
        NSString *txtViewTitle = NSLocalizedString(@"Event Description", @"Event Description");
        if ([textView.text isEqualToString:txtViewTitle]) {
            textView.text = @"";
        }
        [textView becomeFirstResponder];
    }
    
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    [fieldsDic setValue:textView.text forKey:@"event_description"];
    if (textView == self.descriptionBox) {
        
        NSString *txtViewTitle = NSLocalizedString(@"Event Description", @"Event Description");
        if ([textView.text isEqualToString:@""]) {
            textView.text = txtViewTitle;
        }
        [textView resignFirstResponder];
    }
    
}
#pragma mark Custom Methods
-(void)openPhotoLibrary
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}
-(void)openCamera{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}
#pragma mark - UIImagePickerDelegates
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    isImage = YES;
    [self.uploadImageBtn setSelected:YES];
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    profileImageData = UIImageJPEGRepresentation(chosenImage, 0.5);
    [picker dismissViewControllerAnimated:NO completion:nil];
}

/*-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];// this will do the trick
}
 */
- (void)handleTap:(UITapGestureRecognizer *)recognizer
{
    // your code goes here...
    [self.view endEditing:YES];// this will do the trick

}

#pragma mark - MultiPickerDelegates
- (void)multiFriends:(NSString *)ids name:(NSString*)name{
    [fieldsDic setValue:ids forKey:@"event_friends"];
    NSArray *arr = [name componentsSeparatedByString:@","];
    if (arr.count == 1) {
        self.selectedFriends.text = [NSString stringWithFormat:@"%@ is going",[arr objectAtIndex:0]];
    }
    else if (arr.count == 2){
        self.selectedFriends.text = [NSString stringWithFormat:@"%@ and %@ are going",[arr objectAtIndex:0],[arr objectAtIndex:1]];

    }
    else if (arr.count == 3){
        self.selectedFriends.text = [NSString stringWithFormat:@"%@,%@ and %@ are going",[arr objectAtIndex:0],[arr objectAtIndex:1],[arr objectAtIndex:2]];
        
    }
    else{
        self.selectedFriends.text = [NSString stringWithFormat:@"%@,%@ and %lu others are going",[arr objectAtIndex:0],[arr objectAtIndex:1],(unsigned long)arr.count-2];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
