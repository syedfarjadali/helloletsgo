//
//  EventContribVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/30/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "EventContribVC.h"
#import "EventCreatedSuccessVC.h"

@interface EventContribVC () <UITextFieldDelegate>
@property (strong, nonatomic) FFLoadingView *loadingView;

@end

@implementation EventContribVC

- (void)viewDidLoad {
    //NSLog(@"%@",self.fieldsContributionDic);
    //NSLog(@"%@",self.imageData);
    self.createEventBtn.layer.cornerRadius = 4.0;
    self.createEventBtn.layer.masksToBounds = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tap];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addLoader{
    self.loadingView = [[FFLoadingView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-25, [UIScreen mainScreen].bounds.size.height/2-25, 50, 50)];
    self.loadingView.lineWidth = 2;
    self.loadingView.strokeColor = [UIColor colorWithRed:87.0/255.0 green:116.0/255.0 blue:136.0/255.0 alpha:1.0];
    self.loadingView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.loadingView];
    [self.loadingView startLoading];
    [self.view setUserInteractionEnabled:NO];
}

- (void)removeLoader{
    [self.loadingView finishSuccess:nil];
    [self performSelector:@selector(remove) withObject:nil afterDelay:1.0];
}

- (void)remove{
    [self.view setUserInteractionEnabled:YES];
    [self.loadingView removeFromSuperview];
}
- (void)handleTap:(UITapGestureRecognizer *)recognizer
{
    // your code goes here...
    [self.view endEditing:YES];// this will do the trick
    
}

#pragma mark - @IBActions
- (IBAction)createEvent:(id)sender{
    [self.view endEditing:YES];
    if ([self validateFields]) {
        [self addLoader];
        NSDictionary *params = [NSDictionary dictionaryWithDictionary:self.fieldsContributionDic]; //there you have it
        NSLog(@"%@",params);
        //
        UploadFile *file    = [[UploadFile alloc] init];
        file.fileData       = self.imageData;
        file.name           = @"event_images";
        file.fileName       = @"event_images.jpg";
        file.type           = @"image/jpg";
        
        
        [WEB_MANAGER postPathMultipart:KWebServiceCreateEvent
                            parameters:params
                           showLoading:nil
                                 files:@[file]
                               success:^(id responseObject) {
                                   NSLog(@"response:%@", responseObject);
                                   [self removeLoader];
                                   if ([[responseObject objectForKey:RESPONSE_STATUS]boolValue] == 1){
                                       
                                       EventCreatedSuccessVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"EventCreatedSuccessVC"];
                                       [self.navigationController pushViewController:obj animated:YES];
                                       
                                   }
                                   
                               }
                               failure:^(NSError *error) {
                                   [self removeLoader];
                               }];
    }
    else{
        //
    }
}
-(BOOL) validateFields{
    if (self.gasTxt.text.length == 0) {
        
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:@"Please, Enter your gas amount" OnController:self];
        return NO;
    }
    if (self.parkingTxt.text.length == 0) {
        
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:@"Please, Enter your parking amount" OnController:self];
        return NO;
    }
    else  if (self.hotelTxt.text.length == 0)
    {
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:@"Please, Enter your hotel amount" OnController:self];
        return NO;
    }
    else  if (self.ticketsTxt.text.length == 0)
    {
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:@"Please, Enter your tickets amount" OnController:self];
        return NO;
    }
    else  if (self.foodTxt.text.length == 0)
    {
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:@"Please, Enter your food amount" OnController:self];
        return NO;
    }
    else  if (self.otherTxt.text.length == 0)
    {
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:@"Please, Enter your others amount" OnController:self];
        return NO;
    }
    /*else  if (self.itemsTxt.text.length == 0)
    {
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:@"Please, Select your event location" OnController:self];
        return NO;
    }
     */
    return YES;
}

#pragma mark - UITextFields Delegates
- (void)textFieldDidBeginEditing:(UITextField *)textField{
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == self.gasTxt) {
        [self.fieldsContributionDic setValue:textField.text forKey:@"event_gas"];

    }
    if (textField == self.parkingTxt) {
        [self.fieldsContributionDic setValue:textField.text forKey:@"event_parking"];
        
    }
    if (textField == self.hotelTxt) {
        [self.fieldsContributionDic setValue:textField.text forKey:@"event_hotel"];
        
    }
    if (textField == self.ticketsTxt) {
        [self.fieldsContributionDic setValue:textField.text forKey:@"event_ticket"];
        
    }
    if (textField == self.foodTxt) {
        [self.fieldsContributionDic setValue:textField.text forKey:@"event_food"];
        
    }
    if (textField == self.otherTxt) {
        [self.fieldsContributionDic setValue:textField.text forKey:@"event_other"];
        
    }
    if (textField == self.itemsTxt) {
        [self.fieldsContributionDic setValue:textField.text forKey:@"event_additional"];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//NSDictionary *params = [NSDictionary dictionaryWithDictionary:formDict]; //there you have it
//NSLog(@"%@",params);
@end
