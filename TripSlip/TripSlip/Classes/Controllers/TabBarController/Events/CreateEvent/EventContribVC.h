//
//  EventContribVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/30/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventContribVC : UIViewController
{
}
@property (nonatomic, weak)IBOutlet UITextField *gasTxt;
@property (nonatomic, weak)IBOutlet UITextField *parkingTxt;
@property (nonatomic, weak)IBOutlet UITextField *hotelTxt;
@property (nonatomic, weak)IBOutlet UITextField *ticketsTxt;
@property (nonatomic, weak)IBOutlet UITextField *foodTxt;
@property (nonatomic, weak)IBOutlet UITextField *otherTxt;
@property (nonatomic, weak)IBOutlet UITextField *itemsTxt;
//
@property (nonatomic, weak)IBOutlet UIButton *createEventBtn;
@property (nonatomic, retain)NSMutableDictionary *fieldsContributionDic;
@property (nonatomic, retain)NSData *imageData;
@end
