//
//  InviteFriendsVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/24/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "InviteFriendsVC.h"
#import "InviteFriendsCell.h"

@interface InviteFriendsVC () <UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) FFLoadingView *loadingView;

@end

@implementation InviteFriendsVC

- (void)viewDidLoad {
    _dataSource = [[NSMutableArray alloc]init];
    selectedIds = [[NSMutableArray alloc]init];
    servicesIdArr = [[NSMutableArray alloc]init];
    selectedArr = [[NSMutableArray alloc]init];
    [self leftButton];
    [self getAllFriends];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)leftButton{
    UIButton *leftIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [leftIcon setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftIcon addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftIcon];
}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - Custom Accessors
- (void)addLoader{
    self.loadingView = [[FFLoadingView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-25, [UIScreen mainScreen].bounds.size.height/2-25, 50, 50)];
    self.loadingView.lineWidth = 2;
    self.loadingView.strokeColor = [UIColor colorWithRed:87.0/255.0 green:116.0/255.0 blue:136.0/255.0 alpha:1.0];
    self.loadingView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.loadingView];
    [self.loadingView startLoading];
    [self.view setUserInteractionEnabled:NO];
}

- (void)removeLoader{
    [self.loadingView endLoading];
    [self performSelector:@selector(remove) withObject:nil afterDelay:1.0];
}

- (void)remove{
    [self.view setUserInteractionEnabled:YES];
    [self.loadingView removeFromSuperview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)done:(id)sender{
    if (selectedIds.count == 0) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:MSG_ALERT_TITLE_APP_NAME message:@"Please select friends first" preferredStyle:UIAlertControllerStyleAlert]; // 7
        
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        }]; // 8
        
        [alert addAction:defaultAction]; // 9
        
        [self presentViewController:alert animated:YES completion:nil]; // 11
    }
    else{
        
        NSString *ids = [selectedIds componentsJoinedByString:@","];
        servicesTypeName = [selectedArr componentsJoinedByString:@","];
        
        if ([_delegate respondsToSelector:@selector(multiFriends:name:)]) {
            
            [_delegate multiFriends:ids name:servicesTypeName];
        }
        [self.navigationController popViewControllerAnimated:NO];
    }
}

#pragma mark - Service Call
- (void)getAllFriends{
    [self addLoader];
    NSDictionary *params = @{@"user_id":[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]
                             };
    [WEB_MANAGER getPath:KWebServiceGetAllFriends
              parameters:params
             showLoading:nil
                 success:^(id responseObject) {
                     NSLog(@":%@", responseObject);
                     [self removeLoader];
                     if ([[responseObject objectForKey:RESPONSE_STATUS]boolValue] == 1){
                         NSDictionary *dict = [responseObject objectForKey:@"data"];
                         NSLog(@"%@",dict);
                         [_dataSource removeAllObjects];
                         [servicesIdArr removeAllObjects];
                         for (NSDictionary *userDictionary in dict)
                         {
                             [_dataSource addObject:[userDictionary objectForKey:@"user_full_name"]];
                             [servicesIdArr addObject:[userDictionary objectForKey:@"id"]];
                             
                         }
                         dispatch_async(dispatch_get_main_queue() , ^{
                             [self.friendsTableView reloadData];
                         });
                     }
                 }
                 failure:^(NSError *error) {
                     [self removeLoader];
                 }];
    
}

#pragma mark - Protocol conformance
#pragma mark - UITableViewDelegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 58.0f;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"InviteFriendsCell";
    InviteFriendsCell *cell = (InviteFriendsCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"InviteFriendsCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.nameLabel.text = [_dataSource objectAtIndex:indexPath.row];
    //NSURL *userImageUrl = [NSURL URLWithString:eventData.userImage];
    //[cell.userImage sd_setImageWithURL:userImageUrl placeholderImage:[UIImage imageNamed:@"user_placeholder"]];
    [cell.checkBtn addTarget:self action:@selector(check:) forControlEvents:UIControlEventTouchUpInside];
    cell.checkBtn.tag = indexPath.row;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)check:(UIButton*)sender{
    if ([sender isSelected]) {
        [sender setSelected: NO];
        NSLog(@"Arr Value : %@",selectedArr);
        NSLog(@"Arr Value : %@",selectedIds);
        [selectedArr removeObject:[NSString stringWithFormat:@"%@",[_dataSource objectAtIndex:sender.tag]]];
        [selectedIds removeObject:[NSString stringWithFormat:@"%@",[servicesIdArr objectAtIndex:sender.tag]]];
    } else {
        [sender setSelected: YES];
        //[selectedArr removeAllObjects];
        //[selectedIds removeAllObjects];
        [selectedArr addObject:[NSString stringWithFormat:@"%@",[_dataSource objectAtIndex:sender.tag]]];
        [selectedIds addObject:[NSString stringWithFormat:@"%@",[servicesIdArr objectAtIndex:sender.tag]]];
        NSLog(@"Arr Value : %@",selectedArr);
        NSLog(@"Arr Value : %@",selectedIds);
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
