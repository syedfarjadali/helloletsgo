//
//  InviteFriendsVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/24/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MultiPickerDelegates <NSObject>

- (void)multiFriends:(NSString *)ids name:(NSString*)name;

@end

@interface InviteFriendsVC : UIViewController
{
    NSMutableArray *_dataSource;
    NSMutableArray *servicesIdArr;
    NSMutableArray *selectedIds;
    NSMutableArray *selectedArr;
    NSString *servicesTypeName;
}
@property (nonatomic, weak)IBOutlet UITableView *friendsTableView;
@property(nonatomic,assign)id delegate;
@property (nonatomic, strong)NSMutableArray *friendsArray;
@end
