//
//  MessagesVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/16/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "MessagesVC.h"
#import "ChatListCell.h"
#import "DemoMessagesViewController.h"
#import "CreateGroupVC.h"

@interface MessagesVC () <UITableViewDataSource,UITableViewDelegate,JSQDemoViewControllerDelegate>

@end

@implementation MessagesVC

- (void)viewDidLoad {
    [self setNavigationBarButtons];
    [self setNAvigationBar];
    self.segmented.selectedSegmentIndex = 0;
    
    [self.segmented addTarget:self
                       action:@selector(didChangeSegmentControl:)
             forControlEvents:UIControlEventValueChanged];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setNavigationBarButtons{
    //
    circleView = [[UIView alloc] initWithFrame:CGRectMake(15,9,18,18)];
    circleView.layer.cornerRadius = 9;
    circleView.backgroundColor = [UIColor colorWithRed:4.0/255.0 green:21.0/255.0 blue:85.0/255.0 alpha:1.0];
    
    circleViewReq = [[UIView alloc] initWithFrame:CGRectMake(15,9,18,18)];
    circleViewReq.layer.cornerRadius = 9;
    circleViewReq.backgroundColor = [UIColor colorWithRed:4.0/255.0 green:21.0/255.0 blue:85.0/255.0 alpha:1.0];
    //
    notificationLabel = [[UILabel alloc] init];
    [notificationLabel setFrame:CGRectMake(0, 0, 18, 18)];
    [notificationLabel setTextAlignment:NSTextAlignmentCenter];
    notificationLabel.backgroundColor=[UIColor clearColor];
    notificationLabel.textColor=[UIColor whiteColor];
    [notificationLabel setFont:[UIFont fontWithName:@"Helvetica" size:8]];
    notificationLabel.userInteractionEnabled=NO;
    notificationLabel.text= @"10";
    //
    circleViewReqLabel = [[UILabel alloc] init];
    [circleViewReqLabel setFrame:CGRectMake(0, 0, 18, 18)];
    [circleViewReqLabel setTextAlignment:NSTextAlignmentCenter];
    circleViewReqLabel.backgroundColor=[UIColor clearColor];
    circleViewReqLabel.textColor=[UIColor whiteColor];
    [circleViewReqLabel setFont:[UIFont fontWithName:@"Helvetica" size:8]];
    circleViewReqLabel.userInteractionEnabled=NO;
    circleViewReqLabel.text= @"10";
    
    //
    UIButton *leftIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [leftIcon setImage:[UIImage imageNamed:@"friend_request"] forState:UIControlStateNormal];
    [leftIcon addTarget:self action:@selector(friendReq) forControlEvents:UIControlEventTouchUpInside];
    //
    notifitionIcon =  [UIButton buttonWithType:UIButtonTypeCustom];
    [notifitionIcon setImage:[UIImage imageNamed:@"notification"] forState:UIControlStateNormal];
    [notifitionIcon addTarget:self action:@selector(notifications) forControlEvents:UIControlEventTouchUpInside];
    [notifitionIcon setFrame:CGRectMake(2, 0, 24, 24)];
    //
    UIButton *homeBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [homeBtn setImage:[UIImage imageNamed:@"add"] forState:UIControlStateNormal];
    [homeBtn addTarget:self action:@selector(addNewGroup) forControlEvents:UIControlEventTouchUpInside];
    [homeBtn setFrame:CGRectMake(44, 0, 24, 24)];
    //
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 24)];
    [rightBarButtonItems addSubview:notifitionIcon];
    [rightBarButtonItems addSubview:homeBtn];
    [notifitionIcon addSubview:circleView];
    [circleView addSubview:notificationLabel];
    [leftIcon addSubview:circleViewReq];
    [circleViewReq addSubview:circleViewReqLabel];
    
    //
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftIcon];
}
- (void)setNAvigationBar{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigation_bar"]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.layer.shadowColor = [[UIColor whiteColor] CGColor]; self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f); self.navigationController.navigationBar.layer.shadowRadius = 4.0f;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.7f;
}

- (void)didChangeSegmentControl:(UISegmentedControl *)control {
    
    if (control.selectedSegmentIndex == 0) {
        controlSegmented = 0;
        [self.chatTable reloadData];
      
        
    }
    else{
        controlSegmented = 1;
        [self.chatTable reloadData];
    }
}

- (void)friendReq{
    
    UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendReqNAv"];
    [self presentViewController:vc
                       animated:YES
                     completion:^{
                         
                     }];
}
- (void)notifications{
    
    UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationNav"];
    [self presentViewController:vc
                       animated:YES
                     completion:^{
                         
                     }];
}

- (void)addNewGroup{
    //CreateGroupVC
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CreateGroupVC *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"CreateGroupVC"];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - Protocol conformance
#pragma mark - UITableViewDelegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55.0f;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"ChatListCell";
    ChatListCell *cell = (ChatListCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChatListCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DemoMessagesViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"DemoVC"];
    vc.delegateModal = self;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - Demo delegate

- (void)didDismissJSQDemoViewController:(DemoMessagesViewController *)vc
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
