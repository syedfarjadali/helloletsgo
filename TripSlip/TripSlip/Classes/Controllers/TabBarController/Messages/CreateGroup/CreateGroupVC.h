//
//  CreateGroupVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/17/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateGroupVC : UIViewController
{
    NSData *profileImageData;

}
@property (nonatomic, weak)IBOutlet UITextField *groupName;
@property (nonatomic, weak)IBOutlet UITextField *groupCategory;
@property (nonatomic, weak)IBOutlet UITextField *groupMaxPeople;
@property (nonatomic, weak)IBOutlet UITextView *descriptionBox;
@property (nonatomic, weak)IBOutlet UIButton *createBtn;
@property (nonatomic, weak)IBOutlet UIImageView *groupImage;

@end
