//
//  MessagesVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/16/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessagesVC : UIViewController
{
    UIView *circleView;
    UIView *circleViewReq;
    UILabel *circleViewReqLabel;
    UILabel *notificationLabel;
    UIButton *notifitionIcon;
    int controlSegmented;

    
}
@property (nonatomic, weak)IBOutlet UITableView *chatTable;
@property (nonatomic, weak)IBOutlet UISegmentedControl *segmented;
@end
