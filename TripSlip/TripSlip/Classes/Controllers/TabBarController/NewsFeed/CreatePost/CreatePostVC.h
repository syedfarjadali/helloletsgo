//
//  CreatePostVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/20/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>
@import AVFoundation;
@import AVKit;
@interface CreatePostVC : UIViewController
{
    int tag;
    NSData *imageData;
    NSData *videoData;
}
@property (nonatomic,weak)IBOutlet UIImageView *postImage;
@property (nonatomic, weak)IBOutlet UIView *toolBarView;
@property (nonatomic, weak)IBOutlet UITextView *textView;
@property (strong, nonatomic) NSURL *videoURL;

@end
