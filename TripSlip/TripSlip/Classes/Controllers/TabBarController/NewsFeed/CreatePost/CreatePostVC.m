//
//  CreatePostVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/20/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "CreatePostVC.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "PostCreatedVC.h"

@interface CreatePostVC () <UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextViewDelegate>
@property (strong, nonatomic) FFLoadingView *loadingView;

@end

@implementation CreatePostVC

- (void)viewDidLoad {
    //self.textView.inputView = self.toolBarView;
    //[self.textView sizeToFit];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tap];
    [self leftButton];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)leftButton{
    UIButton *leftIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [leftIcon setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftIcon addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftIcon];
}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleTap:(UITapGestureRecognizer *)recognizer
{
    // your code goes here...
    [self.view endEditing:YES];// this will do the trick
}

- (IBAction)postDone:(id)sender{
    PostCreatedVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"PostCreatedVC"];
    [self.navigationController pushViewController:obj animated:YES];
}

- (IBAction)videoAction:(id)sender{
    [self openVideo];
}
- (IBAction)cameraAction:(id)sender{
    [self openCamera];
}

- (IBAction)photoLibraryAction:(id)sender{
    [self openPhotoLibrary];
}

- (IBAction)publishPost:(id)sender{
    [self.view endEditing:YES];// this will do the trick
    if (tag == 0) {
        //video
        [self postVideoStatus];
    }
    else{
        //image
        [self postImageStatus];
    }
}
- (void)postVideoStatus{
    //video_status
    [self addLoader];
    NSDictionary *params = @{@"user_id":[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],
                             @"status":self.textView.text,
                             @"type":@"videoStatus"
                             };
    
    UploadFile *file    = [[UploadFile alloc] init];
    file.fileData       = videoData;
    file.name           = @"video_status";
    file.fileName       = @"sample.mov";
    file.type           = @"video/quicktime";
    
    [WEB_MANAGER postPathMultipart:KWebServiceCreatePost
                        parameters:params
                       showLoading:nil
                             files:@[file]
                           success:^(id responseObject) {
                               NSLog(@"response:%@", responseObject);
                               [self removeLoader];
                               if ([[responseObject objectForKey:RESPONSE_STATUS]boolValue] == 1){
                                   PostCreatedVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"PostCreatedVC"];
                                   [self.navigationController pushViewController:obj animated:YES];
                               }
                               
                           }
                           failure:^(NSError *error) {
                               [self removeLoader];
                           }];
}
- (void)postImageStatus{
    [self addLoader];
    NSDictionary *params = @{@"user_id":[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],
                             @"status":self.textView.text,
                             @"type":@"imageStatus"
                             };
    
    UploadFile *file    = [[UploadFile alloc] init];
    file.fileData       = imageData;
    file.name           = @"image_status";
    file.fileName       = @"image_status.jpg";
    file.type           = @"image/jpg";
    
    
    [WEB_MANAGER postPathMultipart:KWebServiceCreatePost
                        parameters:params
                       showLoading:nil
                             files:@[file]
                           success:^(id responseObject) {
                               NSLog(@"response:%@", responseObject);
                               [self removeLoader];
                               if ([[responseObject objectForKey:RESPONSE_STATUS]boolValue] == 1){
                                   PostCreatedVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"PostCreatedVC"];
                                   [self.navigationController pushViewController:obj animated:YES];
                               }
                             
                           }
                           failure:^(NSError *error) {
                               [self removeLoader];
                           }];
}
#pragma mark Custom Methods
- (void)addLoader{
    self.loadingView = [[FFLoadingView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-25, [UIScreen mainScreen].bounds.size.height/2-25, 50, 50)];
    self.loadingView.lineWidth = 2;
    self.loadingView.strokeColor = [UIColor colorWithRed:87.0/255.0 green:116.0/255.0 blue:136.0/255.0 alpha:1.0];
    self.loadingView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.loadingView];
    [self.loadingView startLoading];
    [self.view setUserInteractionEnabled:NO];
}

- (void)removeLoader{
    [self.loadingView endLoading];
    [self performSelector:@selector(remove) withObject:nil afterDelay:1.0];
}

- (void)remove{
    [self.view setUserInteractionEnabled:YES];
    [self.loadingView removeFromSuperview];
}
- (void)openVideo{
    tag = 0;
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    [picker setVideoMaximumDuration:30];
    [self presentViewController:picker animated:YES completion:NULL];
}

-(void)openPhotoLibrary
{
    tag = 1;
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
    
}
-(void)openCamera{
    tag = 1;
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark - UIImagePickerDelegates
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    if (tag == 0) {
        self.videoURL = info[UIImagePickerControllerMediaURL];
        videoData = [NSData dataWithContentsOfURL:self.videoURL];
        NSLog(@"%@",[NSByteCountFormatter stringFromByteCount:videoData.length countStyle:NSByteCountFormatterCountStyleFile]);
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:self.self.videoURL options:nil];
        AVAssetImageGenerator *generateImg = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        NSError *error = NULL;
        CMTime time = CMTimeMake(1, 1);
        CGImageRef refImg = [generateImg copyCGImageAtTime:time actualTime:NULL error:&error];
        NSLog(@"error==%@, Refimage==%@", error, refImg);
        UIImage *frameImage= [[UIImage alloc] initWithCGImage:refImg];
        self.postImage.image = frameImage;
        [picker dismissViewControllerAnimated:NO completion:nil];
        
    }
    else{
        UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
        self.postImage.image = chosenImage;
        imageData = UIImageJPEGRepresentation(chosenImage, 0.5);
        [picker dismissViewControllerAnimated:NO completion:nil];
    }

}

#pragma mark - Protocol conformance
#pragma mark - UITextFieldDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView == self.textView) {
        
        NSString *txtViewTitle = NSLocalizedString(@"Tell me about it...", @"Tell me about it...");
        if ([textView.text isEqualToString:txtViewTitle]) {
            textView.text = @"";
        }
        [textView becomeFirstResponder];
    }
    
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    if (textView == self.textView) {
        
        NSString *txtViewTitle = NSLocalizedString(@"Tell me about it...", @"Tell me about it...");
        if ([textView.text isEqualToString:@""]) {
            textView.text = txtViewTitle;
        }
        [textView resignFirstResponder];
    }
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    BOOL status = textView.text.length + (text.length - range.length) <= 90;
    if (status)
    {
        return status;

    }
    else
    {
        return status;

    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
