//
//  CommentsVC.m
//  TripSlip
//
//  Created by bsqptech3 on 11/16/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "CommentsVC.h"
#import "CommentsCell.h"
#import "BABFrameObservingInputAccessoryView.h"
#import "RDRStickyKeyboardView.h"
#import "RDRTextView.h"

@interface CommentsVC () <UITextViewDelegate,UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) RDRStickyKeyboardView *contentWrapper;
@property (nonatomic, strong) UITableView *tableView;
@property (strong, nonatomic) FFLoadingView *loadingView;
@end

@implementation CommentsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self _setupSubviews];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:tap];
    self.commentsArr = [[NSMutableArray alloc]init];
    [self getAllComments];
    // Do any additional setup after loading the view.
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}
#pragma mark - Private
#pragma mark - Custom Accessors
- (void)addLoader{
    self.loadingView = [[FFLoadingView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-25, [UIScreen mainScreen].bounds.size.height/2-25, 50, 50)];
    self.loadingView.lineWidth = 2;
    self.loadingView.strokeColor = [UIColor colorWithRed:87.0/255.0 green:116.0/255.0 blue:136.0/255.0 alpha:1.0];
    self.loadingView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.loadingView];
    [self.loadingView startLoading];
    [self.view setUserInteractionEnabled:NO];
}

- (void)removeLoader{
    [self.loadingView endLoading];
    [self performSelector:@selector(remove) withObject:nil afterDelay:1.0];
}

- (void)remove{
    [self.view setUserInteractionEnabled:YES];
    [self.loadingView removeFromSuperview];
}
- (void)_setupSubviews
{
    // Setup tableview
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero
                                                  style:UITableViewStylePlain];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth
    |UIViewAutoresizingFlexibleHeight;
    //[self.tableView registerClass:[CommentsCell class]
          // forCellReuseIdentifier:@"CommentsCell"];
    //self.tableView.backgroundColor = [UIColor cyanColor];
    // Setup wrapper
    self.contentWrapper = [[RDRStickyKeyboardView alloc] initWithScrollView:self.tableView];
    self.contentWrapper.frame = self.view.bounds;
    self.contentWrapper.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.contentWrapper.placeholder = @"Message";
    [self.contentWrapper.inputView.rightButton addTarget:self action:@selector(didTapSend:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.contentWrapper];
}
- (void)didTapSend:(id)sender
{
    if (self.contentWrapper.inputView.textView.text.length == 0||[self.contentWrapper.inputView.textView.text isEqualToString:@""]) {
        
    }
    else{
        NSDictionary *params = @{@"user_id":[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID],
                                 @"event_id":self.eventId,
                                 @"comment_text":self.contentWrapper.inputView.textView.text
                                 };
        
        [WEB_MANAGER postPath:KWebServiceUserComment
                   parameters:params
                  showLoading:nil
                      success:^(id responseObject) {
                          NSLog(@":%@", responseObject);
                          if ([[responseObject objectForKey:RESPONSE_STATUS]boolValue] == 1){
                              self.contentWrapper.inputView.textView.text = @"";
                              [self.contentWrapper hideKeyboard];
                              [self getAllComments];
                          }
                      }
                      failure:^(NSError *error) {
                      }];
        
        
    }
}
- (void)getAllComments{
    [self addLoader];
    NSDictionary *params = @{@"event_id":self.eventId
                             
                             };
    
    [WEB_MANAGER getPath:KWebServiceGetAllComments
               parameters:params
              showLoading:nil
                  success:^(id responseObject) {
                      NSLog(@":%@", responseObject);
                      [self removeLoader];
                      [self.commentsArr removeAllObjects];
                      if ([[responseObject objectForKey:RESPONSE_STATUS]boolValue] == 1){
                          NSDictionary *data = [responseObject valueForKey:@"data"];
                          for (NSDictionary *userDictionary in data)
                          {
                              [self.commentsArr addObject:userDictionary];
                              
                              
                          }
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [self.tableView reloadData];
                          });
                      }
                  }
                  failure:^(NSError *error) {
                      [self removeLoader];
                  }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*- (void)setKeyboard{
    self.commentsTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    
    BABFrameObservingInputAccessoryView *inputView = [[BABFrameObservingInputAccessoryView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    inputView.userInteractionEnabled = NO;
    
    self.textField.inputAccessoryView = inputView;
    
    __weak typeof(self)weakSelf = self;
    
    inputView.inputAcessoryViewFrameChangedBlock = ^(CGRect frame){
        
        CGFloat value = CGRectGetHeight(weakSelf.view.frame) - CGRectGetMinY(weakSelf.textField.inputAccessoryView.superview.frame) - CGRectGetHeight(weakSelf.textField.inputAccessoryView.frame);
        
        weakSelf.toolbarContainerVerticalSpacingConstraint.constant = MAX(0, value);
        
        [weakSelf.view layoutIfNeeded];
        
    };
    
}
 */
- (void)handleTap:(UITapGestureRecognizer *)recognizer
{
    // your code goes here...
    [self.view endEditing:YES];// this will do the trick
    [self.messageTxt resignFirstResponder];
}
- (IBAction)dismiss:(id)sender {
    //[self.textField resignFirstResponder];
}

- (IBAction)enableTextView:(id)sender{
    
}
- (IBAction)sendButtonAction:(id)sender {
   
}
#pragma mark - Protocol conformance
#pragma mark - UITableViewDelegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.commentsArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CommentsCell";
    CommentsCell *cell = (CommentsCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CommentsCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.messageTxtView.lineBreakMode = NSLineBreakByTruncatingTail;
    cell.messageTxtView.numberOfLines=0;
    NSString *imageURL = [[self.commentsArr objectAtIndex:indexPath.row]valueForKey:@"user_image"];
    NSURL *userImageUrl = [NSURL URLWithString:imageURL];
    [cell.profileImage sd_setImageWithURL:userImageUrl placeholderImage:[UIImage imageNamed:@"user_placeholder"]];
    NSString *userName =[[self.commentsArr objectAtIndex:indexPath.row]valueForKey:@"user_full_name"];
    cell.nameLabel.text = userName;
    NSString *messageText =[[self.commentsArr objectAtIndex:indexPath.row]valueForKey:@"comment_text"];
    cell.messageTxtView.text = messageText;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
#pragma mark - Protocol conformance
#pragma mark - UITextFieldDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    /*NSString *txtViewTitle = NSLocalizedString(@"Type your message here...", @"Type your message here...");
    if ([textView.text isEqualToString:txtViewTitle]) {
        textView.text = @"";
    }
    [textView becomeFirstResponder];
     */
    //[textView resignFirstResponder];
    /*if (textView == self.messageTxt) {
        [self.messageSendTxt becomeFirstResponder];
    }
     */
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    
    NSLog(@"%@",textView.text);
    /*NSString *txtViewTitle = NSLocalizedString(@"Type your message here...", @"Type your message here...");
    if ([textView.text isEqualToString:@""]) {
        textView.text = txtViewTitle;
    }
    [textView resignFirstResponder];
     
    if (textView == self.messageTxt) {
        [textView resignFirstResponder];
    }
    if (textView == self.messageSendTxt) {
        [textView resignFirstResponder];
    }
     */
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
