//
//  CommentsVC.h
//  TripSlip
//
//  Created by bsqptech3 on 11/16/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentsVC : UIViewController
{
    NSArray *array;
}
@property (nonatomic,retain)NSString *eventId;
@property (nonatomic,weak)IBOutlet UITableView *commentsTableView;
@property (nonatomic, retain) IBOutlet UITextView * messageTxt;
@property (nonatomic, retain) IBOutlet UITextView * messageSendTxt;
@property (nonatomic, strong) NSMutableArray * commentsArr;
@property (nonatomic, weak)IBOutlet UIView *toolBarView;
@property (nonatomic, weak)IBOutlet UIButton *sendBtn;

- (IBAction)sendButtonAction:(id)sender;
- (IBAction)enableTextView:(id)sender;
@end
