//
//  NewsFeedVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/16/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//
#import "NewsFeedVC.h"
#import "NewsFeedCell.h"
#import "CreatePostVC.h"
#import "CreateEventVC.h"
#import "SearchVC.h"
#import "AdsCell.h"
#import "VideoCell.h"
#import "CommentsVC.h"
#import "BABViewController.h"
@import GoogleMobileAds;

@interface NewsFeedVC () <UITableViewDelegate,UITableViewDataSource,GADBannerViewDelegate>
@property (strong, nonatomic) FFLoadingView *loadingView;

@end

@implementation NewsFeedVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.newsFeedArray = [[NSMutableArray alloc]init];
    eventData = [[EventFeedModel alloc]init];
    eventImageData = [[EventFeedImageModel alloc]init];
    eventVideoData = [[EventFeedVideoModel alloc]init];
    [self refreshControl];
    [self setNAvigationBar];
    [self setNavigationBarButtons];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
     noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, self.newsFeedTable.bounds.size.width, self.newsFeedTable.bounds.size.height)];
    noDataLabel.text             = @"You dont have any event";
    noDataLabel.userInteractionEnabled = YES;
    noDataLabel.textColor        = [UIColor blackColor];
    noDataLabel.textAlignment    = NSTextAlignmentCenter;
    [self getAllEvents];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Custom Accessors
- (void)addLoader{
    self.loadingView = [[FFLoadingView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-25, [UIScreen mainScreen].bounds.size.height/2-25, 50, 50)];
    self.loadingView.lineWidth = 2;
    self.loadingView.strokeColor = [UIColor colorWithRed:87.0/255.0 green:116.0/255.0 blue:136.0/255.0 alpha:1.0];
    self.loadingView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.loadingView];
    [self.loadingView startLoading];
    [self.view setUserInteractionEnabled:NO];
}

- (void)removeLoader{
    [self.loadingView endLoading];
    [self performSelector:@selector(remove) withObject:nil afterDelay:1.0];
}

- (void)remove{
    [self.view setUserInteractionEnabled:YES];
    [self.loadingView removeFromSuperview];
}
- (void)refreshControl{
    refreshControl = [[UIRefreshControl alloc]init];
    [self.newsFeedTable addSubview:refreshControl];
    UIColor *bgRefreshColor = [UIColor colorWithRed:87.0/255.0 green:116.0/255.0 blue:136.0/255.0 alpha:1.0];
    [refreshControl setBackgroundColor:bgRefreshColor];
    [refreshControl setTintColor:[UIColor whiteColor]];
    UIFont * font = [UIFont systemFontOfSize:12.0];
    NSDictionary *attributes = @{NSFontAttributeName : font, NSForegroundColorAttributeName : [UIColor whiteColor]};
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Loading..." attributes:attributes];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
}
- (void)refreshTable {
    //TODO: refresh your data
    [self getAllEvents];
}
- (void)setNAvigationBar{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigation_bar"]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.layer.shadowColor = [[UIColor whiteColor] CGColor]; self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f); self.navigationController.navigationBar.layer.shadowRadius = 4.0f;
    self.navigationController.navigationBar.layer.shadowOpacity = 0.7f;
    self.navigationController.navigationBar.layer.masksToBounds=NO;

}
- (void)setNavigationBarButtons{
    //
    circleView = [[UIView alloc] initWithFrame:CGRectMake(15,9,18,18)];
    circleView.layer.cornerRadius = 9;
    circleView.backgroundColor = [UIColor colorWithRed:4.0/255.0 green:21.0/255.0 blue:85.0/255.0 alpha:1.0];
    
    circleViewReq = [[UIView alloc] initWithFrame:CGRectMake(15,9,18,18)];
    circleViewReq.layer.cornerRadius = 9;
    circleViewReq.backgroundColor = [UIColor colorWithRed:4.0/255.0 green:21.0/255.0 blue:85.0/255.0 alpha:1.0];
    //
    notificationLabel = [[UILabel alloc] init];
    [notificationLabel setFrame:CGRectMake(0, 0, 18, 18)];
    [notificationLabel setTextAlignment:NSTextAlignmentCenter];
    notificationLabel.backgroundColor=[UIColor clearColor];
    notificationLabel.textColor=[UIColor whiteColor];
    [notificationLabel setFont:[UIFont fontWithName:@"Helvetica" size:8]];
    notificationLabel.userInteractionEnabled=NO;
    notificationLabel.text= @"0";
    //
    circleViewReqLabel = [[UILabel alloc] init];
    [circleViewReqLabel setFrame:CGRectMake(0, 0, 18, 18)];
    [circleViewReqLabel setTextAlignment:NSTextAlignmentCenter];
    circleViewReqLabel.backgroundColor=[UIColor clearColor];
    circleViewReqLabel.textColor=[UIColor whiteColor];
    [circleViewReqLabel setFont:[UIFont fontWithName:@"Helvetica" size:8]];
    circleViewReqLabel.userInteractionEnabled=NO;
    circleViewReqLabel.text= @"0";
    
    //
    UIButton *leftIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [leftIcon setImage:[UIImage imageNamed:@"friend_request"] forState:UIControlStateNormal];
    [leftIcon addTarget:self action:@selector(friendReq) forControlEvents:UIControlEventTouchUpInside];
    //
    notifitionIcon =  [UIButton buttonWithType:UIButtonTypeCustom];
    [notifitionIcon setImage:[UIImage imageNamed:@"notification"] forState:UIControlStateNormal];
    [notifitionIcon addTarget:self action:@selector(notifications) forControlEvents:UIControlEventTouchUpInside];
    [notifitionIcon setFrame:CGRectMake(38, 0, 24, 24)];
    //
    UIButton *homeBtn =  [UIButton buttonWithType:UIButtonTypeCustom];
    [homeBtn setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
    [homeBtn addTarget:self action:@selector(search) forControlEvents:UIControlEventTouchUpInside];
    [homeBtn setFrame:CGRectMake(44, 0, 24, 24)];
    //
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, 24)];
    [rightBarButtonItems addSubview:notifitionIcon];
    //[rightBarButtonItems addSubview:homeBtn];
    [notifitionIcon addSubview:circleView];
    [circleView addSubview:notificationLabel];
    [leftIcon addSubview:circleViewReq];
    [circleViewReq addSubview:circleViewReqLabel];
    
    //
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftIcon];
}

#pragma mark- Service Calls
- (void)getAllEvents{
    [self addLoader];
    //event service call
    NSDictionary *params = @{@"user_id":[[NSUserDefaults standardUserDefaults]objectForKey:USER_ID]
                             };
    [EventFeedModel getAllWithParams:params
                     success:^(id responseObject) {
                         NSLog(@"%@",responseObject);
                         [self removeLoader];
                         [self.newsFeedArray removeAllObjects];
                         [self.newsFeedArray addObjectsFromArray:responseObject];
                         dispatch_async(dispatch_get_main_queue(), ^{
                             [self.newsFeedTable reloadData];
                             if(refreshControl.isRefreshing){
                                 [refreshControl endRefreshing];
                             }
                         });
                     }
                     failure:^(NSError *error) {
                         [self removeLoader];
                         [refreshControl endRefreshing];
                     }];

}
- (void)like:(UIButton*)sender{
    //NSIndexPath *myIP = [NSIndexPath indexPathForRow:0 inSection:0];
    //Type cast it to CustomCell
    //ProfileCell *cell = (ProfileCell*)[self.profileTable cellForRowAtIndexPath:myIP];
}

- (void)comment:(UIButton*)sender{
    //
    NSString *idEvent;
    if ([[self.newsFeedArray objectAtIndex:sender.tag] isKindOfClass:[EventFeedModel class]]) {
        eventData = [self.newsFeedArray objectAtIndex:sender.tag];
        idEvent = eventData.eventId;
    }
    else if ([[self.newsFeedArray objectAtIndex:sender.tag] isKindOfClass:[EventFeedImageModel class]]){
        eventImageData = [self.newsFeedArray objectAtIndex:sender.tag];
        idEvent = eventImageData.imageStatusId;
    }
    else{
        eventVideoData = [self.newsFeedArray objectAtIndex:sender.tag];
        idEvent = eventVideoData.videoId;
    }
    CommentsVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentsVC"];
    obj.eventId = idEvent;
    [self.navigationController pushViewController:obj animated:YES];
}

- (void)share:(UIButton*)sender{
    //
    if ([[self.newsFeedArray objectAtIndex:sender.tag] isKindOfClass:[EventFeedModel class]]) {
        //
        eventData = [self.newsFeedArray objectAtIndex:sender.tag];
        NSString *textToShare = [NSString stringWithFormat:@"!%@",eventData.eventName];
        NSURL *myWebsite = [NSURL URLWithString:eventData.eventImage];
        NSArray * objectsToShare = @[[NSString stringWithFormat:@"%@", textToShare], myWebsite];
        controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
        NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                       UIActivityTypePostToWeibo,
                                       UIActivityTypePrint,
                                       UIActivityTypeMail,
                                       UIActivityTypeMessage,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeSaveToCameraRoll,
                                       UIActivityTypeAddToReadingList,
                                       UIActivityTypeAirDrop,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypePostToFlickr,
                                       UIActivityTypePostToVimeo];
        
        
        controller.excludedActivityTypes = excludeActivities;
        [self presentViewController:controller animated:YES completion:nil];
    }
    else if ([[self.newsFeedArray objectAtIndex:sender.tag] isKindOfClass:[EventFeedImageModel class]]){
        //
        eventImageData = [self.newsFeedArray objectAtIndex:sender.tag];
        NSString *textToShare = [NSString stringWithFormat:@"!%@",eventImageData.imageStatusDescription];
        NSURL *myWebsite = [NSURL URLWithString:eventImageData.imageStatusImage];
        NSArray * objectsToShare = @[[NSString stringWithFormat:@"%@", textToShare], myWebsite];
        controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
        NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                       UIActivityTypePostToWeibo,
                                       UIActivityTypePrint,
                                       UIActivityTypeMail,
                                       UIActivityTypeMessage,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeSaveToCameraRoll,
                                       UIActivityTypeAddToReadingList,
                                       UIActivityTypeAirDrop,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypePostToFlickr,
                                       UIActivityTypePostToVimeo];
        
        
        controller.excludedActivityTypes = excludeActivities;
        [self presentViewController:controller animated:YES completion:nil];
    }
    else{
        //
        /*eventVideoData = [self.newsFeedArray objectAtIndex:sender.tag];
        NSString *textToShare = [NSString stringWithFormat:@"!%@",eventVideoData.videoDescription];
        NSURL *myWebsite = [NSURL URLWithString:eventVideoData.videoImage];
        NSArray * objectsToShare = @[[NSString stringWithFormat:@"%@", textToShare], myWebsite];
        controller = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
        NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                       UIActivityTypePostToWeibo,
                                       UIActivityTypePrint,
                                       UIActivityTypeMail,
                                       UIActivityTypeMessage,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypeSaveToCameraRoll,
                                       UIActivityTypeAddToReadingList,
                                       UIActivityTypeAirDrop,
                                       UIActivityTypeAssignToContact,
                                       UIActivityTypePostToFlickr,
                                       UIActivityTypePostToVimeo];
        
        
        controller.excludedActivityTypes = excludeActivities;
        [self presentViewController:controller animated:YES completion:nil];
         */
    }
}
- (void)friendReq{
    
    UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FriendReqNAv"];
    [self presentViewController:vc
                       animated:YES
                     completion:^{
                         
                     }];
}
- (void)notifications{
    
    UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationNav"];
    [self presentViewController:vc
                       animated:YES
                     completion:^{
                         
                     }];
}
- (void)search{
    SearchVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchVC"];
    [self.navigationController pushViewController:obj animated:YES];
}

- (IBAction)createPost:(id)sender{
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"HelloLetsGo"
                                                                  message:nil
                                                           preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* createPost = [UIAlertAction actionWithTitle:@"Create Post"
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action)
                                 {
                                     CreatePostVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"CreatePostVC"];
                                     [self.navigationController pushViewController:obj animated:YES];
                                 }];
    
    UIAlertAction* createEvent = [UIAlertAction actionWithTitle:@"Create Event"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      CreateEventVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"CreateEventVC"];
                                      [self.navigationController pushViewController:obj animated:YES];
                                  }];
    
    
    UIAlertAction* uploadImage = [UIAlertAction actionWithTitle:@"Upload Image"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      CreatePostVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"CreatePostVC"];
                                      [self.navigationController pushViewController:obj animated:YES];
                                  }];
    
    
    UIAlertAction* uploadVideo = [UIAlertAction actionWithTitle:@"Upload Video"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * action)
                                  {
                                      CreatePostVC *obj = [self.storyboard instantiateViewControllerWithIdentifier:@"CreatePostVC"];
                                      [self.navigationController pushViewController:obj animated:YES];
                                  }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:^(UIAlertAction * action)
                             {
                                 
                             }];
    
    
    [alert addAction:createPost];
    [alert addAction:createEvent];
    [alert addAction:uploadImage];
    [alert addAction:uploadVideo];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];

}
#pragma mark - Protocol conformance
#pragma mark - UITableViewDelegates

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if (self.newsFeedArray.count == 0) {
        self.newsFeedTable.backgroundView = noDataLabel;
        self.newsFeedTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    else{
        self.newsFeedTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        numOfSections                = 1;
        self.newsFeedTable.backgroundView = nil;
    }
    return numOfSections;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.newsFeedArray.count+1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int row = (int)[indexPath row];
    if (row < self.newsFeedArray.count) {
        return 340.0f;
    }
    return 50.0f;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"NewsFeedCell";
    static NSString *MyIdentifier = @"AdCell";
    if (indexPath.row < self.newsFeedArray.count) {
        if ([[self.newsFeedArray objectAtIndex:indexPath.row] isKindOfClass:[EventFeedModel class]]) {
            //eventCell
            eventData = [self.newsFeedArray objectAtIndex:indexPath.row];
            NewsFeedCell *cell = (NewsFeedCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NewsFeedCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            cell.backgroundColor = [UIColor clearColor];
            cell.userFullName.text = eventData.eventName;
            cell.EventDescription.text = eventData.eventDescription;
            NSURL *userImageUrl = [NSURL URLWithString:eventData.userImage];
            [cell.userImage sd_setImageWithURL:userImageUrl placeholderImage:[UIImage imageNamed:@"user_placeholder"]];
            NSURL *url = [NSURL URLWithString:eventData.eventImage];
            [cell.EventImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
            [cell.EventImage setupImageViewerWithImageURL:url];
            [cell.likeBtn addTarget:self action:@selector(like:) forControlEvents:UIControlEventTouchUpInside];
            cell.likeBtn.tag = indexPath.row;
            [cell.commentBtn addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
            cell.commentBtn.tag = indexPath.row;
            [cell.shareBtn addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];
            cell.shareBtn.tag = indexPath.row;
            return cell;
        }
        else if ([[self.newsFeedArray objectAtIndex:indexPath.row] isKindOfClass:[EventFeedImageModel class]]){
            //imageCell
            eventImageData = [self.newsFeedArray objectAtIndex:indexPath.row];
            NewsFeedCell *cell = (NewsFeedCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NewsFeedCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            cell.backgroundColor = [UIColor clearColor];
            cell.userFullName.text = eventImageData.userFullName;
            cell.EventDescription.text = eventImageData.imageStatusDescription;
            NSURL *userImageUrl = [NSURL URLWithString:eventImageData.userImage];
            [cell.userImage sd_setImageWithURL:userImageUrl placeholderImage:[UIImage imageNamed:@"user_placeholder"]];
            NSURL *imageStatusUrl = [NSURL URLWithString:eventImageData.imageStatusImage];
            [cell.EventImage sd_setImageWithURL:imageStatusUrl placeholderImage:[UIImage imageNamed:@""]];
            [cell.EventImage setupImageViewerWithImageURL:imageStatusUrl];
            [cell.likeBtn addTarget:self action:@selector(like:) forControlEvents:UIControlEventTouchUpInside];
            cell.likeBtn.tag = indexPath.row;
            [cell.commentBtn addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
            cell.commentBtn.tag = indexPath.row;
            [cell.shareBtn addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];
            cell.shareBtn.tag = indexPath.row;
            return cell;
        }
        else{
            //videoCell
            eventVideoData = [self.newsFeedArray objectAtIndex:indexPath.row];
            VideoCell *cell = (VideoCell *)[tableView dequeueReusableCellWithIdentifier:@"VideoCell"];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VideoCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            cell.backgroundColor = [UIColor clearColor];
            cell.userFullName.text = eventVideoData.userFullName;
            cell.videoDescription.text = eventVideoData.videoDescription;
            NSURL *userImageUrl = [NSURL URLWithString:eventVideoData.userImage];
            [cell.userImage sd_setImageWithURL:userImageUrl placeholderImage:[UIImage imageNamed:@"user_placeholder"]];
            NSURL *url = [NSURL URLWithString:eventVideoData.videoImage];
            [cell.videoImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
            [cell.likeBtn addTarget:self action:@selector(like:) forControlEvents:UIControlEventTouchUpInside];
            cell.likeBtn.tag = indexPath.row;
            [cell.commentBtn addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
            cell.commentBtn.tag = indexPath.row;
            [cell.shareBtn addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];
            cell.shareBtn.tag = indexPath.row;
            [cell.playBtn addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
            cell.playBtn.tag = indexPath.row;
            return cell;
        }
    }
    else{
        AdsCell *cell = (AdsCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AdsCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        cell.backgroundColor = [UIColor clearColor];
        cell.bannerView.adUnitID = @"ca-app-pub-3940256099942544/2934735716";
        cell.bannerView.rootViewController = self;
        [cell.bannerView loadRequest:[GADRequest request]];
        cell.bannerView.delegate = self;
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int row = (int)[indexPath row];
    NSLog(@"%@",[self.newsFeedArray objectAtIndex:row]);
}
- (void)playVideo:(UIButton*)sender{
    eventVideoData = [self.newsFeedArray objectAtIndex:sender.tag];
    NSURL *videoURL = [NSURL URLWithString:eventVideoData.videoName];
    AVPlayer *player = [AVPlayer playerWithURL:videoURL];
    playerViewController = [AVPlayerViewController new];
    playerViewController.player = player;
    [self presentViewController:playerViewController animated:YES completion:nil];
}
/// Tells the delegate an ad request loaded an ad.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    NSLog(@"adViewDidReceiveAd");
    adView.alpha = 0;
    [UIView animateWithDuration:1.0 animations:^{
        adView.alpha = 1;
    }];
}

/// Tells the delegate an ad request failed.
- (void)adView:(GADBannerView *)adView
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"adView:didFailToReceiveAdWithError: %@", [error localizedDescription]);
}

/// Tells the delegate that a full screen view will be presented in response
/// to the user clicking on an ad.
- (void)adViewWillPresentScreen:(GADBannerView *)adView {
    NSLog(@"adViewWillPresentScreen");
}

/// Tells the delegate that the full screen view will be dismissed.
- (void)adViewWillDismissScreen:(GADBannerView *)adView {
    NSLog(@"adViewWillDismissScreen");
}

/// Tells the delegate that the full screen view has been dismissed.
- (void)adViewDidDismissScreen:(GADBannerView *)adView {
    NSLog(@"adViewDidDismissScreen");
}

/// Tells the delegate that a user click will open another app (such as
/// the App Store), backgrounding the current app.
- (void)adViewWillLeaveApplication:(GADBannerView *)adView {
    NSLog(@"adViewWillLeaveApplication");
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
