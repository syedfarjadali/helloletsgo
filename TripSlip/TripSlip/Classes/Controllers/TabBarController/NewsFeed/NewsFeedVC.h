//
//  NewsFeedVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/16/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventFeedModel.h"
#import "EventFeedVideoModel.h"
#import "EventFeedImageModel.h"
@import AVFoundation;
@import AVKit;
@interface NewsFeedVC : UIViewController
{
    UIView *circleView;
    UIView *circleViewReq;
    UILabel *circleViewReqLabel;
    UILabel *notificationLabel;
    UIButton *notifitionIcon;
    UILabel *noDataLabel;
    UIRefreshControl *refreshControl;
    EventFeedModel *eventData;
    EventFeedImageModel *eventImageData;
    EventFeedVideoModel *eventVideoData;
    UIActivityViewController *controller;
    AVPlayerViewController *playerViewController;


}
- (IBAction)createPost:(id)sender;
@property (nonatomic, weak)IBOutlet UITableView *newsFeedTable;
@property (nonatomic, strong)NSMutableArray *newsFeedArray;
@end
