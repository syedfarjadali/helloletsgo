//
//  VideoCell.h
//  TripSlip
//
//  Created by bsqptech3 on 11/15/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoCell : UITableViewCell
{
    
}
@property (nonatomic, weak)IBOutlet UIButton *playBtn;
@property (nonatomic, weak)IBOutlet UILabel *userFullName;
@property (nonatomic, weak)IBOutlet UIImageView *userImage;
@property (nonatomic, weak)IBOutlet UIImageView *videoImage;
@property (nonatomic, weak)IBOutlet UILabel *videoDescription;
@property (nonatomic, weak)IBOutlet UILabel *videotLikes;
@property (nonatomic, weak)IBOutlet UILabel *videoComments;
@property (nonatomic, weak)IBOutlet UIButton *likeBtn;
@property (nonatomic, weak)IBOutlet UIButton *commentBtn;
@property (nonatomic, weak)IBOutlet UIButton *shareBtn;
@end
