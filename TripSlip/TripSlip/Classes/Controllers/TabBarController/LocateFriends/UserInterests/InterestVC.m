//
//  InterestVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/24/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "InterestVC.h"
#import "TagCell.h"
#import "FlowLayout.h"

@interface InterestVC ()
@property (strong, nonatomic) NSArray *items;
@property (strong, nonatomic) FlowLayout *layout;
@end

@implementation InterestVC

static NSString * const reuseIdentifier = @"TagCell";

- (void)viewDidLoad
{
    [super viewDidLoad];
    array = @[@"Birthday Events", @"Dinner Parties", @"Road Trips", @"Opening Ceremonies",@"Business Conference",@"City Tour",@"Food Festivals",@"Bachelor party",@"Band Gig",@"Baseball Game",@"Bicycling",@"Birthday Parties",@"Boating",@"Business/Networking",@"Camping",@"Concert"];
    // Do any additional setup after loading the view, typically from a nib.
    self.layout = [[FlowLayout alloc] init];
    self.layout.alignment = FlowAlignmentLeft;
    self.layout.estimatedItemSize = CGSizeMake(80, 30);
    self.layout.sectionInset = UIEdgeInsetsMake(20, 30, 20, 30);
    self.layout.headerReferenceSize = CGSizeMake(300, 30);
    self.layout.footerReferenceSize = CGSizeMake(300, 30);
    self.collectionView.collectionViewLayout = self.layout;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return array.count;
}

/*- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
 {
 if (kind == UICollectionElementKindSectionHeader) {
 return [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"TagHeader" forIndexPath:indexPath];
 } else if (kind == UICollectionElementKindSectionFooter) {
 return [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"TagFooter" forIndexPath:indexPath];
 }
 return nil;
 }
 */
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TagCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    cell.maxWidthConstraint.constant = CGRectGetWidth(collectionView.bounds) - self.layout.sectionInset.left - self.layout.sectionInset.right - cell.layoutMargins.left - cell.layoutMargins.right - 10;
    cell.textLabel.numberOfLines = 1;
    cell.textLabel.text = array[indexPath.row];
    return cell;
}

@end
