//
//  LocateFriendsVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/16/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocateFriendsVC : UIViewController
{
    UIView *circleView;
    UIView *circleViewReq;
    UILabel *circleViewReqLabel;
    UILabel *notificationLabel;
    UIButton *notifitionIcon;
}
@property (nonatomic, weak)IBOutlet UITableView *locateTable;
@end
