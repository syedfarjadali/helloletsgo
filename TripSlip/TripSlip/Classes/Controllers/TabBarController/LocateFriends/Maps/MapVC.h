//
//  MapVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/18/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapVC : UIViewController <GMSMapViewDelegate>
{
    UISearchBar *searchBar;
    UISearchDisplayController *searchDisplayController;
}
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (nonatomic,retain)NSString *latitude;
@property (nonatomic,retain)NSString *longitude;
@property (nonatomic,retain)NSString *className;
@end
