//
//  MapVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/18/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "MapVC.h"

@interface MapVC () <UISearchBarDelegate,UISearchDisplayDelegate,UITableViewDelegate,UITableViewDataSource>

@end

@implementation MapVC

- (void)viewDidLoad {
    [self leftButton];
    //
    UIButton *leftIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [leftIcon setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
    [leftIcon addTarget:self action:@selector(initialiseSearch) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftIcon];
    //Google Maps
    self.mapView.myLocationEnabled = YES;
    self.mapView.mapType = kGMSTypeNormal;
    self.mapView.accessibilityElementsHidden = NO;
    self.mapView.settings.scrollGestures = YES;
    self.mapView.settings.zoomGestures = YES;
    self.mapView.settings.compassButton = YES;
    self.mapView.settings.myLocationButton = YES;
    self.mapView.delegate = self;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    //
    /*GMSMarker *saveMarker = [[GMSMarker alloc] init];
    saveMarker.position = CLLocationCoordinate2DMake([self.latitude floatValue], [self.longitude floatValue]);
    saveMarker.icon = [UIImage imageNamed:@"mapPin.png"];
    saveMarker.title = @"Business Location";
    //saveMarker.snippet = [sharedObj.locationArr objectAtIndex:0];
    saveMarker.map = self.mapView;
    
    self.mapView.camera = [GMSCameraPosition cameraWithLatitude:[self.latitude floatValue] longitude:[self.longitude floatValue] zoom:15.0 bearing:18.0 viewingAngle:65.0];
    [self.mapView animateToViewingAngle:45];
     */
    [self setMapMarker];
}
- (void)viewWillDisappear:(BOOL)animated{
    [self.view endEditing:YES];
    [searchBar resignFirstResponder];
    self.navigationController.navigationBarHidden = NO;
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [searchBar setAlpha:0.0];
}
- (void)viewDidDisappear:(BOOL)animated{
   
}
- (void)setMapMarker{
    if ([self.className isEqualToString:@"EventList"]) {
        GMSMarker *saveMarker = [[GMSMarker alloc] init];
        saveMarker.position = CLLocationCoordinate2DMake([self.latitude floatValue], [self.longitude floatValue]);
        //saveMarker.icon = [UIImage imageNamed:@"mapPin.png"];
        //saveMarker.title = @"Business Location";
        //saveMarker.snippet = [sharedObj.locationArr objectAtIndex:0];
        saveMarker.map = self.mapView;
        
        self.mapView.camera = [GMSCameraPosition cameraWithLatitude:[self.latitude floatValue] longitude:[self.longitude floatValue] zoom:15.0 bearing:18.0 viewingAngle:65.0];
        [self.mapView animateToViewingAngle:45];
    }
}
- (void)leftButton{
    UIButton *leftIcon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [leftIcon setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [leftIcon addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftIcon];
}
- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialiseSearch
{
    if(!searchBar) {
        searchBar = [[UISearchBar alloc] init];
        [searchBar setFrame:CGRectMake(0, 0, self.view.frame.size.width, 55)];
        [searchBar setShowsCancelButton:YES animated:YES];
        [self.view addSubview: searchBar];
        searchBar.delegate = self;
        self.navigationController.navigationBarHidden = YES;
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }else {
        searchBar.alpha = 1.0;
        self.navigationController.navigationBarHidden = YES;
        [[UIApplication sharedApplication] setStatusBarHidden:YES];

    }
}
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    [searchBar resignFirstResponder];
    self.navigationController.navigationBarHidden = NO;
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [searchBar setAlpha:0.0];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
