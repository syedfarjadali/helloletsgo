//
//  ReportFormVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/25/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportFormVC : UIViewController

@property (weak, nonatomic)IBOutlet UITextView *descriptionBox;
@property (weak, nonatomic)IBOutlet UIButton *submitBtn;;
@end
