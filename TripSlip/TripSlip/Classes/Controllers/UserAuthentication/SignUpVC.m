//
//  SignUpVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/9/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "SignUpVC.h"
#import "Singleton.h"
#define kOFFSET_FOR_KEYBOARD 80.0

@interface SignUpVC () <UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate>
@property (strong, nonatomic) FFLoadingView *loadingView;

@end

@implementation SignUpVC

- (void)viewDidLoad {
    [self CurrentLocationIdentifier]; // call this method
    notificationToken = [self notification];
    [self.userNameTxt setValue:[UIColor whiteColor]
                 forKeyPath:@"_placeholderLabel.textColor"];
    [self.fullNameTxt setValue:[UIColor whiteColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
    [self.emailTxt setValue:[UIColor whiteColor]
                 forKeyPath:@"_placeholderLabel.textColor"];
    [self.passwordTxt setValue:[UIColor whiteColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    //numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithKeyboard)],
                           nil];
    [numberToolbar sizeToFit];
    self.emailTxt.inputAccessoryView = numberToolbar;
    self.passwordTxt.inputAccessoryView = numberToolbar;
    self.fullNameTxt.inputAccessoryView = numberToolbar;
    self.userNameTxt.inputAccessoryView = numberToolbar;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController.navigationBar setHidden:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString*)notification{
    
    NSUserDefaults * token  = [NSUserDefaults standardUserDefaults];
    NSString *tokenKey = [NSString stringWithFormat:@"%@",[token objectForKey:@"deviceToken"]];
    if (tokenKey == nil || tokenKey == (id)[NSNull null]) {
        
        return @"111111111111111111111111";
    }
    else{
        
        return tokenKey;
    }
    
    
}
- (void)doneWithKeyboard{
    [self.view endEditing:YES];
}

- (void)pushTab{
    UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectInterestRoot"];
    [self presentViewController:vc
                       animated:YES
                     completion:^{
                     }];
}
- (void)addLoader{
    self.loadingView = [[FFLoadingView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-25, [UIScreen mainScreen].bounds.size.height/2-25, 50, 50)];
    self.loadingView.lineWidth = 2;
    //self.loadingView.strokeColor = [UIColor colorWithRed:21.0/255.0 green:99.0/255.0 blue:153.0/255.0 alpha:1.0];
    self.loadingView.strokeColor = [UIColor whiteColor];
    self.loadingView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.loadingView];
    [self.loadingView startLoading];
    [self.view setUserInteractionEnabled:NO];
}

- (void)removeLoader{
    [self.loadingView finishSuccess:nil];
    [self performSelector:@selector(remove) withObject:nil afterDelay:1.0];
}

- (void)remove{
    [self.view setUserInteractionEnabled:YES];
    [self.loadingView removeFromSuperview];
}
#pragma mark - IBActions
//"user_name, user_email, user_password(only required when user is registring using normal method),
//user_full_name, user_image, user_lat, user_long, user_device_token, user_social_media"

- (IBAction)Signup:(id)sender{
    if ([self validateFields]) {
        if (isImage == YES) {
            [self addLoader];
            NSDictionary *params = @{@"user_email":self.emailTxt.text,
                                     @"user_password":self.passwordTxt.text,
                                     @"user_full_name":self.fullNameTxt.text,
                                     @"user_name":self.userNameTxt.text,
                                     @"user_device_token":notificationToken,
                                     @"user_lat":latitude,
                                     @"user_long":longitude,
                                     @"user_social_media":@"0",
                                     @"user_social_type":@"0"
                                     };
            
            UploadFile *file    = [[UploadFile alloc] init];
            file.fileData       = profileData;
            file.name           = @"user_image";
            file.fileName       = @"user_image.jpg";
            file.type           = @"image/jpg";
            
            
            [WEB_MANAGER postPathMultipart:kWebServiceUserRegister
                                parameters:params
                               showLoading:nil
                                     files:@[file]
                                   success:^(id responseObject) {
                                       NSLog(@"response:%@", responseObject);
                                       [self removeLoader];
                                       if ([[responseObject objectForKey:RESPONSE_STATUS]boolValue] == 1){
                                           Singleton *obj = [Singleton sharedSingleton];
                                           obj.checkInterest = NO;
                                           NSDictionary *dictUser = [responseObject objectForKey:@"data"];
                                           NSLog(@"%@",dictUser);
                                           NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
                                           [userDefaults setObject:[dictUser objectForKey:@"id"]  forKey:USER_ID];
                                           [userDefaults setObject:[dictUser objectForKey:@"user_email"]  forKey:USER_EMAIL];
                                           [userDefaults setObject:[dictUser objectForKey:@"user_image"]  forKey:USER_IMAGE];
                                           [userDefaults setObject:[dictUser objectForKey:@"user_full_name"]  forKey:USER_FIRST_NAME];
                                           [userDefaults setObject:[dictUser objectForKey:@"user_name"]  forKey:USER_NAME];
                                           [userDefaults setObject:[dictUser objectForKey:@"user_is_social_media"]  forKey:USER_SOCIAL];
                                           [userDefaults setObject:[dictUser objectForKey:@"user_lat"]  forKey:USER_LATITUDE];
                                           [userDefaults setObject:[dictUser objectForKey:@"user_long"]  forKey:USER_LONGITUDE];
                                           [userDefaults setObject:[dictUser objectForKey:@"user_is_blocked"]  forKey:USER_BLOCKED];
                                           [userDefaults setObject:[dictUser objectForKey:@"user_about"]  forKey:USER_ABOUT];
                                           [userDefaults setObject:[dictUser objectForKey:@"user_password"]  forKey:USER_PASSWORD];
                                           [userDefaults setObject:[dictUser objectForKey:@"user_social_type"]  forKey:USER_SOCIAL_TYPE];
                                           [userDefaults setBool:YES forKey:@"userLogin"];
                                           [userDefaults synchronize];
                                           [self pushTab];
                                           
                                       }
                                       else{
                                           
                                       }
                                       
                                       
                                   }
                                   failure:^(NSError *error) {
                                       [self removeLoader];
                                   }];
            
            
            
        }
        else{
            [self addLoader];
            NSDictionary *params = @{@"user_email":self.emailTxt.text,
                                     @"user_password":self.passwordTxt.text,
                                     @"user_full_name":self.fullNameTxt.text,
                                     @"user_name":self.userNameTxt.text,
                                     @"user_device_token":notificationToken,
                                     @"user_lat":latitude,
                                     @"user_long":longitude,
                                     @"user_social_media":@"",
                                     @"user_social_type":@"0"
                                     };
            
            [WEB_MANAGER postPath:kWebServiceUserRegister
                       parameters:params
                      showLoading:nil
                          success:^(id responseObject) {
                              NSLog(@":%@", responseObject);
                              [self removeLoader];
                              if ([[responseObject objectForKey:RESPONSE_STATUS]boolValue] == 1){
                                  Singleton *obj = [Singleton sharedSingleton];
                                  obj.checkInterest = NO;
                                  NSDictionary *dictUser = [responseObject objectForKey:@"data"];
                                  NSLog(@"%@",dictUser);
                                  NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
                                  [userDefaults setObject:[dictUser objectForKey:@"id"]  forKey:USER_ID];
                                  [userDefaults setObject:[dictUser objectForKey:@"user_email"]  forKey:USER_EMAIL];
                                  [userDefaults setObject:[dictUser objectForKey:@"user_image"]  forKey:USER_IMAGE];
                                  [userDefaults setObject:[dictUser objectForKey:@"user_full_name"]  forKey:USER_FIRST_NAME];
                                  [userDefaults setObject:[dictUser objectForKey:@"user_name"]  forKey:USER_NAME];
                                  [userDefaults setObject:[dictUser objectForKey:@"user_is_social_media"]  forKey:USER_SOCIAL];
                                  [userDefaults setObject:[dictUser objectForKey:@"user_lat"]  forKey:USER_LATITUDE];
                                  [userDefaults setObject:[dictUser objectForKey:@"user_long"]  forKey:USER_LONGITUDE];
                                  [userDefaults setObject:[dictUser objectForKey:@"user_is_blocked"]  forKey:USER_BLOCKED];
                                  [userDefaults setObject:[dictUser objectForKey:@"user_about"]  forKey:USER_ABOUT];
                                  [userDefaults setObject:[dictUser objectForKey:@"user_password"]  forKey:USER_PASSWORD];
                                  [userDefaults setObject:[dictUser objectForKey:@"user_social_type"]  forKey:USER_SOCIAL_TYPE];
                                  [userDefaults setBool:YES forKey:@"userLogin"];
                                  [userDefaults synchronize];
                                  [self pushTab];
                                  
                              }
                          }
                          failure:^(NSError *error) {
                              [self removeLoader];
                          }];
            
        }
    }
    else{
        
    }
}
- (IBAction)backToLogin:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)cameraAction:(id)sender{
    [self openCamera];
}

- (IBAction)photoLibraryAction:(id)sender{
    [self openPhotoLibrary];
}
#pragma mark - LocationManager
//------------ Current Location Address-----
-(void)CurrentLocationIdentifier
{
    //---- For getting current gps location
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if (IS_OS_8_OR_LATER) {
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [locationManager requestWhenInUseAuthorization];
            [locationManager requestAlwaysAuthorization];
        }
    }
    [locationManager startUpdatingLocation];
    //------
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations objectAtIndex:0];
    latitude = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
    longitude = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
    [locationManager stopUpdatingLocation];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (!(error))
         {
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             NSLog(@"\nCurrent Location Detected\n");
             NSLog(@"placemark %@",placemark);
             //NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
             //NSString *Address = [[NSString alloc]initWithString:locatedAt];
             NSString *Area = [[NSString alloc]initWithString:placemark.locality];
             NSString *Country = [[NSString alloc]initWithString:placemark.country];
             NSString *CountryArea = [NSString stringWithFormat:@"%@, %@", Area,Country];
             NSLog(@"%@",CountryArea);
         }
         else
         {
             NSLog(@"Geocode failed with error %@", error);
             NSLog(@"\nCurrent Location Not Detected\n");
             //return;
             //CountryArea = NULL;
         }
         /*---- For more results
          placemark.region);
          placemark.country);
          placemark.locality);
          placemark.name);
          placemark.ocean);
          placemark.postalCode);
          placemark.subLocality);
          placemark.location);
          ------*/
     }];
}

#pragma mark Custom Methods

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

-(void)openPhotoLibrary
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
    
}
-(void)openCamera{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:NULL];
}
-(BOOL) validateFields{
    if (self.fullNameTxt.text.length == 0) {
        
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:MSG_ALERT_FIRST_NAME OnController:self];
        return NO;
    }
    if (self.userNameTxt.text.length == 0) {
        
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:MSG_ALERT_USERNAME OnController:self];
        return NO;
    }
    else  if (self.emailTxt.text.length == 0)
    {
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:MSG_ALERT_EMAIL OnController:self];
        return NO;
    }
    else  if([self.passwordTxt.text length] <6){
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:@"Password must be atleast 6 characters" OnController:self];
        return NO;
        
    }
    else  if(isImage == NO){
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:MSG_ALERT_IMAGE OnController:self];
        return NO;
    }
    return YES;
}
#pragma mark - UIImagePickerDelegates
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    isImage = YES;
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.profileImageView.image = chosenImage;
    profileData = UIImageJPEGRepresentation(chosenImage, 0.5);
    [picker dismissViewControllerAnimated:NO completion:nil];
    
}

#pragma mark - UITextfields Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.passwordTxt) {
        if([self.passwordTxt.text length] <6){
            [[UITextField appearance] setTintColor:[UIColor redColor]];
            return NO;
        }
        else{
            [[UITextField appearance] setTintColor:[UIColor blueColor]];
        }
    }
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    [self animateTextField:textField up:NO];
    if (textField == self.passwordTxt) {
        if([textField.text length] <6){
            [HelloLetsGoGlobals showAlertWithTitle:@"Alert!" AndMessage:@"Password must be atleast 6 characters" OnController:self];
        }
    }
    if (textField == self.emailTxt) {
        BOOL isValidate = [self validateEmailWithString:textField.text];
        if (!isValidate) {
            [HelloLetsGoGlobals showAlertWithTitle:@"Alert!" AndMessage:@"Please enter valid email address" OnController:self];
        }
        
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -130; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
