//
//  SelectSocialVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/13/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

typedef void (^RPSBlock)(void);

@interface SelectSocialVC : UIViewController <CLLocationManagerDelegate>
{
    NSArray *interestArray;
    RPSBlock _alertOkHandler;
    NSMutableDictionary * userInfoSocial;
    Singleton *sharedInstance;
    NSData *socialImageData;
    NSString *notificationToken;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    NSString *latitude;
    NSString *longitude;
    NSString *socialType;

}
@end
