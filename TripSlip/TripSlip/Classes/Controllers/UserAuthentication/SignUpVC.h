//
//  SignUpVC.h
//  TripSlip
//
//  Created by bsqptech3 on 10/9/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface SignUpVC : UIViewController <CLLocationManagerDelegate>
{
    NSData *profileData;
    BOOL isImage;
    NSString *notificationToken;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    NSString *latitude;
    NSString *longitude;
}
@property (nonatomic,weak)IBOutlet UIImageView *profileImageView;
@property (nonatomic, weak)IBOutlet UITextField *userNameTxt;
@property (nonatomic, weak)IBOutlet UITextField *fullNameTxt;
@property (nonatomic, weak)IBOutlet UITextField *emailTxt;
@property (nonatomic, weak)IBOutlet UITextField *passwordTxt;

- (IBAction)Signup:(id)sender;
@end
