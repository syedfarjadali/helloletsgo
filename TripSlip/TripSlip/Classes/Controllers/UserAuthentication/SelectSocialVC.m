//
//  SelectSocialVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/13/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "SelectSocialVC.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Google/SignIn.h>


@interface SelectSocialVC () <GIDSignInDelegate,GIDSignInUIDelegate>
@property (strong, nonatomic) FFLoadingView *loadingView;

@end

@implementation SelectSocialVC

- (void)viewDidLoad {
    sharedInstance = [Singleton sharedSingleton];
    socialType = @"";
    [self CurrentLocationIdentifier]; // call this method
    notificationToken = [self notification];
    [GIDSignIn sharedInstance].uiDelegate = self;
    userInfoSocial = [[NSMutableDictionary alloc]init];
    NSNotificationCenter * notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self
                           selector:@selector(notificationRecieved:)
                               name:@"googleSignIn"
                             object:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController.navigationBar setHidden:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Accessors
- (NSString*)notification{
    
    NSUserDefaults * token  = [NSUserDefaults standardUserDefaults];
    NSString *tokenKey = [NSString stringWithFormat:@"%@",[token objectForKey:@"deviceToken"]];
    if (tokenKey == nil || tokenKey == (id)[NSNull null]) {
        
        return @"111111111111111111111111";
    }
    else{
        
        return tokenKey;
    }
    
    
}
- (void)pushToRoot{
    UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarNavRoot"];
    [self presentViewController:vc
                       animated:YES
                     completion:^{
                         
                     }];
}
- (void)pushTab{
    UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectInterestRoot"];
    [self presentViewController:vc
                       animated:YES
                     completion:^{
                     }];
}
- (void)addLoader{
    self.loadingView = [[FFLoadingView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-25, [UIScreen mainScreen].bounds.size.height/2-25, 50, 50)];
    self.loadingView.lineWidth = 2;
    //self.loadingView.strokeColor = [UIColor colorWithRed:21.0/255.0 green:99.0/255.0 blue:153.0/255.0 alpha:1.0];
    self.loadingView.strokeColor = [UIColor whiteColor];
    self.loadingView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.loadingView];
    [self.loadingView startLoading];
    [self.view setUserInteractionEnabled:NO];
}

- (void)removeLoader{
    [self.loadingView finishSuccess:nil];
    [self performSelector:@selector(remove) withObject:nil afterDelay:1.0];
}

- (void)remove{
    [self.view setUserInteractionEnabled:YES];
    [self.loadingView removeFromSuperview];
}

- (void)loginWithSocial:(NSDictionary*)userInfo{
    [self addLoader];
    UploadFile *file    = [[UploadFile alloc] init];
    file.fileData       = socialImageData;
    file.name           = @"user_image";
    file.fileName       = @"user_image.jpg";
    file.type           = @"image/jpg";
    
    [WEB_MANAGER postPathMultipart:kWebServiceUserRegister
                        parameters:userInfo
                       showLoading:nil
                             files:@[file]
                           success:^(id responseObject) {
                               NSLog(@"response:%@", responseObject);
                               [self removeLoader];
                               if ([[responseObject objectForKey:RESPONSE_STATUS]boolValue] == 1){
                                   sharedInstance.checkInterest = NO;
                                   if ([[responseObject objectForKey:RESPONSE_MESSAGE]isEqualToString:@"Already Signed Up Using This Email Via Social Media"]) {
                                       NSDictionary *dictUser = [responseObject objectForKey:@"data"];
                                       NSLog(@"%@",dictUser);
                                       NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
                                       [userDefaults setObject:[dictUser objectForKey:@"id"]  forKey:USER_ID];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_email"]  forKey:USER_EMAIL];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_image"]  forKey:USER_IMAGE];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_full_name"]  forKey:USER_FIRST_NAME];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_name"]  forKey:USER_NAME];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_is_social_media"]  forKey:USER_SOCIAL];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_lat"]  forKey:USER_LATITUDE];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_long"]  forKey:USER_LONGITUDE];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_is_blocked"]  forKey:USER_BLOCKED];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_social_type"]  forKey:USER_SOCIAL_TYPE];
                                       [userDefaults setObject:[responseObject objectForKey:@"intrest_name"]   forKey:USER_INTEREST];
                                       [userDefaults setObject:[responseObject objectForKey:@"intrest_id"]   forKey:USER_INTEREST_ID];
                                       [userDefaults setBool:YES forKey:@"userLogin"];
                                       [userDefaults synchronize];
                                       [self pushToRoot];
                                   }
                                   else{
                                       NSDictionary *dictUser = [responseObject objectForKey:@"data"];
                                       NSLog(@"%@",dictUser);
                                       NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
                                       [userDefaults setObject:[dictUser objectForKey:@"id"]  forKey:USER_ID];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_email"]  forKey:USER_EMAIL];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_image"]  forKey:USER_IMAGE];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_full_name"]  forKey:USER_FIRST_NAME];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_name"]  forKey:USER_NAME];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_is_social_media"]  forKey:USER_SOCIAL];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_lat"]  forKey:USER_LATITUDE];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_long"]  forKey:USER_LONGITUDE];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_is_blocked"]  forKey:USER_BLOCKED];
                                       [userDefaults setObject:[dictUser objectForKey:@"user_social_type"]  forKey:USER_SOCIAL_TYPE];
                                       [userDefaults setBool:YES forKey:@"userLogin"];
                                       [userDefaults synchronize];
                                       [self pushTab];
                                   }
                               }
                               else{
                                   
                               }
                               
                           }
                           failure:^(NSError *error) {
                               [self removeLoader];
                           }];
    
}

- (void)notificationRecieved:(NSNotification*)notification{
    
    NSLog(@"%@",notification.userInfo);
    if (notification.userInfo.count == 0) {
        
    }
    else{
        //[userInfoSocial removeAllObjects];
        //[userInfoSocial setValue:[NSString stringWithFormat:@"%@",[notification.userInfo valueForKey:@"name"]] forKey:@"user_fullname"];
        //[userInfoSocial setValue:[notification.userInfo valueForKey:@"email"] forKey:@"user_email"];
        //[self pushUserSignup:userInfoSocial];
        
        NSURL *aURL = [NSURL URLWithString:[[notification.userInfo valueForKey:@"url"] stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        UIImage *chosenImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:aURL]];
        socialImageData = UIImageJPEGRepresentation(chosenImage, 0.5);
        socialType = @"google";
        NSDictionary *dic = @{@"user_full_name":[notification.userInfo valueForKey:@"name"],@"user_email":[notification.userInfo valueForKey:@"email"],@"user_name":@"",@"user_social_media":@"1",@"user_device_token":notificationToken,@"user_lat":latitude,@"user_long":longitude,@"user_social_type":socialType};
        NSLog(@"%@",dic);
        [self loginWithSocial:dic];
        
    }
    
}
#pragma mark - IBActions
- (IBAction)facebook:(id)sender{
    NSString *fbTitle = NSLocalizedString(@"Facebook", @"Facebook");
    NSString *fbMessage = NSLocalizedString(@"HelloLetsGo", @"HelloLetsGo");
    NSString *fbLogout = NSLocalizedString(@"Log out", @"Log out");
    NSString *fbLogin = NSLocalizedString(@"Log in", @"Log in");
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:fbTitle
                                                                   message:fbMessage
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *button1 = [UIAlertAction actionWithTitle:[FBSDKAccessToken currentAccessToken] ? fbLogout : fbLogin style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * action) {
                                                        //code to run once button is pressed
                                                        if ([FBSDKAccessToken currentAccessToken]) {
                                                            FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
                                                            [login logOut];
                                                        } else {
                                                            // Try to login with permissions
                                                            [self loginAndRequestPermissionsWithSuccessHandler:nil
                                                                                     declinedOrCanceledHandler:^{
                                                                                         // If the user declined permissions tell them why we need permissions
                                                                                         // and ask for permissions again if they want to grant permissions.
                                                                                         [self alertDeclinedPublishActionsWithCompletion:^{
                                                                                             [self loginAndRequestPermissionsWithSuccessHandler:nil
                                                                                                                      declinedOrCanceledHandler:nil
                                                                                                                                   errorHandler:^(NSError * error) {
                                                                                                                                       NSLog(@"Error: %@", error.description);
                                                                                                                                   }];
                                                                                         }];
                                                                                     }
                                                                                                  errorHandler:^(NSError * error) {
                                                                                                      NSLog(@"Error: %@", error.description);
                                                                                                  }];
                                                        }
                                                    }];
    UIAlertAction *button2 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                    handler:^(UIAlertAction * action) {
                                                        
                                                    }];
    [alert addAction:button1];
    [alert addAction:button2];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)googlePlus:(id)sender{
    [[GIDSignIn sharedInstance] signIn];

}
- (IBAction)backToLogin:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)createMyAccount:(id)sender{
    UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpNaV"];
    [self presentViewController:vc
                       animated:YES
                     completion:^{
                         
                     }];
}

#pragma mark FBSignup
- (void)alertDeclinedPublishActionsWithCompletion:(RPSBlock)completion {
    
    NSString *fbErrorTitle = NSLocalizedString(@"Publish Permissions", @"Publish Permissions");
    NSString *fbErrorMessage = NSLocalizedString(@"Publish permissions are needed to share content automatically. Do you want to enable publish permissions?", @"Publish permissions are needed to share content automatically. Do you want to enable publish permissions?");
    NSString *fbNo = NSLocalizedString(@"No", @"No");
    NSString *fbOk = NSLocalizedString(@"Ok", @"Ok");
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:fbErrorTitle
                                 message:fbErrorMessage
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:fbOk
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                                   //[self dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:fbNo
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                                   //[self dismissViewControllerAnimated:YES completion:nil];
                                   
                               }];
    
    _alertOkHandler = [completion copy];
    [alert addAction:okButton];
    [alert addAction:noButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)loginAndRequestPermissionsWithSuccessHandler:(RPSBlock) successHandler
                           declinedOrCanceledHandler:(RPSBlock) declinedOrCanceledHandler
                                        errorHandler:(void (^)(NSError *)) errorHandler{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    sharedInstance.loginManager = login;
    [login
     logInWithReadPermissions: @[@"email",@"public_profile",@"user_friends"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
         } else if (result.isCancelled) {
             
         } else {
             
             NSLog(@"fb user is = %@",result);
             [self fetchUserInfo];
         }
     }];
    
}

-(void)fetchUserInfo
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"%@",[FBSDKAccessToken currentAccessToken].userID);
        sharedInstance.currentAccessToken = [FBSDKAccessToken currentAccessToken];
        sharedInstance.tokenFb = [[FBSDKAccessToken currentAccessToken]tokenString];
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"email, id, name, first_name, last_name, picture.type(large), link, gender"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             NSLog(@"%@",result);
             if (!error)
             {
                 //getting values
                 NSString *name = [result valueForKey:@"name"];
                 NSString *email= [result valueForKey:@"email"];
                 //NSString *gender= [result valueForKey:@"gender"];
                 //NSString *fbId= [result valueForKey:@"id"];
                 NSArray *picture_arr = [result objectForKey:@"picture"];
                 NSArray *data_arr = [picture_arr valueForKey:@"data"];
                 NSString *profile_pic = [data_arr valueForKey:@"url"];
                 NSURL *aURL = [NSURL URLWithString:[profile_pic stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
                 UIImage *chosenImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:aURL]];
                 socialImageData = UIImageJPEGRepresentation(chosenImage, 0.5);
                 socialType = @"facebook";
                 NSDictionary *dic = @{@"user_full_name":name,@"user_email":email,@"user_name":@"",@"user_social_media":@"1",@"user_device_token":notificationToken,@"user_lat":latitude,@"user_long":longitude,@"user_social_type":socialType};
                 NSLog(@"%@",dic);
                 //[userInfoSocial removeAllObjects];
                 //[userInfoSocial setValue:[NSString stringWithFormat:@"%@",[dic valueForKey:@"name"]] forKey:@"user_fullname"];
                 //[userInfoSocial setValue:[dic valueForKey:@"email"] forKey:@"user_email"];
                 //[userInfoSocial setValue:[dic valueForKey:@"url"] forKey:@"user_pic"];
                 //[self pushUserSignup:userInfoSocial];
                 [self loginWithSocial:dic];
                 
             }
             else
             {
                 
                 NSString *alertTitle = NSLocalizedString(@"Facebook", @"Facebook");
                 NSString *alertMessage = NSLocalizedString(@"Something went wrong, Please try again later", @"Something went wrong, Please try again later");
                 NSString *alertOk = NSLocalizedString(@"OK", @"OK");
                 
                 
                 UIAlertController * alert = [UIAlertController
                                              alertControllerWithTitle:alertTitle
                                              message:alertMessage
                                              preferredStyle:UIAlertControllerStyleAlert];
                 
                 
                 UIAlertAction* noButton = [UIAlertAction
                                            actionWithTitle:alertOk
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action) {
                                                //Handle no, thanks button
                                                //[self dismissViewControllerAnimated:YES completion:nil];
                                                
                                            }];
                 
                 [alert addAction:noButton];
                 
                 [self presentViewController:alert animated:YES completion:nil];
                 
             }
         }];
        
    }
    
}

- (void)pushUserSignup:(NSDictionary *)sender{
    
    /*if (sender) {
        [[NSUserDefaults standardUserDefaults] setObject:sender forKey:@"User_Info"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    UINavigationController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"RootUserNav"];
    vc.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor colorWithRed:35.0/255.0 green:56.0/255.0 blue:110.0/255.0 alpha:1.0]};
    [self presentViewController:vc
                       animated:YES
                     completion:^{
                         
                     }];
     */
    
}
#pragma mark - Protocol conformance
#pragma mark - GoogleDelegates
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // ...
}
#pragma mark - LocationManager
//------------ Current Location Address-----
-(void)CurrentLocationIdentifier
{
    //---- For getting current gps location
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if (IS_OS_8_OR_LATER) {
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [locationManager requestWhenInUseAuthorization];
            [locationManager requestAlwaysAuthorization];
        }
    }
    [locationManager startUpdatingLocation];
    //------
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    currentLocation = [locations objectAtIndex:0];
    latitude = [NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude];
    longitude = [NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude];
    [locationManager stopUpdatingLocation];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (!(error))
         {
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
             NSLog(@"\nCurrent Location Detected\n");
             NSLog(@"placemark %@",placemark);
             //NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
             //NSString *Address = [[NSString alloc]initWithString:locatedAt];
             NSString *Area = [[NSString alloc]initWithString:placemark.locality];
             NSString *Country = [[NSString alloc]initWithString:placemark.country];
             NSString *CountryArea = [NSString stringWithFormat:@"%@, %@", Area,Country];
             NSLog(@"%@",CountryArea);
         }
         else
         {
             NSLog(@"Geocode failed with error %@", error);
             NSLog(@"\nCurrent Location Not Detected\n");
             //return;
             //CountryArea = NULL;
         }
         /*---- For more results
          placemark.region);
          placemark.country);
          placemark.locality);
          placemark.name);
          placemark.ocean);
          placemark.postalCode);
          placemark.subLocality);
          placemark.location);
          ------*/
     }];
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:@"googleSignIn"];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
