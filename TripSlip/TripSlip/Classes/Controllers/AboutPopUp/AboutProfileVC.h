//
//  AboutProfileVC.h
//  TripSlip
//
//  Created by bsqptech3 on 11/8/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AboutDelegates <NSObject>

//- (void)closePopup;
- (void)closePopupUser;
//- (void)closePopupBusiness;

@end

@interface AboutProfileVC : UIViewController
{
    
}
@property(nonatomic,assign)id delegate;
@property (nonatomic,weak)IBOutlet UIButton *closeBtn;
@property (nonatomic,weak)IBOutlet UITextView *aboutBox;
@end
