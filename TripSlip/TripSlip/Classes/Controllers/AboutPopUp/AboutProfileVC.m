//
//  AboutProfileVC.m
//  TripSlip
//
//  Created by bsqptech3 on 11/8/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "AboutProfileVC.h"

@interface AboutProfileVC ()

@end

@implementation AboutProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)closeUserPopUp:(id)sender{
    
    if ([_delegate respondsToSelector:@selector(closePopupUser)]) {
        
        [_delegate closePopupUser];
    }
}

- (IBAction)close:(id)sender{
    [self closeUserPopUp:self];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
