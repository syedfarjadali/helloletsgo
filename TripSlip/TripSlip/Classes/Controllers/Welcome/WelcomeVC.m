//
//  WelcomeVC.m
//  TripSlip
//
//  Created by bsqptech3 on 10/13/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "WelcomeVC.h"

@interface WelcomeVC ()

@end

@implementation WelcomeVC

- (void)viewDidLoad {
    
    NSString *htmlString = @"<hr><center><p>Welcome to HelloLet'sGo, the cost sharing mobile app that let's you create events for all the everyday things you like to do. Create and invite your friends, or the public, on activities and events and share in the cost of things like gas, food, parking, hotel, registration, tickets etc., making it more cost effective for you and all your friends to do the things you would be doing anyways. Use the in app features to view user profiles before you join/accept/decline attending an event. Set limits on how many people can join-in on your event and coordinate details using the in app chat/messaging service.</p></center><center><p>View events going on around you using the in app map or list and join those that interest you. All up to you. So come one, create or join an event, and start sharing and saving!</p></center><hr><center><h1>EXAMPLES</h1></center><center><p>Going to the ball game or a concert and have space in your car for others to come along? Create your event in Hello Let's Go, add details like date and time, space limits and required contribution to help with gas or parking, post and share it with your friends on social media.</p></center><center><p>Organizing a group camping trip, bachelor or tailgate party? Create a Group Event in Hello Let's Go and invite friends to join in and share in the cost to make it happen. Use the in app chat and messaging service to discuss and coordinate details as well as view who is going.</p></center><center><p>Going on a road trip to the next city over? Create and post your road trip event and invite friends to go with you and share in the cost of the gas. You will gain company on the trip and share in the cost for gas, making it more cost effective for both to get to your destination....because hey, you were going there anyways.</p></center><center><p>Taking the boat out to go fishing next Saturday? Create your fishing trip event and invite friends and family, or the public, to come along and share in the cost for things like gas on the boat, food or even fish bait. You decide. Post it on Hello Let's Go and share it. Because there is always someone close by that would love to go fishing.</p></center>";
    
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];
   
    
    self.textView.attributedText = attributedString;
    self.textView.font = [UIFont fontWithName:@"SFUIText-Regular" size:14.0];
    self.textView.textColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = false;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController.navigationBar setHidden:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismiss:(id)sender{
    NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:YES forKey:@"welComeScreen"];
    [userDefaults synchronize];
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
