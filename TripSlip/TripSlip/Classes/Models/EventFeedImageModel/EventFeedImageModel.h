//
//  EventFeedImageModel.h
//  TripSlip
//
//  Created by bsqptech3 on 11/15/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
@interface EventFeedImageModel : NSObject
{
    User *user;
}
@property (nonatomic, strong) NSString * userId;
@property (nonatomic, strong) NSString * userImage;
@property (nonatomic, strong) NSString * userFullName;
@property (nonatomic, strong) NSString * imageStatusId;
@property (nonatomic, strong) NSString * imageStatusImage;
@property (nonatomic, strong) NSString * imageStatusDescription;
@property (nonatomic, strong) NSString * imageStatusComments;
@property (nonatomic, strong) NSString * imageStatusLikes;
@property (nonatomic, strong) NSString * type;
+ (instancetype) sharedInstance;
-(id)initWithDictionary:(NSDictionary *)sourceDictionary;
@end
