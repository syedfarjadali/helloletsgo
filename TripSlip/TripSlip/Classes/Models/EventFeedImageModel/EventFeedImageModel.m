//
//  EventFeedImageModel.m
//  TripSlip
//
//  Created by bsqptech3 on 11/15/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "EventFeedImageModel.h"

@implementation EventFeedImageModel
+ (instancetype) sharedInstance

{
    static dispatch_once_t once = 0;
    static EventFeedImageModel *imageFeed;
    
    dispatch_once(&once, ^{ imageFeed = [[EventFeedImageModel alloc] init]; });
    
    return imageFeed;
}

-(id)initWithDictionary:(NSDictionary *)sourceDictionary
{
    self = [super init];
    if (self != nil)
    {
        self.userId                 =[sourceDictionary valueForKey:@"user_id"];
        self.userImage              =[sourceDictionary valueForKey:@"user_image"];
        self.userFullName           =[sourceDictionary valueForKey:@"user_full_name"];
        self.imageStatusId          =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"id"]];
        self.imageStatusImage           =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"image_name"]];
        self.imageStatusDescription       =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"status"]];
        self.imageStatusComments         =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"total_comment"]];
        //self.imageStatusLikes           =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@""]];
        self.type                   =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"type"]];
        //self.user              =[[User alloc] initWithDictionary:[sourceDictionary objectForKey:@"user"]];
    }
    return self;
}
@end
