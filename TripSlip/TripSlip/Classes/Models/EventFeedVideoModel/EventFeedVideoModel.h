//
//  EventFeedVideoModel.h
//  TripSlip
//
//  Created by bsqptech3 on 11/15/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
@interface EventFeedVideoModel : NSObject
{
    User *user;
    
}
@property (nonatomic, strong) NSString * userId;
@property (nonatomic, strong) NSString * userImage;
@property (nonatomic, strong) NSString * userFullName;
@property (nonatomic, strong) NSString * videoId;
@property (nonatomic, strong) NSString * videoImage;
@property (nonatomic, strong) NSString * videoName;
@property (nonatomic, strong) NSString * videoDescription;
@property (nonatomic, strong) NSString * eventComments;
@property (nonatomic, strong) NSString * eventLikes;
@property (nonatomic, strong) NSString * type;
+ (instancetype) sharedInstance;
-(id)initWithDictionary:(NSDictionary *)sourceDictionary;
@end
