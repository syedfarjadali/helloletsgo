//
//  EventFeedVideoModel.m
//  TripSlip
//
//  Created by bsqptech3 on 11/15/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "EventFeedVideoModel.h"

@implementation EventFeedVideoModel
+ (instancetype) sharedInstance

{
    static dispatch_once_t once = 0;
    static EventFeedVideoModel *videoFeed;
    
    dispatch_once(&once, ^{ videoFeed = [[EventFeedVideoModel alloc] init]; });
    
    return videoFeed;
}

-(id)initWithDictionary:(NSDictionary *)sourceDictionary
{
    self = [super init];
    if (self != nil)
    {
        self.userImage              =[sourceDictionary valueForKey:@"user_image"];
        self.userFullName           =[sourceDictionary valueForKey:@"user_full_name"];
        self.videoId          =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"id"]];
        self.videoName          =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"video_name"]];
        self.videoImage           =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"video_thumb"]];
        self.videoDescription       =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"status"]];
        //self.eventComments         =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@""]];
        //self.eventLikes           =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@""]];
        self.type                   =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"type"]];
        //self.user              =[[User alloc] initWithDictionary:[sourceDictionary objectForKey:@"user"]];

    }
    return self;
}
@end
