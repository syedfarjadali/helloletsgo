//
//  EventFeedModel.h
//  TripSlip
//
//  Created by bsqptech3 on 11/13/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
@interface EventFeedModel : NSObject
{
    User *user;
}
@property (nonatomic, strong) NSString * userId;
@property (nonatomic, strong) NSString * userImage;
@property (nonatomic, strong) NSString * userFullName;
@property (nonatomic, strong) NSString * eventName;
@property (nonatomic, strong) NSString * eventId;
@property (nonatomic, strong) NSString * eventImage;
@property (nonatomic, strong) NSString * eventRating;
@property (nonatomic, strong) NSString * eventLatitude;
@property (nonatomic, strong) NSString * eventLongitude;
@property (nonatomic, strong) NSString * eventAddress;
@property (nonatomic, strong) NSString * eventDistance;
@property (nonatomic, strong) NSString * eventReview;
@property (nonatomic, strong) NSString * eventCategory;
@property (nonatomic, strong) NSString * eventContact;
@property (nonatomic, strong) NSString * eventDescription;
@property (nonatomic, strong) NSString * eventTime;
@property (nonatomic, strong) NSString * eventStartDate;
@property (nonatomic, strong) NSString * eventEndDate;
@property (nonatomic, strong) NSString * eventVisitors;
@property (nonatomic, strong) NSString * eventComments;
@property (nonatomic, strong) NSString * eventLikes;
@property (nonatomic, strong) NSString * eventPrivate;
@property (nonatomic, strong) NSString * eventIsLike;
@property (nonatomic, strong) NSString * type;

+ (instancetype) sharedInstance;
-(id)initWithDictionary:(NSDictionary *)sourceDictionary;
+(void) getAllWithParams:(NSDictionary *) params
                 success:(void (^)(id responseObject))success
                 failure:(void (^)(NSError *error))failure;
@end
