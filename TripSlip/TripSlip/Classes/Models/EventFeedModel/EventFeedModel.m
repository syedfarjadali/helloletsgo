//
//  EventFeedModel.m
//  TripSlip
//
//  Created by bsqptech3 on 11/13/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "EventFeedModel.h"
#import "EventFeedImageModel.h"
#import "EventFeedVideoModel.h"

@implementation EventFeedModel

+ (instancetype) sharedInstance

{
    static dispatch_once_t once = 0;
    static EventFeedModel *eventFeed;
    
    dispatch_once(&once, ^{ eventFeed = [[EventFeedModel alloc] init]; });
    
    return eventFeed;
}

-(id)initWithDictionary:(NSDictionary *)sourceDictionary
{
    self = [super init];
    if (self != nil)
    {
        
        /*"category_id" = "69a8caae-a9ba-11e7-a702-180373dcc244";
         "created_at" = "2017-11-14 11:38:48";
         "event_additional" = "chips n drinks";
         "event_description" = "The only way I could ever find out how";
         "event_end_date" = "2017-11-16";
         "event_expected_number_of_visitors" = 50;
         "event_food" = 12;
         "event_gas" = 12;
         "event_hotel" = 12;
         "event_is_private" = 0;
         "event_lat" = "";
         "event_location" = "Amsterdam, Netherlands";
         "event_long" = "";
         "event_name" = birthday;
         "event_other" = 30;
         "event_parking" = 12;
         "event_start_date" = "2017-11-15";
         "event_tickets" = "";
         "event_time" = "06:37:00";
         id = "61481840-c930-11e7-bb86-c37e4e28bf1f";
         "image_name" = "http://olxseventyseven.com/helloletsgo/public/storage/images/eventImages/e11dfa10dc060ba8dd508ac2c4f75354.jpg";
         interested = 0;
         "total_comment" = 0;
         "updated_at" = "2017-11-14 11:38:48";
         "user_full_name" = Bshshe;
         "user_id" = "772e96e0-c62c-11e7-b8ec-239cc5ed14b;
         */
        
        self.userId                 =[sourceDictionary valueForKey:@"user_id"];
        self.userImage              =[sourceDictionary valueForKey:@" user_image"];
        self.userFullName           =[sourceDictionary valueForKey:@"user_full_name"];
        self.eventName              =[sourceDictionary valueForKey:@"event_name"];
        self.eventId                =[sourceDictionary valueForKey:@"id"];
        self.eventImage             =[sourceDictionary valueForKey:@"image_name"];
        self.eventVisitors          =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"event_expected_number_of_visitors"]];
        self.eventAddress           =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"event_location"]];
        self.eventDescription       =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"event_description"]];
        self.eventTime              =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"event_time"]];
        self.eventStartDate         =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"event_start_date"]];
        self.eventEndDate           =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"event_end_date"]];
        self.eventComments          =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"total_comment"]];
        //self.eventLikes             =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"total_likes"]];
        //self.eventRating            =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"event_rating"]];
        self.eventPrivate           =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"event_is_private"]];
        //self.eventIsLike            =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@""]];
        self.type                   =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"type"]];
        //self.user              =[[User alloc] initWithDictionary:[sourceDictionary objectForKey:@"user"]];

    }
    return self;
}
+ (void)getAllWithParams:(NSDictionary *) params
                 success:(void (^)(id responseObject))success
                 failure:(void (^)(NSError *error))failure
{
    [WEB_MANAGER getPath:KWebServiceGetAllEvents
              parameters:params
             showLoading:nil
                 success:^(id responseObject) {
                     NSLog(@"response status userFeedsAll :%@", responseObject);
                     if ([[responseObject valueForKey:RESPONSE_STATUS] boolValue] == 1) {
                         NSDictionary *data = [responseObject valueForKey:@"data"];
                         NSLog(@"eventFeed :%@",data);
                         NSMutableArray *results = [NSMutableArray new];
                         for (NSDictionary *userDictionary in data)
                         {
                             if ([[userDictionary objectForKey:@"type"] isEqualToString:@"imageStatus"]) {
                                 EventFeedImageModel *eventImageData = [[EventFeedImageModel alloc] initWithDictionary:userDictionary];
                                 [results addObject:eventImageData];
                             }
                             else if ([[userDictionary objectForKey:@"type"] isEqualToString:@"videoStatus"]){
                                 EventFeedVideoModel *eventVideoData = [[EventFeedVideoModel alloc] initWithDictionary:userDictionary];
                                 [results addObject:eventVideoData];
                             }
                             else{
                                 EventFeedModel *eventData = [[EventFeedModel alloc] initWithDictionary:userDictionary];
                                 [results addObject:eventData];
                             }
                         }
                         success(results);
                     }
                     
                 }
                 failure:^(NSError *error) {
                     failure(error);
                 }];
    
    
}
@end
