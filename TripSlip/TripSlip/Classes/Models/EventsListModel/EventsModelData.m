//
//  EventsModelData.m
//  TripSlip
//
//  Created by bsqptech3 on 11/14/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "EventsModelData.h"

@implementation EventsModelData
+ (instancetype) sharedInstance

{
    static dispatch_once_t once = 0;
    static EventsModelData *eventModelData;
    
    dispatch_once(&once, ^{ eventModelData = [[EventsModelData alloc] init]; });
    
    return eventModelData;
}

-(id)initWithDictionary:(NSDictionary *)sourceDictionary
{
    self = [super init];
    if (self != nil)
    {
        {
            self.userId                 =[sourceDictionary valueForKey:@"user_id"];
            self.eventName              =[sourceDictionary valueForKey:@"event_name"];
            self.eventId                =[sourceDictionary valueForKey:@"id"];
            self.eventImage             =[sourceDictionary valueForKey:@"event_image"];
            self.eventVisitors          =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"event_expected_number_of_visitors"]];
            self.eventAddress           =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"event_location"]];
            self.eventDescription       =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"event_description"]];
            self.eventTime              =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"event_time"]];
            self.eventStartDate         =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"event_start_date"]];
            self.eventEndDate           =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"event_end_date"]];
            self.eventLat           =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"event_lat"]];
            self.eventLong           =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"event_long"]];
            self.eventtype              =[NSString stringWithFormat:@"%@",[sourceDictionary valueForKey:@"event_is_private"]];
        }
    }
    return self;
}
+ (void)getAllWithParams:(NSDictionary *) params
                 success:(void (^)(id responseObject))success
                 failure:(void (^)(NSError *error))failure
{
    [WEB_MANAGER getPath:KWebServiceUserAllEvents
              parameters:params
             showLoading:nil
                 success:^(id responseObject) {
                     NSLog(@"response status userFeedsAll :%@", responseObject);
                     if ([[responseObject valueForKey:RESPONSE_STATUS] boolValue] == 1) {
                         NSDictionary *data = [responseObject valueForKey:@"data"];
                         NSLog(@"eventFeed :%@",data);
                         NSMutableArray *results = [NSMutableArray new];
                         for (NSDictionary *userDictionary in data)
                         {
                             EventsModelData *eventData = [[EventsModelData alloc] initWithDictionary:userDictionary];
                             [results addObject:eventData];
                             
                         }
                         success(results);
                     }
                     
                 }
                 failure:^(NSError *error) {
                     failure(error);
                 }];
    
    
}
@end
