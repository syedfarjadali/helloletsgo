//
//  User.h
//  TripSlip
//
//  Created by bsqptech3 on 11/20/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
{
 
}
@property (nonatomic, retain)NSString *userID;
@property (nonatomic, retain)NSString *userName;
@property (nonatomic, retain)NSString *fullName;
@property (nonatomic, retain)NSString *emailAddress;
@property (nonatomic, retain)NSString *userPassword;
@property (nonatomic, retain)NSString *userImage;
@property (nonatomic, retain)NSString *userSocialMedia;
@property (nonatomic, retain)NSString *userFollowers;
@property (nonatomic, retain)NSString *userFollowing;
@property (nonatomic, retain)NSString *userLatitude;
@property (nonatomic, retain)NSString *userLongitude;
@property (nonatomic, retain)NSString *userIsBlocked;
@property (nonatomic, retain)NSString *userAbout;
@property (nonatomic, retain)NSString *userSocialType;

+ (instancetype) sharedInstance;
- (id)initWithDictionary:(NSDictionary *)sourceDictionary;
@end
