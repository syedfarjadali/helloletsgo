//
//  User.m
//  TripSlip
//
//  Created by bsqptech3 on 11/20/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "User.h"

@implementation User
+ (instancetype) sharedInstance

{
    static dispatch_once_t once = 0;
    static User *user;
    
    dispatch_once(&once, ^{ user = [[User alloc] init]; });
    
    return user;
}

-(id)initWithDictionary:(NSDictionary *)sourceDictionary
{

    self = [super init];
    if (self != nil)
    {
        self.userID                 =[NSString stringWithFormat:@"%@", [sourceDictionary valueForKey:@"user_id"]];
        self.fullName               =[sourceDictionary valueForKey:@"fullname"];
        self.emailAddress           =[sourceDictionary valueForKey:@"email"];
        self.userName               =[sourceDictionary valueForKey:@"username"];
        self.userPassword           =[sourceDictionary valueForKey:@"password"];
        self.userImage              =[sourceDictionary valueForKey:@"image"];
        self.userSocialMedia        =[sourceDictionary valueForKey:@""];
        self.userFollowers          =[sourceDictionary valueForKey:@"follower"];
        self.userFollowing          =[sourceDictionary valueForKey:@"following"];
        self.userLatitude           =[sourceDictionary valueForKey:@"lat"];
        self.userLongitude          =[sourceDictionary valueForKey:@"long"];
        self.userIsBlocked          =[sourceDictionary valueForKey:@""];
        self.userAbout=             [NSString stringWithFormat:@"%@", [sourceDictionary valueForKey:@""]];
        self.userSocialType=        [NSString stringWithFormat:@"%@", [sourceDictionary valueForKey:@""]];
        
        
    }
    return self;
}
@end
