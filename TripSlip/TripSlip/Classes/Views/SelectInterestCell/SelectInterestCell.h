//
//  SelectInterestCell.h
//  TripSlip
//
//  Created by bsqptech3 on 10/16/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectInterestCell : UITableViewCell

@property (nonatomic,weak)IBOutlet UIButton *checkBtn;
@property (nonatomic,weak)IBOutlet UILabel *interestText;
@property (nonatomic,weak)IBOutlet UIImageView *interestImage;

@end
