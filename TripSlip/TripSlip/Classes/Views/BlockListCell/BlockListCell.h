//
//  BlockListCell.h
//  TripSlip
//
//  Created by bsqptech3 on 10/20/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlockListCell : UITableViewCell

@property (nonatomic, weak)IBOutlet UIButton *unBlockBtn;
@end
