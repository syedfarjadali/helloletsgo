//
//  EventGalleryCell.h
//  TripSlip
//
//  Created by bsqptech3 on 11/7/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventGalleryCell : UITableViewCell

@property (nonatomic,weak)IBOutlet UICollectionView *collectionGallery;
@end
