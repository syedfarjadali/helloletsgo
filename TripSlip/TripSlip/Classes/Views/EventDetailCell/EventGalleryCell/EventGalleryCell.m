//
//  EventGalleryCell.m
//  TripSlip
//
//  Created by bsqptech3 on 11/7/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "EventGalleryCell.h"

@implementation EventGalleryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
