//
//  NewsFeedCell.h
//  TripSlip
//
//  Created by bsqptech3 on 10/16/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsFeedCell : UITableViewCell

@property (nonatomic, weak)IBOutlet UILabel *userFullName;
@property (nonatomic, weak)IBOutlet UIImageView *userImage;
@property (nonatomic, weak)IBOutlet UIImageView *EventImage;
@property (nonatomic, weak)IBOutlet UILabel *EventDescription;
@property (nonatomic, weak)IBOutlet UILabel *EventLikes;
@property (nonatomic, weak)IBOutlet UILabel *EventComments;
@property (nonatomic, weak)IBOutlet UILabel *EventRating;
@property (nonatomic, weak)IBOutlet UIButton *likeBtn;
@property (nonatomic, weak)IBOutlet UIButton *commentBtn;
@property (nonatomic, weak)IBOutlet UIButton *shareBtn;



@end
