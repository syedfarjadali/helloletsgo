//
//  CommentsCell.h
//  TripSlip
//
//  Created by bsqptech3 on 11/14/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentsCell : UITableViewCell
{
    
}
@property (nonatomic,weak)IBOutlet UILabel *messageTxtView;
@property (nonatomic,weak)IBOutlet UILabel *nameLabel;
@property (nonatomic,weak)IBOutlet UIImageView *profileImage;
@end
