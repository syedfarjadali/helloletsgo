//
//  AdsCell.h
//  TripSlip
//
//  Created by bsqptech3 on 11/7/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMobileAds;

@interface AdsCell : UITableViewCell

@property (nonatomic, weak)IBOutlet GADBannerView *bannerView;
@end
