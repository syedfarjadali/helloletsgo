//
//  EventsCell.h
//  TripSlip
//
//  Created by bsqptech3 on 10/16/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsCell : UITableViewCell

@property (nonatomic, weak)IBOutlet UIButton *locationBtn;
@property (nonatomic, weak)IBOutlet UIImageView *eventImage;
@property (nonatomic, weak)IBOutlet UILabel *eventName;
@property (nonatomic, weak)IBOutlet UILabel *eventLocation;
@property (nonatomic, weak)IBOutlet UILabel *eventDate;
@property (nonatomic, weak)IBOutlet UILabel *eventFriends;

@end
