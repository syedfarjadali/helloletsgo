//
//  AboutTableViewCell.h
//  TripSlip
//
//  Created by bsqptech3 on 10/23/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANTagsView.h"

@interface AboutTableViewCell : UITableViewCell

@property (nonatomic, weak)IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong)IBOutlet UIView *anTagView;
@property (nonatomic, strong)IBOutlet UIButton *showAll;
@end
