//
//  InterestCollectionViewCell.h
//  TripSlip
//
//  Created by bsqptech3 on 10/23/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InterestCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak)IBOutlet UILabel *interestDataLabel;
@property (nonatomic, strong) NSLayoutConstraint *cellWidthConstraint;
@end
