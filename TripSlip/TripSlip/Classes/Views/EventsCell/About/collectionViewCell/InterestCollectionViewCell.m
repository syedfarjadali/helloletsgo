//
//  InterestCollectionViewCell.m
//  TripSlip
//
//  Created by bsqptech3 on 10/23/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "InterestCollectionViewCell.h"

static CGSize _extraMargins = {0,0};


@implementation InterestCollectionViewCell

- (void)drawRect:(CGRect)rect
{
    // inset by half line width to avoid cropping where line touches frame edges
    CGRect insetRect = CGRectInset(rect, 0.5, 0.5);
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:insetRect cornerRadius:rect.size.height/2.0];
    
    // white background
    [[UIColor whiteColor] setFill];
    [path fill];
    
    // red outline
    [[UIColor redColor] setStroke];
    [path stroke];
}


/*- (void)awakeFromNib {
    [super awakeFromNib];
    // Cell dimension constraints code
  
   /* self.interestDataLabel.translatesAutoresizingMaskIntoConstraints = NO;
    
    CGFloat screenWidth = CGRectGetWidth([UIScreen mainScreen].bounds);
    
    // The cell width is half the screen width minus the gap between the cells
    // The gap should be slightly larger than the minium space between cells set for the flow layout to prevent layout and scrolling issues
    CGFloat cellWidth = (screenWidth - 10) / 2;
    [self.interestDataLabel addConstraint:[NSLayoutConstraint constraintWithItem:self.interestDataLabel
                                                           attribute:NSLayoutAttributeWidth
                                                           relatedBy:NSLayoutRelationLessThanOrEqual
                                                              toItem:nil
                                                           attribute:NSLayoutAttributeNotAnAttribute
                                                          multiplier:1.0
                                                            constant:cellWidth]];
 
    //[self intrinsicContentSize];
}
- (void)setCellWidth:(CGFloat) width {
    self.cellWidthConstraint.constant = width;
    self.cellWidthConstraint.active = YES;
}

- (CGSize)intrinsicContentSize
{
    CGSize size = [self.interestDataLabel intrinsicContentSize];
    
    if (CGSizeEqualToSize(_extraMargins, CGSizeZero))
    {
        // quick and dirty: get extra margins from constraints
        for (NSLayoutConstraint *constraint in self.constraints)
        {
            if (constraint.firstAttribute == NSLayoutAttributeBottom || constraint.firstAttribute == NSLayoutAttributeTop)
            {
                // vertical spacer
                _extraMargins.height += [constraint constant];
            }
            else if (constraint.firstAttribute == NSLayoutAttributeLeading || constraint.firstAttribute == NSLayoutAttributeTrailing)
            {
                // horizontal spacer
                _extraMargins.width += [constraint constant];
            }
        }
    }
    
    // add to intrinsic content size of label
    size.width += _extraMargins.width;
    size.height += _extraMargins.height;
    
    return size;
}
*/


@end
