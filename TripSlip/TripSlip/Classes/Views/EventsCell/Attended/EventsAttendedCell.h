//
//  EventsAttendedCell.h
//  TripSlip
//
//  Created by bsqptech3 on 10/16/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPFloatRatingView.h"

@interface EventsAttendedCell : UITableViewCell
@property (weak, nonatomic) IBOutlet TPFloatRatingView *ratingView;
@end
