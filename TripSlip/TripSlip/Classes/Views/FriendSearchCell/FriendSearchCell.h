//
//  FriendSearchCell.h
//  TripSlip
//
//  Created by bsqptech3 on 10/18/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendSearchCell : UITableViewCell

@property (nonatomic, weak)IBOutlet UIButton *addBtn;
@property (nonatomic, weak)IBOutlet UIButton *chatBtn;
@end
