//
//  MoreCell.h
//  TripSlip
//
//  Created by bsqptech3 on 10/17/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreCell : UITableViewCell

@property (nonatomic, weak)IBOutlet UILabel *titleText;
@property (nonatomic, weak)IBOutlet UIImageView *titleImageView;
@end
