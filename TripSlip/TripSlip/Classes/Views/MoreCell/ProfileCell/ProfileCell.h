//
//  ProfileCell.h
//  TripSlip
//
//  Created by bsqptech3 on 10/18/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileCell : UITableViewCell

@property (nonatomic, weak)IBOutlet UIButton *optionBtn;
@property (nonatomic, weak)IBOutlet UIButton *aboutBtn;
@property (nonatomic, weak)IBOutlet UIButton *eventBtn;
@property (nonatomic, weak)IBOutlet UILabel *nameLabel;
@property (nonatomic, weak)IBOutlet UILabel *followLabel;
@property (nonatomic, weak)IBOutlet UILabel *followingLabel;
@property (nonatomic, weak)IBOutlet UITextView *aboutLabel;
@property (nonatomic, weak)IBOutlet UIImageView *profileImageView;

@end
