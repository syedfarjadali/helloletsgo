//
//  OthersProfileCell.h
//  TripSlip
//
//  Created by bsqptech3 on 10/19/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OthersProfileCell : UITableViewCell


@property (nonatomic, weak)IBOutlet UIButton *optBtn;
@property (nonatomic, weak)IBOutlet UIButton *followBtn;
@property (nonatomic, weak)IBOutlet UIButton *aboutBtn;
@property (nonatomic, weak)IBOutlet UIButton *eventBtn;

@end
