//
//  LogoutCell.h
//  TripSlip
//
//  Created by bsqptech3 on 10/17/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogoutCell : UITableViewCell

@property (nonatomic, weak)IBOutlet UIButton *logoutBtn;
@end
