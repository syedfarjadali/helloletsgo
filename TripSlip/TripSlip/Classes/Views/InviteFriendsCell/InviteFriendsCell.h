//
//  InviteFriendsCell.h
//  TripSlip
//
//  Created by bsqptech3 on 10/24/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteFriendsCell : UITableViewCell

@property (nonatomic,weak)IBOutlet UIButton *checkBtn;
@property (nonatomic,weak)IBOutlet UILabel *nameLabel;
@property (nonatomic,weak)IBOutlet UIImageView *profileImage;

@end
