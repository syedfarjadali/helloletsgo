//
//  FriendListCell.h
//  TripSlip
//
//  Created by bsqptech3 on 10/18/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendListCell : UITableViewCell

@property (nonatomic, weak)IBOutlet UIButton *unfriendBtn;
@end
