//
//  ViewController.m
//  TripSlip
//
//  Created by bsqptech3 on 9/11/17.
//  Copyright © 2017 bsqptech3. All rights reserved.
//

#import "ViewController.h"
#import "DemoMessagesViewController.h"

@interface ViewController () <JSQDemoViewControllerDelegate,UITextFieldDelegate>

@property (strong, nonatomic) FFLoadingView *loadingView;

@end

@implementation ViewController
//My new project
- (void)viewDidLoad {
    [super viewDidLoad];
    notificationToken = [self notification];
    [self.emailTxt setValue:[UIColor whiteColor]
                 forKeyPath:@"_placeholderLabel.textColor"];
    [self.passwordTxt setValue:[UIColor whiteColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    //numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithKeyboard)],
                           nil];
    [numberToolbar sizeToFit];
    self.emailTxt.inputAccessoryView = numberToolbar;
    self.passwordTxt.inputAccessoryView = numberToolbar;

    
    [self.emailTxt setValue:[UIColor whiteColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
    [self.passwordTxt setValue:[UIColor whiteColor]
                    forKeyPath:@"_placeholderLabel.textColor"];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController.navigationBar setHidden:YES];
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"welComeScreen"]) {
        //show welcome screen
        UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"WelcomeNav"];
        [self presentViewController:vc
                           animated:YES
                         completion:^{
                             
                         }];
    }
    else{
        
    }
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"userLogin"]) {
        
        
    }
    else{
        
        UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarNavRoot"];
        [self presentViewController:vc
                           animated:YES
                         completion:^{
                             
                         }];
    }
}
- (NSString*)notification{
    
    NSUserDefaults * token  = [NSUserDefaults standardUserDefaults];
    NSString *tokenKey = [NSString stringWithFormat:@"%@",[token objectForKey:@"deviceToken"]];
    if (tokenKey == nil || tokenKey == (id)[NSNull null]) {
        
        return @"111111111111111111111111";
    }
    else{
        
        return tokenKey;
    }
    
    
}
- (void)doneWithKeyboard{
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addLoader{
    self.loadingView = [[FFLoadingView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-25, [UIScreen mainScreen].bounds.size.height/2-25, 50, 50)];
    self.loadingView.lineWidth = 2;
    //self.loadingView.strokeColor = [UIColor colorWithRed:21.0/255.0 green:99.0/255.0 blue:153.0/255.0 alpha:1.0];
    self.loadingView.strokeColor = [UIColor whiteColor];
    self.loadingView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.loadingView];
    [self.loadingView startLoading];
    [self.view setUserInteractionEnabled:NO];
}

- (void)removeLoader{
    [self.loadingView finishSuccess:nil];
    [self performSelector:@selector(remove) withObject:nil afterDelay:1.0];
}

- (void)remove{
    [self.view setUserInteractionEnabled:YES];
    [self.loadingView removeFromSuperview];
}
- (void)pushMainTab{
    UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarNavRoot"];
    [self presentViewController:vc
                       animated:YES
                     completion:^{
                         
                     }];
}

- (void)getForgetPassword:(NSString*)email{
    [self addLoader];
    NSDictionary *params = @{@"user_email":email
                             };
    
    [WEB_MANAGER getPath:KWebServiceForgotPAssword
               parameters:params
              showLoading:nil
                  success:^(id responseObject) {
                      [self removeLoader];
                      NSLog(@":%@", responseObject);
                      if ([[responseObject objectForKey:RESPONSE_STATUS]boolValue] == 1){
                          [HelloLetsGoGlobals showAlertWithTitle:RESPONSE_SUCCESS AndMessage:[responseObject objectForKey:RESPONSE_MESSAGE] OnController:self];
                      }
                  }
                  failure:^(NSError *error) {
                      [self removeLoader];
                  }];
    
}
#pragma mark - IBActions
- (IBAction)forgotPassword:(id)sender{
    [self.view endEditing:YES];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:MSG_ALERT_TITLE_APP_NAME message:MSG_ALERT_EMAIL preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Email Address";
    }];
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"Current email %@", [[alertController textFields][0] text]);
        //compare the current password and do action here
        NSString *textField = [[alertController textFields][0] text];
        if (textField.length == 0) {
            [HelloLetsGoGlobals showAlertWithTitle:@"Email Address" AndMessage:MSG_ALERT_EMAIL OnController:self];
        }
        else{
            [self getForgetPassword:textField];
        }
        
    }];
    [alertController addAction:confirmAction];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}
- (IBAction)signUp:(id)sender{
    UINavigationController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectSignUpNav"];
    [self presentViewController:vc
                       animated:YES
                     completion:^{
                         
                     }];
    
}

- (IBAction)Login:(id)sender{
   
    if (self.emailTxt.text.length == 0) {
        
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:MSG_ALERT_EMAIL OnController:self];
        
    }
    else if (self.passwordTxt.text.length == 0){
        
        [HelloLetsGoGlobals showAlertWithTitle:MSG_ALERT_TITLE_APP_NAME AndMessage:MSG_ALERT_PASSORD OnController:self];
        
    }
    else{
        [self addLoader];
        NSDictionary *params = @{@"user_email":self.emailTxt.text,
                                 @"user_password":self.passwordTxt.text,
                                 @"user_device_token":notificationToken
                                 };
        
        
        [WEB_MANAGER postPath:kWebServiceUserLogin
                   parameters:params
                  showLoading:nil
                      success:^(id responseObject) {
                          [self removeLoader];
                          NSLog(@":%@", responseObject);
                          if ([[responseObject objectForKey:RESPONSE_STATUS]boolValue] == 1){
                              NSDictionary *dictUser = [responseObject objectForKey:@"data"];
                              NSLog(@"%@",dictUser);
                              NSUserDefaults * userDefaults = [NSUserDefaults standardUserDefaults];
                              [userDefaults setObject:[dictUser objectForKey:@"id"]  forKey:USER_ID];
                              [userDefaults setObject:[dictUser objectForKey:@"user_email"]  forKey:USER_EMAIL];
                              [userDefaults setObject:[dictUser objectForKey:@"user_image"]  forKey:USER_IMAGE];
                              [userDefaults setObject:[dictUser objectForKey:@"user_full_name"]  forKey:USER_FIRST_NAME];
                              [userDefaults setObject:[dictUser objectForKey:@"user_name"]  forKey:USER_NAME];
                              [userDefaults setObject:[dictUser objectForKey:@"user_is_social_media"]  forKey:USER_SOCIAL];
                              [userDefaults setObject:[dictUser objectForKey:@"user_lat"]  forKey:USER_LATITUDE];
                              [userDefaults setObject:[dictUser objectForKey:@"user_long"]  forKey:USER_LONGITUDE];
                              [userDefaults setObject:[dictUser objectForKey:@"user_is_blocked"]  forKey:USER_BLOCKED];
                              [userDefaults setObject:[dictUser objectForKey:@"user_about"]  forKey:USER_ABOUT];
                              [userDefaults setObject:[dictUser objectForKey:@"user_password"]  forKey:USER_PASSWORD];
                              [userDefaults setObject:[dictUser objectForKey:@"user_social_type"]  forKey:USER_SOCIAL_TYPE];
                              [userDefaults setObject:[responseObject objectForKey:@"intrest_name"]   forKey:USER_INTEREST];
                              [userDefaults setObject:[responseObject objectForKey:@"intrest_id"]   forKey:USER_INTEREST_ID];
                              [userDefaults setBool:YES forKey:@"userLogin"];
                              [userDefaults synchronize];
                              [self pushMainTab];
                          }
                      }
                      failure:^(NSError *error) {
                          [self removeLoader];
                      }];
        
    }
    
}
#pragma mark - Demo delegate

- (void)didDismissJSQDemoViewController:(DemoMessagesViewController *)vc
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - UITextfields Delegates
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -130; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

@end
